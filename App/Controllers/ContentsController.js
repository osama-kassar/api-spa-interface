angular.module('app.contents', [])
.controller('ContentsController', function($scope, $rootScope, $http, $routeParams, CONS, $q, Schema, userDt) {
    
    var user = userDt.data.userInfo
    
    if (user==''||user==null) {
        $(".preloader-all").css({"opacity":"1", "zIndex":"9999"})
        setTimeout(function(){
            $window.location.reload();
        }, 1000)
    }
    $rootScope.scrollTo()
    
    
    
	$scope.param = $routeParams.id;
	$scope.param2 = $routeParams.p2;
    
    $scope.perPage = 12;
    $scope.currPage = 0;
    
    $scope.statusIcons = {
        "scheduled":"calendar", "active":"caret-square-o-right greenText", "inactive":"thumbs-o-up", "deleted":"trash", "not_published":"pause", "published":"bullhorn greenText", "impossible":"hand-paper-o redText"
    }
    $scope.metricsIcons = {"POST_ENGAGED_USERS":"mouse-pointer", "POST_IMPRESSIONS":"eye", "POST_LINKS":"thumbs-o-up"}
    $scope.directories = ["MEINESTADT", "GO_LOCAL", "BUSINESSBRANCHENBUCH", "WEB_DE", "GMX", "ONE_AND_ONE", "I_GLOBAL", "OEFFNUNGSZEITENBUCH", "BUNDES_TELEFONBUCH", "WO_GIBTS_WAS", "DIALO", "HOTFROG", "BRANCHENBUCH_DEUTSCHLAND", "MARKTPLATZ_MITTELSTAND", "GO_YELLOW", "WHERE_TO", "FIND_OPEN", "GUIDELOCAL", "YELLBO", "JELLOO", "ABCLOCAL", "NAVMII"];
    
    var _reset = function(key){
        !key ? key="" : key;
        var obj = {
            "post": {"businessIds":[], "callToAction":"", "couponCode":"", "dateEnd":"", "dateStart":"", "description":"", "directories":[], "excludedLocationIds":[], "labels":[], "locationIds":[], "photos":[], "termsAndConditions":"", "title":"", "type":"POST", "url":"", "videos":[], "g_button":"learn_more"},
            "template": {"description":"", "ownerId":"", "ownerName":"", "photos":"", "shared":"", "title":"", "type":"", "url":"", "userId":user.id, "videos":""}
        };
        return obj[key];
    }
    var _add0 = function (i) {
      if (i < 10) {
        i = "0" + i;
      }
      return i;
    }
    var _find = function(obj, elm, val){
        for(var i in obj){
            if(obj[i][elm] == val){ return true; }
        }
        return false;
    }
    var _isOther = function(obj){
        for(var i in obj){
            if(obj[i].type !== "FACEBOOK" && obj[i].type !== "GOOGLE"){
                return true;
            }
        }
        return false;
    }
    $scope.filter = { "from": "", "to": ""};
    $scope.get_time = function(dat){
        var newDate = new Date(dat);
        var d = _add0( newDate.getHours() )+":"+_add0( newDate.getMinutes() )+":00"
        return d
    }
    
    $scope.doReset = function(tar){
        $scope[tar] = _reset(tar);
    }
    
    
    // DatePicker Range setup
    $scope.datePicker = {
        toOptions: {
            showWeeks: false
        },
        fromOptions: {
            showWeeks: false
        },
        //Method for update minDate and MaxDate
        changeMinAndMaxDates: function () {
            $scope.datePicker.fromOptions.maxDate = $scope.filter.to==null ? null : new Date($scope.filter.to);
            $scope.datePicker.toOptions.minDate = $scope.filter.from==null ? null : new Date($scope.filter.from);
        }
    };
    
    $scope.set_post_class = function(num){
        var cls = '';
        if(num == 2){ cls = 'half'; }
        if(num >= 3){ cls = 'big'; }
        if(num == 102){ cls = 'half'; }
        if(num >= 103){ cls = 'thumb'; }
        return cls;
    }
/************************ POSTS CRUD ***********************************/
    // GET ALL POSTS
    $scope.getPosts = function(){
        var url = "/socialposts/&v="+ user.v +"&offset="+ $scope.currPage +"&max="+$scope.perPage+"&locationIds="+user.managedLocations.join()
        __.getApi( url ).then(function (res) {
            if(res.data.status == "SUCCESS"){
                $scope.posts = res.data.response.socialPosts;
            }else{
                console.log("Error: ".res);
            }
        })
    }
    $scope.getPostableDirectories = function(){
        __.getApi( '/socialposts/directories' ).then(function (res) {
            $scope.postableDirectories = res.data.response.directories
            $scope.postableDirectories.splice(0, 2)
        })
    }
    // GET POST
    $scope.getPost = function(tId){
        var url = "/socialposts/"+tId+"&v="+ user.v
        __.getApi( url ).then(function (res) {
            if(res.data.status == "SUCCESS"){
                $scope.post = res.data.response.socialPost;
                $scope.post.postOn = {}
                $scope.post.postOn.facebook = _find($scope.post.directories, "type", "FACEBOOK")
                $scope.post.postOn.google = _find($scope.post.directories, "type", "GOOGLE")
                $scope.post.postOn.other = _isOther($scope.post.directories)
                $rootScope.filesInfo['photosTemplate'] = $scope.post.photos;
                __.goToTab('create_post');
            }else{
                console.log("Error: ".res);
            }
        })
    }
    /*
    
    // GET TEMPALATE
    $scope.getTemplate = function(tId){
        var url = "/socialposts/templates/"+tId+"&v="+user.v
        __.getApi( url ).then(function (res) {
            if(res.data.status == "SUCCESS"){
                $scope.post = res.data.response.socialPostTemplate;
                $rootScope.filesInfo['photosTemplate'] = $scope.post.photos;
                __.goToTab('create_post');
            }else{
                console.log("Error: ".res);
            }
        })
    }
    */
    
    // SAVE POST
    $scope.savePost = function(pId){
        var method = "PATCH";
        if(!pId){
            pId=""; $scope.post.photos = []; method = "POST"
        }
        $scope.post.businessIds = [];
        $scope.post.labels = [];
        $scope.post.locationIds = user.managedLocations;
        $scope.post.directories = !$scope.post.postOn.other ? [] : $scope.directories;
        if($scope.post.postOn.google){$scope.post.directories.unshift("GOOGLE")}
        if($scope.post.postOn.facebook){$scope.post.directories.unshift("FACEBOOK")}
        
        if($scope.post.type == 'OFFER'){
            $scope.post.dateStart = $scope.filter.from;
            $scope.post.dateEnd = $scope.filter.to;
        }else{
            $scope.post.dateStart = $scope.post.publishDate+" "+$scope.post.publishTime;
        }
        if($scope.post.type == 'QUESTION_AND_ANSWER'){
            $scope.post.directories = ["GOOGLE"]
        }
            
        if($rootScope.filesInfo.photosTemplate){
            for(var i=0; i<$rootScope.filesInfo.photosTemplate.length; i++){
                if($rootScope.filesInfo.photosTemplate[i].tmp_name){
                    var imgParts = $rootScope.filesInfo.photosTemplate[i].tmp_name.split('base64,')
                    $scope.post.photos[i] = imgParts[1]
                }else{
                    $scope.post.photos[i] = $rootScope.filesInfo.photosTemplate[i]
                }
            }
        }else{
            $scope.post.photos = []
        }
        
        var url = "/socialposts/"+pId+"&v="+user.v
        __.getApi( url , $scope.post, "POST")
            .then(function (res) {
                if(res.data.status == "SUCCESS"){
                    __.opAlert("save-success", "success")
                    $scope.getPosts();
                }else{
                    __.opAlert(["save-fail", res.data.message], "error")
                }
            })
    }
    
    // DELETE TEMPALATE
    $scope.deletePost = function(tId){
        if(confirm("Delete?")){
            var url = "/socialposts/"+tId+"&v="+user.v+"&userId="+user.id
            __.getApi( url , '', "DELETE")
                .then(function (res) {
                    if(res.data.status == "SUCCESS"){
                        __.opAlert("delete-success", "success")
                        $scope.getPosts();
                    }else{
                        __.opAlert(["delete-fail", res.data.message], "error")
                    }
                })
        }
    }
    
    
/************************ TEMPLATE CRUD ***********************************/
    // GET ALL TEMPALATES
    $scope.getTemplates = function(){
        var url = "/socialposts/templates"
        __.getApi( url ).then(function (res) {
            if(res.data.status == "SUCCESS"){
                $scope.templates = res.data.response.socialPostTemplates;
            }else{
                console.log("Error: "+res.data.message);
            }
        })
    }
    // GET TEMPALATE
    $scope.getTemplate = function(tId){
        var url = "/socialposts/templates/"+tId+"&v="+user.v
        __.getApi( url ).then(function (res) {
            if(res.data.status == "SUCCESS"){
                $scope.post = res.data.response.socialPostTemplate;
                $rootScope.filesInfo['photosTemplate'] = $scope.post.photos;
                __.goToTab('create_post');
            }else{
                console.log("Error: ".res);
            }
        })
    }
    // SAVE TEMPALATE
    $scope.saveTemplate = function(tId){
        var method = "PATCH";
        if(!tId){
            tId=""; $scope.post.photos = []; method = "POST"
        }
        if($rootScope.filesInfo.photosTemplate){
            for(var i=0; i<$rootScope.filesInfo.photosTemplate.length; i++){
                if($rootScope.filesInfo.photosTemplate[i].tmp_name){
                    var imgParts = $rootScope.filesInfo.photosTemplate[i].tmp_name.split('base64,')
                    $scope.post.photos[i] = imgParts[1]
                }else{
                    $scope.post.photos[i] = $rootScope.filesInfo.photosTemplate[i]
                }
            }
        }else{
            $scope.post.photos = []
        }
        
        $scope.post.access_token = user.access_token;
        $scope.post.userId = user.id;
        var url = "/socialposts/templates/"+tId+"&v="+user.v+"&userId="+user.id
        __.getApi( url, $scope.post, method)
            .then(function (res) {
                if(res.data.status == "SUCCESS"){
                    __.opAlert("save-success", "success")
                    $scope.getTemplates();
                    $scope.post={}
                    $scope.post = res.data.response.socialPostTemplate;
                    $rootScope.filesInfo['photosTemplate'] = $scope.post.photos
                }else{
                    __.opAlert(["save-fail", res.data.message], "error")
                }
        })
    }
    
    // DELETE TEMPALATE
    $scope.deleteTemplate = function(tId){
        if(confirm("Delete?")){
            var url = "/socialposts/templates/"+tId+"&v="+user.v+"&userId="+user.id
            __.getApi( url , '', "DELETE")
                .then(function (res) {
                    if(res.data.status == "SUCCESS"){
                        __.opAlert("delete-success", "success")
                        $scope.getTemplates();
                    }else{
                        __.opAlert(["delete-fail", res.data.message], "error")
                    }
                })
        }
    }
    
});