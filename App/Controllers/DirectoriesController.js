
angular.module('app.directories', [])
.controller('DirectoriesController', function($scope, $rootScope, $http, $routeParams, CONS, $q, Schema, $location, userDt) {
    
    var user = userDt.data.userInfo
    
    if (user==''||user==null) {
        $(".preloader-all").css({"opacity":"1", "zIndex":"9999"});
        setTimeout(function(){
            $window.location.reload();
        }, 1000)
    }
    $rootScope.scrollTo()
    
    
    
	$scope.param = $routeParams.id;
	$scope.param2 = $routeParams.p2;
    
    var _reset = function(key){
        !key ? key="" : key;
        var obj = {
            "directories": {}
        };
        return obj[key];
    }
    $scope.cat_titles = ['Search', 'Social Networks, Apps', 'Map, Navigation Services', 'General Publishers', 'Aggregators'];
    var cats = [
        ["Bing", "Google Search"],
        ["Facebook", "Foursquare", "Instagram", "Yelp"],
        ["Google Maps", "HERE", "TomTom", "Where To?", "GM", "Audi", "Ford", "Fiat", "Navmii", "Waze", "Mercedes", "Uber", "Yellbo", "Toyota", "BMW", "VW"],
        ["Öffnungszeitenbuch", "WEB.DE", "GoYellow", "Yalwa", "Opendi", "infoisinfo", "golocal", "Tupalo", "Cylex", "GMX", "Meinungsmeister", "Branchenbuch Deutschland", "Hotfrog", "Find Open", "Brownbook", "Koomio", "abclocal", "Infobel", "Yandex", "Holiday Check", "TripAdvisor", "1&1", "dialo", "Guidelocal", "wogibtswas.de", "BundesTelefonbuch", "Business Branchenbuch", "jelloo", "meinestadt.de", "Marktplatz Mittelstand"],
        ["Factual"]
    ]
    
    
    $scope.pending = [];
    
    $scope.getDirectories = function(){
            __.getApi( '/locations/'+user.managedLocations[0]+'&v='+__.v ).then( function(res2){
                if(res2.data.status == "SUCCESS"){
                    var dt = res2.data.response.location.listings;
                    var grp = {0:[], 1:[], 2:[], 3:[], 4:[]};
                    for(var i in dt){
                        dt[i].icon = 'published.svg';
                        dt[i].stateMessage = 'dir_published_msg';
                        dt[i].resolveUrl = CONS.uberall+ 'en/app/admea/' +dt[i].type.toLowerCase()+ '/connect?secret=' + dt[i].connectSecret+ '&id=' +user.managedLocations[0]+ '&language=' + __.currLang + '&redirectTo=' + $location.absUrl();
                        dt[i].actionNeeded = null
                        
                        
                        

                        //syncStatus = IN_SYNC
                        if(dt[i].syncStatus == 'IN_SYNC'){
                            dt[i].icon = 'safeguard-active.svg';
                            dt[i].stateMessage = 'dir_safeguard-active_msg';
                        }

                        //syncStatus = NOT_IN_SYNC || claimStatus = UNKNOWN & syncStatus = NOT_FOUND || claimStatus = NOT_CLAIMABLE & syncStatus = NOT_FOUND
                        if(
                            dt[i].syncStatus == 'NOT_IN_SYNC' || 
                            (dt[i].claimStatus == 'UNKNOWN' && dt[i].syncStatus == 'NOT_FOUND') || 
                            (dt[i].claimStatus == 'NOT_CLAIMABLE' && dt[i].syncStatus == 'NOT_FOUND')
                        ){
                            dt[i].icon = 'safeguard-updating.svg';
                            dt[i].stateMessage = 'dir_safeguard-updating_msg';
                        }

                        //listing.mandatoryFields.length > 1 || claimStatus = CLAIMED_BY_OTHERS
                        if(dt[i].mandatoryFields.length>0 || dt[i].claimStatus == 'CLAIMED_BY_OTHERS'){
                            dt[i].icon = 'claimed-by-others.svg';
                            dt[i].stateMessage = 'dir_claimed-by-others';
                        }

                        //connectStatus = NOT_CONNECTED
                        if(dt[i].connectStatus == 'NOT_CONNECTED'){
                            dt[i].icon = 'edit-needed.svg';
                            dt[i].stateMessage = 'dir_edit_required_icon_msg';
                            dt[i].actionNeeded = 'connect';
//                            $scope.pending.push(dt[i])
//                            dt.splice(i, 1);
                        }

                        //syncStatus = NO_ONLINE_LISTING
                        if(dt[i].syncStatus == 'NO_ONLINE_LISTING' || dt[i].claimStatus == 'CLAIMED_BY_US'){
                            dt[i].icon = 'published.svg';
                            dt[i].stateMessage = 'dir_published_msg';
                        }
                        
                        if(dt[i].type == 'GOOGLE'){
                           if(dt[i].connectStatus == 'NOT_CONNECTED' && dt[i].listingUrl == '' && dt[i].claimStatus == 'UNKNOWN'){
                                dt[i].icon = 'edit-needed.svg';
                                dt[i].stateMessage = 'dir_edit_required_icon_msg';
                                dt[i].actionNeeded = 'connect';
                                $scope.pending.push(dt[i])
                                dt.splice(i, 1);
                           }
                           if(dt[i].connectStatus == 'CONNECTED' && dt[i].listingUrl != null && dt[i].claimStatus == 'CLAIMABLE'){
                                dt[i].icon = 'edit-needed.svg';
                                dt[i].stateMessage = 'dir_verify_required_icon_msg';
                                dt[i].actionNeeded = 'verify';
                               $scope.pending.push(dt[i])
                                dt.splice(i, 1);
                            }
                        }
                        
                        if(dt[i].type == 'FACEBOOK'){
                           if(dt[i].connectStatus == 'NOT_CONNECTED'){
                                dt[i].icon = 'edit-needed.svg';
                                dt[i].stateMessage = 'dir_edit_required_icon_msg';
                                dt[i].actionNeeded = 'connect';
                                dt[i].resolveUrl = CONS.uberall+dt[i].type.toLowerCase()+ '/connect?secret=' + dt[i].connectSecret+ '&id=' +user.managedLocations[0]+ '&language=' + __.currLang + '&redirectTo=' + $location.path();
                                $scope.pending.push(dt[i]);
                                dt.splice(i, 1);
                            }
                        }
                        if(!dt[i].icon){
                            dt[i].icon = 'published.svg';
                        }
                        
                        for(var j=0; j<cats.length; j++){
                            if(jQuery.inArray(dt[i].typeName, cats[j]) !== -1) {
                                dt[i].category = $scope.cat_titles[j];
                                grp[j].push( dt[i] );
                            }
                        }
                    }
                    $rootScope.directories = grp
                }
            })
    }
    // get listing statuses and list 
    var getPending2 = function(dt){
        var pending = [];
        for(var i in dt){
            if(dt[i].type == 'FACEBOOK'){
                if(dt[i].connectStatus == 'NOT_CONNECTED'){
                    pending.push(dt[i])
                }
            }
            if(dt[i].type == 'GOOGLE'){
               if(dt[i].connectStatus == 'NOT_CONNECTED' && dt[i].listingUrl == '' && dt[i].claimStatus == 'UNKNOWN'){
                    pending.push(dt[i])
                    if($rootScope.ListingHealth.directoriesMissingConnect.indexOf("GOOGLE") == -1){
                        $rootScope.ListingHealth.directoriesMissingConnect.push('GOOGLE')
                    }
               }
               if(dt[i].connectStatus == 'CONNECTED' && dt[i].listingUrl != null && dt[i].claimStatus == 'CLAIMABLE'){
                    pending.push(dt[i])
                    if($rootScope.ListingHealth.directoriesMissingConnect.indexOf("GOOGLE") == -1){
                        $rootScope.ListingHealth.directoriesMissingConnect.push('GOOGLE')
                    }
                }
            }
        }
        return pending.length;
    }
    
    $scope.getListings2 = function(){
        __.getApi( "/dashboard/listingHealth/"+ user.managedLocations.join() +"&v="+user.v ).then( function(res){
            if(res.data.status == "SUCCESS"){
                $rootScope.ListingHealth = res.data.response
                __.getApi( '/locations/'+user.managedLocations[0]+'&v='+__.v ).then( function(res){
                    if(res.data.status == "SUCCESS"){
                        var dt = res.data.response.location.listings;
                        $rootScope.ListingHealth.pending = getPending2(dt);
                    }
                })
            }else{
                $rootScope.ListingHealth=[]
            }
        })
    }
});