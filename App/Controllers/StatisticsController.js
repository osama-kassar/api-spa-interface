(function(){
angular.module('app.statistics', [])
.controller('StatisticsController', function($scope, $rootScope, $http, $routeParams, CONS, $q, Schema, $translate, userDt, $window) {
    var user = '';
    user = userDt.data.userInfo
    
    if (user==''||user==null) {
        $(".preloader-all").css({"opacity":"1", "zIndex":"9999"})
        setTimeout(function(){
            $window.location.reload();
        }, 1000)
    }
    $rootScope.scrollTo()
    
	$scope.param = $routeParams.id;
	$scope.param2 = $routeParams.p2;
    
    $scope.startDate = $rootScope.set_date("onlydate", -30);
    $scope.endDate = $rootScope.set_date("onlydate");
    $scope.filter = {"from": "", "to": ""};
    
    var _reset = function(key){
        !key ? key="" : key;
        var obj = {
            "statistics": {},
            // CHARTS            
            "Line":{
                datasetOverride: [{ yAxisID: 'y-axis-1' }],
                colors: ["#3ee22e", "#eeffee"],
                options:{
                    scales: {
                        yAxes: [ {
                            position: 'left',
                            ticks: {
                                 beginAtZero: true,
                                 userCallback: function(label, index, labels) {
                                     if (Math.floor(label) === label) {
                                         return label;
                                     }

                                 },
                             }
                        } ],
                        xAxes: [{
                            
                        }]
                    },
                    legend: {
                      display: true,
                      position: 'bottom'
                    },
                },
                labels: [],
                series: [],
                data : [],
            },
            "Base": {
                DataSetOverride : [{ yAxisID: 'y-axis-1' }],
                colors: ["#3ee22e", "#eeeeee"],
                options : {
                    tooltips:{
                        xPadding:16
                    },
                    maintainAspectRatio: false,
                    legend: { display: false },
                    scales: {
                        yAxes: [{
                            backgroundColor:"rgba(252, 207, 3, 1)", 
                            id: 'y-axis-1',
                            position: 'left', 
                            barThickness: 4,
                            border:false,
                            gridLines: {
                                color: "rgba(0, 0, 0, 0)",
                            },
                            stacked: true,
                        }],
                        xAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: 'RATINGS',
                            },
                            gridLines: {
                                color: "rgba(0, 0, 0, 0)",
                            },
                            display: false,
                        }]
                    }
                },
                labels:["★5","★4","★3","★2","★1"], 
                series:["Ratings Number", "Total"], 
                data:[ [] ],
            },
            "Doughnut":{labels:[], data:[], 
                colors: ["#3ee22e", "#03a1fc", "#ff8585"], 
                options: {
                    legend: {
                      display: true,
                      position: 'bottom'
                    },
                    cutoutPercentage: 60,
                    tooltipEvents: [],
                    tooltipCaretSize: 0,
                    showTooltips: true
                }
            },
            
            ///URLS 
            "Feeds": "/dashboard/feed&v="+user.v+"&locationIds="+user.managedLocations.join(),
            "ListingHealth":"/dashboard/listingHealth/"+ user.managedLocations.join() +"&v="+user.v,
            "Duplicates":"/dashboard/suppressedDuplicates&locationIds="+user.managedLocations.join()+"&v="+user.v,
            "CompletenessAverage":"/dashboard/profileCompleteness",
            "Completeness":"/locations/"+ user.managedLocations.join() +"/profileCompleteness&v="+user.v,
            "VisibilityIndexs":"/locations/"+ user.managedLocations.join() +"/visibilityindexes/interesting&v="+user.v+"&startDate=" +$scope.startDate+ "&endDate=" +$scope.endDate,
            "CustomerFeedback":"/dashboard/customerFeedback&v="+ user.v +"&startDate="+ $scope.startDate +"&endDate="+ $scope.endDate +"&textFilter=&locationIds="+ user.managedLocations.join() +"&type=" ,
            "CustomerFeedbackByPeriod":"/dashboard/customerFeedback&v="+user.v+"&startDate=" +$scope.startDate+ "&endDate=" +$scope.endDate+ "&textFilter=&locationIds="+ user.managedLocations.join() +"&type=&group=month" ,
            "CustomerFeedbackKeywords":"/dashboard/customerFeedback&v="+user.v+"&startDate=" +$scope.startDate+ "&endDate=" +$scope.endDate+ "&textFilter=&locationIds="+ user.managedLocations.join() ,

//
//            "FB_Clicks":"/dashboard/insightsData&startDate=" +$scope.startDate+ "&endDate=" +$scope.endDate+ "&group=DAY&type=facebook&metrics=PAGE_CONSUMPTIONS&locationIds=" +user.managedLocations.join()+ "&v="+user.v,
//            "FB_Impressions":"/dashboard/insightsData&startDate=" +$scope.startDate+ "&endDate=" +$scope.endDate+ "&group=DAY&type=facebook&metrics=PAGE_IMPRESSIONS&locationIds=" +user.managedLocations.join()+ "&v="+user.v,
            
            "Statistics":"/datapoints/statistics&v="+user.v,
            "AllListings":"/contentlists/"+user.id,
            "Dashboard":"/locations/" +user.managedLocations.join()+ "/dashboard&v="+user.v,
            
            "AverageRating":"/datapoints/statistics&v=" +user.v+ "&minActionDate=" +$scope.startDate+ "&maxActionDate=" +$scope.endDate+ "&locationIds="+user.managedLocations.join(),
            
            "AverageRatingDirectory":"/dashboard/customerFeedback&v=" +user.v+ "&startDate=" +$scope.startDate+ "&endDate=" +$scope.endDate+ "&type=" +$scope.directory_type+ "&textFilter=&locationIds=" +user.managedLocations.join(),
            
            "G_Impressions":"/dashboard/insightsData&startDate=" +$scope.startDate+ "&endDate=" +$scope.endDate+ "&group=DAY&type=google&metrics[]=ACTIONS_DRIVING_DIRECTIONS&metrics[]=ACTIONS_WEBSITE&metrics[]=ACTIONS_PHONE&locationIds=" +user.managedLocations.join()+ "&v=" +user.v,
            
            "G_Clicks":"/dashboard/insightsData&startDate=" +$scope.startDate+ "&endDate=" +$scope.endDate+ "&group=DAY&type=google&metrics[]=VIEWS_MAPS&metrics[]=VIEWS_SEARCH&locationIds=" +user.managedLocations.join()+ "&v=" +user.v,
            
            "G_Photos":"/dashboard/insightsData&startDate=" +$scope.startDate+ "&endDate=" +$scope.endDate+ "&group=DAY&type=google&metrics[]=PHOTOS_VIEWS_MERCHANT&locationIds=" +user.managedLocations.join()+ "&v=" +user.v,
            
            "G_Queries":"/dashboard/insightsData&startDate=" +$scope.startDate+ "&endDate=" +$scope.endDate+ "&group=DAY&type=google&metrics[]=QUERIES_DIRECT&metrics[]=QUERIES_INDIRECT&metrics[]=QUERIES_CHAIN&locationIds=" +user.managedLocations.join()+ "&v=" +user.v
        };
        return obj[key];
    }
    
//    $scope.CompletenessAverage = _reset("Gauge");
    $scope.VisibilityIndexs = _reset("Line");
    $scope.AverageRating = _reset("Base");
//    $scope.Duplicates = _reset("Doughnut");
    
    
    
    
    // DatePicker Range setup
    $scope.datePicker = {
        toOptions: {
            showWeeks: false
        },
        fromOptions: {
            showWeeks: false
        },
        //Method for update minDate and MaxDate
        changeMinAndMaxDates: function () {
            $scope.datePicker.fromOptions.maxDate = $scope.filter.to==null ? null : new Date($scope.filter.to);
            $scope.datePicker.toOptions.minDate = $scope.filter.from==null ? null : new Date($scope.filter.from);
        }
        
    };
    
    
    
    
    
    //Duplicates list
    $scope.getDuplicates = function(){
        __.getApi( _reset( "Duplicates" ) ).then( function(res){
            if(res.data.status == "SUCCESS"){
                if(res.data.response.countByDirectory.length < 1){
                    $scope.Duplicates.labels = ["No Duplicates"]
                    $scope.Duplicates.data = [0]
                    $scope.Duplicates.data.rows.push({
                        c: [{ v: "No Duplicates"}, {v:1}] 
                    })
                }else{
                    angular.forEach(res.data.response.countByDirectory, function(itm){

                        $scope.Duplicates.labels.push(itm.directoryType)
                        $scope.Duplicates.data.push(itm.count)

    //                    $scope.Duplicates.data.rows.push({
    //                        c: [{ v: itm.directoryType}, {v:itm.count}] 
    //                    })
                    })
                }
            }else{
                $scope.Duplicates = null
            }
        })
    }
    // get listing statuses and list 
    var getPending = function(dt){
        var pending = [];
        for(var i in dt){
            if(dt[i].type == 'FACEBOOK'){
                if(dt[i].connectStatus == 'NOT_CONNECTED'){
                    pending.push(dt[i])
                }
            }
            if(dt[i].type == 'GOOGLE'){
               if(dt[i].connectStatus == 'NOT_CONNECTED' && dt[i].listingUrl == '' && dt[i].claimStatus == 'UNKNOWN'){
                    pending.push(dt[i])
                    if($rootScope.ListingHealth.directoriesMissingConnect.indexOf("GOOGLE") == -1){
                        $rootScope.ListingHealth.directoriesMissingConnect.push('GOOGLE')
                    }
               }
               if(dt[i].connectStatus == 'CONNECTED' && dt[i].listingUrl != null && dt[i].claimStatus == 'CLAIMABLE'){
                    pending.push(dt[i])
                    if($rootScope.ListingHealth.directoriesMissingConnect.indexOf("GOOGLE") == -1){
                        $rootScope.ListingHealth.directoriesMissingConnect.push('GOOGLE')
                    }
                }
            }
        }
        return pending.length;
    }
    
    $scope.getListings = function(){
        __.getApi( "/dashboard/listingHealth/"+ user.managedLocations.join() +"&v="+user.v ).then( function(res){
            if(res.data.status == "SUCCESS"){
                $rootScope.ListingHealth = res.data.response
                __.getApi( '/locations/'+user.managedLocations[0]+'&v='+__.v ).then( function(res){
                    if(res.data.status == "SUCCESS"){
                        var dt = res.data.response.location.listings;
                        $rootScope.ListingHealth.pending = getPending(dt);
                    }
                })
            }else{
                $rootScope.ListingHealth=[]
            }
        })
    }
    //////////////Periodic statistics
    $scope.getVisibilityIndexs = function(){
        __.getApi( _reset( "VisibilityIndexs" ) ).then( function(res){
            $scope.VisibilityIndexs = _reset("Line");
            $scope.VisibilityIndexs.series[0] = 'Visibility Index';
            if(res.data.status == "SUCCESS"){
                angular.forEach(res.data.response.indexes, function(itm){
                    $scope.VisibilityIndexs.labels.push(itm.date.substr(0, 10))
                    $scope.VisibilityIndexs.data[0].push(itm.pointsReached)
                })
            }else{
                $scope.VisibilityIndexs.labels[0]=["No Data"]
                $scope.VisibilityIndexs.data[0]=[0]
            }
        })
    }
    $scope.getRatings = function(){
        __.getApi( _reset( "AverageRating" ) ).then( function(res){
            $scope.AverageRatingChart = _reset("Base")
            var ratings_arr = [0,0,0,0,0];
            var max=0;
            if(res.data.status == "SUCCESS"){
                $scope.AverageRating = res.data.response; 
                var dt = res.data.response.directories; 
                for(var i in dt){
                    var val = Math.floor( dt[i].averageRating )
                    if(val > 0){
                        ratings_arr[val-1] +=1;
                        max+=1
                    }
                }
                $scope.AverageRatingChart.data[0] = ratings_arr.reverse();
                for(var j in $scope.AverageRatingChart.data[0]){
                    $scope.AverageRatingChart.labels[j] = ($scope.AverageRatingChart.data[0][j] / max * 100)+"% "+$scope.AverageRatingChart.labels[j]
                }
                $scope.AverageRatingChart.data[1] = [max,max,max,max,max];
                $scope.AverageRating.total = max;
                $scope.AverageRating.starsVal = Math.floor(res.data.response.averageRating)
            }else{
                $scope.AverageRatingChart.data[0] = [0,0,0,0,0];
                $scope.AverageRatingChart.data[1] = [1,1,1,1,1];
            }
        })
    }
    
    $scope.insights = {"G_Impressions":{},"G_Clicks":{},"G_Photos":{},"G_Queries":{}}
    $scope.getGMetrics = function(tar){
        __.getApi( _reset( tar ) ).then( function(res){
            var dt = res.data.response;
            $scope.insights[tar] = tar == 'G_Queries' ? _reset("Doughnut") : _reset("Line");
            $scope.insights[tar].colors = ["#3ee22e", "#03a1fc", "#ff8585"];
            if(res.data.status == "SUCCESS" && res.data.response.metrics.length>0){
                angular.forEach(dt.metrics, function(row, rk){
                    $scope.insights[tar].data[rk]=[]
                    if(tar == 'G_Queries'){
                        $translate( row.name ).then(function (translatedValue) {
                            $scope.insights[tar].labels.push( translatedValue );
                        });
                        var val = 0;
                        angular.forEach(row.data, function( itm ,ik){
                            val += itm.count
                        })
                        $scope.insights[tar].data[rk] = val
                    }else{
                        $translate( row.name ).then(function (translatedValue) {
                            $scope.insights[tar].series[rk] = translatedValue;
                        });
                        angular.forEach(row.data, function( itm ,ik){
                            if(rk==0){
                                $scope.insights[tar].labels.push(itm.period)
                            }
                            $scope.insights[tar].data[rk].push(itm.count)
                        })
                    }
                })
            }else{
                $scope.insights[tar].data[0]=[0.1];
                $scope.insights[tar].labels = ["No Data"];
                $scope.insights[tar].series = ["No Data"];
            }
        })
    }
    
    $scope.getRatingsDirectory = function(){
        __.getApi( _reset( "AverageRatingDirectory" ) ).then( function(res){
            if(res.data.status == "SUCCESS"){
                $scope.AverageRatingChart = _reset("Base")
                var ratings_arr = [0,0,0,0,0];
                var max= res.data.response.ratingCount||1;
                $scope.AverageRating = res.data.response;
                $scope.AverageRatingChart.data[0] = ratings_arr
                $scope.AverageRatingChart.data[1] = [max,max,max,max,max];
            }else{
                $scope.AverageRatingChart.data[0] = 0
                $scope.AverageRatingChart.data[1] = [0,0,0,0,0];
            }
        })
    }
    
    $scope.getFeeds = function(){
        __.getApi( _reset( "Feeds" ) ).then( function(res){
            if(res.data.status == "SUCCESS"){
                $scope.Feeds = res.data.response;
            }else{
                $scope.Feeds = [];
            }
        })
    }
    
    $scope.getPeriodicStats = function(){
        $scope.getRatings();
        $scope.getGMetrics("G_Impressions");
        $scope.getGMetrics("G_Clicks");
        $scope.getGMetrics("G_Photos");
        $scope.getGMetrics("G_Queries");
    }
    
});
})()
