angular.module('app.users', [])
.controller('UsersController', function($scope, $rootScope, $http, $routeParams, CONS, $q, Schema, $translate, $timeout, $window) {
    
//    var user = userDt.data.userInfo
    
	$scope.param = $routeParams.id;
	$scope.param2 = $routeParams.p2;
    $scope.userData={};
    
    $scope.doLogin = function(){
        $scope.userData.v=__.v
        // Login
        __.getApi('/users/login', $scope.userData, 'post').then(
            function success(response) {
                if(response.data == "" || response.data.status !== 'SUCCESS'){
                    console.log("Cannot connect to data resource! "+response.data);
                    __.opAlert("connect-fail", "error")
                }else{
                    
                    $scope.userData.access_token = response.data.response.access_token;
                    $scope.userData.role = 'user'
                    
                    if(response.data.status == 'SUCCESS' ){
//                        __.opAlert("login-success", "success")
                        // Register new session
                        regSession($scope.userData).then(function(res){
                            $rootScope.user = res.data;
                            $http.get(CONS.appApi+'?r_method=GET&url_target_page=/users/me&v='+__.v).then(function(res){
//                                console.log("user info ", res.data)
                                if(res.data !== ""){
                                    var dt = res.data.response.user;
                                    dt.role = 'user';
                                    dt.access_token = $rootScope.user.userInfo.access_token;
                                    dt.v = $rootScope.user.userInfo.v;
                                    regSession(dt).then(function(res){
                                        $rootScope.goTo('/statistics');
                                    })
                                }else{
                                    console.log(response.data.message);
                                }
                            })
                        })
                    }else{
                        __.opAlert("login-fail", "error")
                        console.log(response.data.message);
                    }
                }
            },
            function error(response) {
                __.opAlert("connect-fail", "error")
                console.log("Unable to perform get request");
            }
        )
    }
	var regSession = function( userData ){
        !userData ? userData=null : userData
        var roleTarget = 'visitor';
        if(userData !== null){roleTarget = userData.role}
        return $http({
			method : 'POST',
			url	: CONS.appHttpMain+'/Acl.php?rl='+roleTarget,
			data: userData
		});
	}
	
	$scope.doLogout = function(){
		regSession(  ).then(function(res){
            __.opAlert("logout-success")
            $timeout(function(){
                $rootScope.goTo('/login');
                $window.location.reload();
            }, 1000);
		})
	}
    
    $scope.chkLogin = function(){
        $timeout(function(){
        if(!__.empty($rootScope.user.userInfo)){
//            __.opAlert('you-already-loggedin')
            $rootScope.goTo('/statistics');
        }},1500)
    }
    $scope.doRedirect = function(){
        $timeout(function(){
            $rootScope.goTo('/statistics');
        }, 1500)
        
    }
    
    
    /*********************************** SETTINGS ********************************/
    var getSettings = function(){
        // GET ALL SESSIONS
        var _urlUser = '/users/me&v='+__.v
        __.getApi( _urlUser ).then(function (res) {
            if(res.data.status == "SUCCESS"){
                $scope.settings = res.data.response.user
                $scope.settings.emailSettings.sort((a, b) => (a.emailType > b.emailType) ? 1 : -1);
                for(var i in $scope.settings.emailSettings){
                    if($scope.settings.emailSettings[i].frequency !== 'NEVER'){
                        $scope.settings.emailSettings[i].val = true
                    }else{
                        $scope.settings.emailSettings[i].val = false
                    }
                }
                // GET ALL SESSIONS CUONT
                var _url = '/users/otherActiveSessions?v='+__.v
                __.getApi( _url ).then(function (res) {
                    if(res.data.status == "SUCCESS"){
                        $scope.settings.allSessions = res.data.response.numberOfOtherActiveTokens
                    }
                })
            }else{
                __.opAlert(["get-fail", res.data.message], "error");
            }
        })
    }
    if(!__.empty($rootScope.user.userInfo)){
        getSettings();
    }
    
    $scope.saveSettings = function(){
        // GET ALL SESSIONS
        var _url = '/users/me&v='+__.v
        __.getApi( _url, $scope.settings, "PATCH"  ).then(function (res) {
            if(res.data.status == "SUCCESS"){
                __.opAlert("save-success", "success");
                getSettings()
            }else{
                __.opAlert(["save-fail", res.data.message], "error");
            }
        })
    }
    
    $scope.deleteSessions = function(){
        // GET ALL SESSIONS
        var _url = "/users/"+user.id+"/allSessions";
        __.getApi( _url, "", "DELETE").then(function (res) {
            if(res.data.status == "SUCCESS"){
                __.opAlert("delete-success", "success");
            }else{
                __.opAlert(["delete-fail", res.data.message], "error");
            }
        })
    }
    
    
    
	
});