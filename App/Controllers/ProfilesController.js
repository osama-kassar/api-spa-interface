angular.module('app.profiles', [])
.controller('ProfilesController', function($scope, $rootScope, $http, $routeParams, CONS, $q, Schema, userDt, uploadFile_service, $timeout) {
    var user = {};
    user = userDt.data.userInfo
    if (user==''||user==null) {
        $(".preloader-all").css({"opacity":"1", "zIndex":"9999"})
        setTimeout(function(){
            $window.location.reload();
        }, 1000)
    }
    $rootScope.scrollTo()
    
	$scope.param = $routeParams.id;
	$scope.param2 = $routeParams.p2;
    $scope.photoObj = {"MAIN":[], "LOGO":[], "SQUARED_LOGO":[], "LANDSCAPE":[], "PHOTO":[] };
    $scope.files={}
    var profileItems = ["activeDirectoriesCount", "activeListingsCount", "addressDisplay", "addressExtra", "attributes", "autoSync", "brands", "businessId", "categories", "cellphone", "city", "cleansingInvalidDataReason", "cleansingStatus", "contentLists", "country", "dataPoints", "dateCreated", "descriptionLong", "descriptionShort", "email", "emailVerification", "fax", "features", "id", "identifier", "imprint", "keywords", "labels", "languages", "lastSyncStarted", "lastUpdated", "lat", "legalIdent", "listings", "listingsBeingUpdated", "listingsInSync", "lng", "mainPhoto", "missingMandatoryFields", "name", "nameDescriptor", "openingHours", "openingHoursNotes", "paymentOptions", "phone", "photos", "profileCompleteness", "province", "publishedListingsCount", "serviceAreas", "services", "socialProfiles", "specialOpeningHours", "status", "street", "streetNo", "streetType", "taxNumber", "updateHistory", "videos", "visibilityIndex", "website", "websiteExtra", "zip"];
    
    var _reset = function(key){
        !key ? key="" : key;
        var obj = {
            "profile": {"locations":{}},
            "landingpage":{
                "header"    :{},
                "aboutus"   :{},
                "products"  :{},
                "news"      :{},
                "photos"    :{},
                "contacts"  :{},
                "pages"     :{},
                "settings"  :{},
                "configs"   :{},
            },
            "product":{
                id:-1, 
                landingpage_id: "",
                product_title: "",
                product_desc: "",
                product_photo: "",
                rec_state:1
            },
            "news":{
                id:-1, 
                landingpage_id: "",
                news_title: "",
                news_desc: "",
                news_photos: "",
                rec_state:1
            },
            "content":{
                id:-1, 
                landingpage_id: "",
                section_id: "",
                content_object: "",
                rec_state:1
            }
            
        };
        return obj[key];
    }
    $scope.attributes_alt = [];
    $scope.landingpage = _reset("landingpage");
    $scope.landingpage.products = _reset("products");
    $scope.landingpage.news = _reset("news");
    $scope.inc = 0;
    $scope.sec_names = ["","header","aboutus","products","news","photos","contacts","pages", "settings", "configs"]
    $scope.photo_names = ["photosLP_LOGO","photosLP_BACKGROUND","photosLP_LOGO", "photosLP_BACKGROUND", "photosLP_ABOUTUS", "photosLP_PRODUCT", "photosLP_PHOTOS"]
    $scope.remove_same_val = function(obj1, obj2){
        var res = [];
        for(var i  in obj1){
            var ind = obj2.map(x=>x.externalId).indexOf(obj1[i].externalId)
            if( ind === -1 ){
                res.push(obj1[i])
            }
        }
        return res;
    }
    
    $scope.getImg = function(url){
        $http.get(url, {responseType: "arraybuffer"}).success((data) => {
                console.log(data)
                //fd.append('file', data);
        });
    }
    
    // SAVE LOCATION ( PROFILE )
    $rootScope.saveProfile = function(){
        
        //Prepare attributes for save
        $rootScope.profile.attributes = [];
        for(var g in $scope.attributes_alt){
            var ind = $scope.attributesList.map(x=>x.externalId).indexOf($scope.attributes_alt[g].externalId)
            var obj = $scope.attributesList[ind];
            obj.value = $scope.attributes_alt[g].value
            
            if($scope.attributesList[g].externalId.indexOf('url_') > -1){
                obj.possibleOptions = null
            }else{
                obj.possibleOptions = [];
                obj.possibleOptions[0] =  {
                    "value": obj.value,
                    "displayName": obj.displayName
                } 
                delete obj.valueMetadata;
            }
            $rootScope.profile.attributes.push(obj)
        }
        
        // Set Pyament logos
        var paymentOptions = []
        for( var j in $rootScope.profile.paymentOptions ){
            paymentOptions.push($rootScope.profile.paymentOptions[j].name)
        }
        $rootScope.profile.paymentOptions = paymentOptions
        
        // Prepare photos for DELETE
        var is_deleted_photo = []
        for(var k3 in $rootScope.profile.photos){
            if(
                $rootScope.filesInfo[ 'photos'+$rootScope.profile.photos[k3].type ].indexOf($rootScope.profile.photos[k3].publicUrl) > -1
            ){
                is_deleted_photo.push( $rootScope.profile.photos[k3] );
            }
        } 
        $rootScope.profile.photos = is_deleted_photo
        
        // Prepare photos for SAVE
        for(var k in $rootScope.filesInfo){
            if($rootScope.filesInfo[k].length>0){
                for(var k2 in $rootScope.filesInfo[k]){
                    if($rootScope.filesInfo[k][k2].tmp_name){
                        var mediaPhtots = {
                            "locationId":user.managedLocations[0], 
                            "order":k2, 
                            "photo": $rootScope.filesInfo[k][k2].tmp_name.split('base64,')[1], 
                            "type":k.replace("photos", ""), 
                            "v":user.v
                        };
                        var url = "/photos"
                        __.getApi( url, mediaPhtots, "POST").then(function (res) {
                            if(res.data.status == "SUCCESS"){
                                $rootScope.profile.photos.push(res.data.response.photo)
                            }
                        })
                    }
                }
            }
        }
        
        // Prepare categories for save
        $rootScope.profile.categories = [];
        for(var i in $scope.categories_alt){
            $rootScope.profile.categories.push($scope.categories_alt[i].id)
        }
        
        // Prepare OpeningHours for save
        $rootScope.profile.openingHours = __.setOpeningHours( $rootScope.profile.openingHours, "forSave" )
        
        
        // Cleaning Location Object for save
        var filteredDt = {"v":user.v}
        for(var k in $rootScope.profile){
            if(profileItems.indexOf(k) > -1){
                filteredDt[k] = $rootScope.profile[k]
            }
        }
        
        // SAVING....
        var _url = "/locations/"+user.managedLocations.join();
        __.getApi( _url, filteredDt, "PATCH").then(function (res) {
            if(res.data.status == "SUCCESS"){
                __.getProfile()
                __.opAlert("save-success", "success");
            }else{
                __.opAlert(["save-fail", res.data.message], "error");
            }
        })
    }
    
    
    $scope.getSuggestions = function(){
        var _url = "/locations/suggestions&v=" +user.v+ "&sort=identifier&order=asc&max=50&locationIds" +user.managedLocations.join()+ "language="+__.currLang;
        __.getApi( _url ).then(function (res) {
            $scope.suggestions = []
            if(res.data.status == "SUCCESS"){
                
                for(var i in res.data.response.locations){  
                    for(var j in res.data.response.locations[i].suggestionsForFields){
                        $scope.suggestions.push( res.data.response.locations[i].suggestionsForFields[j] )
                    }
                }
            }
        })
    }
    
    $rootScope.doSync = function(){
        var _url = "/locations/sync"
        __.getApi( _url, {"locationIds":user.managedLocations}, "POST" ).then(function (res) {
            if(res.data.status == "SUCCESS"){
                __.opAlert("sync-success", "success");
            }else{
                __.opAlert(["sync-fail", res.data.message], "error");
            }
        })
    }
    // GET ALL TEMPALATES
    $scope.getTemplates = function(){
        var url = "/socialposts/templates"
        __.getApi( url ).then(function (res) {
            if(res.data.status == "SUCCESS"){
                $scope.templates = res.data.response.socialPostTemplates;
            }else{
                console.log("Error: "+res.data.message);
            }
        })
    }
    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    ////////////////////////////////////////////////
    //////////LANDING PAGE///////////////
    ////////////////////////////////////////////////
    
    $scope.linksEditData = {
        "keywords":"profiles/basicdata?target=keywords",
        "description":"profiles/basicdata?target=descriptionLong", 
        "auther":"profiles/basicdata?target=name",
        "email":"profiles/basicdata?target=email",
        "phone":"profiles/basicdata?target=phone"
    }
    var sectionsPhotos = [
        "",
        "LP_LOGO, LP_BACKGROUND",
        "LP_ABOUTUS1, LP_ABOUTUS2, LP_ABOUTUS3",
        "LP_PRODUCT",
        "",
        "",
        "",
        ""
    ]
    var _txtFilter = function(txt){
        return txt.replace(/'|"/gi, '')
    }
    var _slug = function (Text){
        return Text
            .toString()
            .trim()
            .toLowerCase()
            .replace(/\s+/g, "-")
            .replace(/[^\w\-]+/g, "")
            .replace(/\-\-+/g, "-")
            .replace(/^-+/, "")
            .replace(/-+$/, "");
            ;
    }
    $scope.chkIsLandingPage = function (){
        __.getRec("-1, "+user.id, "landingpages").then(function(res){
            if(res.data.length > 0){
                $scope.landingpage.configs = res.data[0];
                $scope.landingpage.configs.seo_data = angular.fromJson(res.data[0].seo_data)
            }else{
                var configs = [
                    {"attr":"keywords", "value":$rootScope.profile.keywords.join(","), "type":"name"},
                    {"attr":"description", "value":$rootScope.profile.descriptionLong, "type":"name"},
                    {"attr":"auther", "value":$rootScope.profile.name, "type":"name"}
                ]
                var dt = { "id":-1, "user_id": user.id, "user_domain": user.id+'', "page_title":$rootScope.profile.name, "seo_data":JSON.stringify(configs), "rec_state":0}
                __.saveRec(-1, "landingpages", dt).then(function(res2){
                    $scope.landingpage.configs = res2.data[0];
                    $scope.landingpage.configs.seo_data = angular.fromJson(res2.data[0].seo_data)
                    var sections = [1,2,5,6,7,8]
                    angular.forEach(sections, function(section){
                        $scope.getLPSection(section)
                    })
                })
                alert("You don't have LANDING PAGE yet, We will create you one.");
                __.opAlert('please-wait', 'warning');
                setTimeout(function(){
                    $scope.chkIsLandingPage()
                },3000)
            }
        })
    }
    
    $scope.saveLPSection = function(_section_id){
        __.setCvrBtn('sec_'+_section_id+'_btn' ,1, 'save')
        var secName = $scope.sec_names[_section_id]
        var content = ( $scope.landingpage[secName]['content_object'] );
        
        // Save to database
        function saveIt(_dt){
            __.saveRec(_dt.id, "contents", _dt).then(function(res){
                if(res.data.length>0){
                    __.opAlert('save-success', 'success');
                    __._getUpdate('sec_'+_section_id);
                    __.setCvrBtn('sec_'+_section_id+'_btn' ,0, 'save');
                }else{
                    console.log('Save section data failed '+_section_id)
                }
            })
        }
        var obj = {
            id:$scope.landingpage[secName].id,
            landingpage_id:$scope.landingpage.configs.id,
            section_id:_section_id
        }
        if($scope.files.photos != undefined){
            var oldPhotos = {}
            for(var photoKey in content){
                if(sectionsPhotos[_section_id].indexOf(photoKey)){
                    oldPhotos[photoKey] = content[photoKey]
                }
            }
            uploadPhotos(_section_id, oldPhotos).then(function(res){
                setTimeout(function(){
                    for(var i in res){ content[i] = res[i][0] }
                    obj.content_object = JSON.stringify(content)
                    saveIt(obj)
                },1500)
            })
        }else{
            obj.content_object = JSON.stringify(content)
            saveIt(obj)
        }
    }
    
    var chkIsSection = function(_dt){
//        _dt.content_object = _txtFilter(_dt.content_object);
        __.saveRec(_dt.id||-1, "contents", _dt).then(function(res){
            if(res.data.length>0){
                __._getUpdate('sec_'+_dt.section_id);
            }else{
                console.log('Save section data failed '+_dt.section_id)
            }
        })
    }
    
    $scope.getLPSection = function(_section_id){
        __.setCvr('#page_cvr', 1)
        var secName = $scope.sec_names[_section_id];
        $scope.landingpage[secName] = {}
        setTimeout(function(){
            __.getRec((-1+", "+$scope.landingpage.configs.id+", "+_section_id), "contents").then(function(res){
                if(res.data.length>0){
                    var dt = res.data[0]
                    dt.content_object = angular.fromJson(dt.content_object)
                    $scope.landingpage[secName] = dt
                    for(let k in dt.content_object){
                        if(k.indexOf('LP_') > -1){
                            $rootScope.filesInfo['photos'+k] = [dt.content_object[k]]
                        }
                    }
                }else{
                    var dt = {
                        id:-1, 
                        landingpage_id: $scope.landingpage.configs.id, 
                        section_id: _section_id, 
                        content_object: '',
                        rec_state: 1
                    }
                    // header
                    if(_section_id == 1){
                        $scope.landingpage.header.title="ADD TITLE HERE"
                        $scope.landingpage.header.subtitle="SUB-TITLE"
                        dt.content_object = JSON.stringify($scope.landingpage.header)
                        chkIsSection(dt)
                    }
                    // about us
                    if(_section_id == 2){
                        dt.content_object = JSON.stringify({desc: $rootScope.profile.descriptionLong})
                        chkIsSection(dt)
                    }
                    // photos
                    if(_section_id == 5){
                        dt.content_object = JSON.stringify({
                            photos:[]
                        })
                        chkIsSection(dt)
                    }
                    // contact
                    if(_section_id == 6){
                        dt.content_object = JSON.stringify({
                            address:$rootScope.profile.streetAndNumber+", "+$rootScope.profile.city+", "+$rootScope.profile.country,
                            email:$rootScope.profile.email,
                            phone:$rootScope.profile.phone
                        })
                        chkIsSection(dt)
                    }
                    // pages
                    if(_section_id == 7){
                        $scope.landingpage.pages.p1 = {"title":"Impressum","desc":"ADD YOUR DATA HERE"}
                        $scope.landingpage.pages.p2 = {"title":"Data privacy","desc":"ADD YOUR DATA HERE"}
                        dt.content_object = JSON.stringify($scope.landingpage.pages)
                        chkIsSection(dt)
                    }
                    // settings
                    if(_section_id == 8){
                        $scope.landingpage.settings.products_sec_title = "Our Products & Sevices"
                        $scope.landingpage.settings.isPublished = false
                        dt.content_object = JSON.stringify($scope.landingpage.settings)
                        chkIsSection(dt)
                    }
                }
            })
            __.setCvr('#page_cvr', 0)
        },2000)
    }
    // Upload photos
    function uploadPhotos(_section_id, oldImage){
        var defer = $q.defer();
//        $timeout(function() {
            __.setCvrBtn('sec_'+_section_id+'_btn' ,1, 'save')
            var secName = $scope.sec_names[_section_id]
            var fileName = '';
            var uploaded_photos = [];
            var inc=0;
            var totalPhotos = Object.keys($scope.files.photos)

            for(let k in $scope.files.photos){
                if(sectionsPhotos[_section_id].indexOf(k) > -1){
                    var photos = $scope.files.photos[k]
                    
                    $("#error_message_"+k).html('')
                    uploaded_photos[k] = []
                    
                    !oldImage ? delOldImage = null : delOldImage = oldImage[k];

                    for(let k2 in photos){
                        var photo = photos[k2];
                        fileName = (new Date().getTime()+inc)+'_'+k;
                        var isValidPhoto = __.chkPhoto(photo, k);
                        if(isValidPhoto == true){
                            __.uploadFile( photo, fileName, delOldImage, 'lp')
                                .then(function(res){
                                    if(res.status == "SUCCESS"){
                                        uploaded_photos[k].push( res.uploadedFileName );
                                        if(inc+1 == totalPhotos.length){
                                           defer.resolve(uploaded_photos)
                                        }
                                        inc++
                                    }else{
                                        console.log('Photo upload failed.')
                                    }
                                })
                        }else{
                            $("#error_message_"+k).html(isValidPhoto);
                        }
                    }
                }else{
                    if(inc+1 == totalPhotos.length){
                       defer.resolve(uploaded_photos)
                    }
                    inc++
                }
            }
//        },1500)
        return defer.promise;
    }
    
    // PRODUCTS 
    $scope.getProduct = function(_id){
        $scope.landingpage.product = _reset('product');
        $scope.landingpage.product.landingpage_id= $scope.landingpage.configs.id;
        if(_id == -1){return;}
        __.getRec(_id, "products").then(function(res){
            if(res.data.length>0){
                $scope.landingpage.product = res.data[0]
                $rootScope.filesInfo['photosLP_PRODUCT'] = [res.data[0].product_photo]
            }
        })
    }
    
    $scope.getProducts = function(){
        $scope.landingpage.products=[]
        __.getList($scope.landingpage.configs.id, "products").then(function(res){
            if(res.data.length>0){
                $scope.landingpage.products = res.data
            }
        })
    }
    
    $scope.saveProduct = function(_dt){
        __.setCvrBtn('sec_3_btn' ,1, 'save');
        function saveIt(_dt){
            __.saveRec(_dt.id>-1 ? _dt.id : -1, "products", _dt).then(function(res){
                if(res.data.length>0){
                    $scope.landingpage.product = _reset("product")
                    $rootScope.filesInfo["photosLP_PRODUCT"] = []
                    __.opAlert('save-success', 'success');
                    __.setCvrBtn('sec_3_btn' ,0, 'save');
                    __._getUpdate('sec_3');
                    $("#add_product_btn").click()
                }else{
                    console.log('Save section data failed '+_dt.section_id)
                }
            })
        }
        if(typeof $scope.filesInfo['photosLP_PRODUCT'][0] == 'object'){
            var doUpload = uploadPhotos(3);
            doUpload.then(function(res) {
                    _dt.product_photo = res.LP_PRODUCT[0];
                    _dt.landingpage_id = $scope.landingpage.configs.id;
                    _dt.rec_state=1;

                    saveIt(_dt)
            })
        }else{
            if(_dt.id > 0){
                    saveIt(_dt)
            }else{
                alert('Please add product photo');
                __.setCvrBtn('sec_3_btn' ,0, 'save');
            }
        }
    }
    
    // Delete Product
    $scope.deleteProduct = function(obj){
        if(confirm('Delete?')){
            __.delPhoto(obj.product_photo).then(function(res1){
                __.delRec(obj.id, "products").then(function(res2){
                    console.log(res1.data, res2.data)
                    __.opAlert('delete-success', 'success')
                    __._getUpdate('sec_3');
                })
            })
        }
    }
    
    // NEWS 
    // GET ALL NEWS
    $scope.getNews = function(){
        $scope.landingpage.news=[]
        __.getList($scope.landingpage.configs.id, "news").then(function(res){
            if(res.data.length>0){
                $scope.landingpage.news = res.data
                for(var i in $scope.landingpage.news){
                    $scope.landingpage.news[i].news_photos = res.data[i].news_photos.split(",")
                }
            }
        })
    }
    // SYNC ALL NEWS
    var urlUploadPhotos = function(photos){
        var inc = 0, uploaded_files=[], imgName=[];
        angular.forEach(photos, function(photo){
            imgName = photo.split('/')
            var fname = imgName[imgName.length-2]+"_"+imgName[imgName.length-1]        
            __.uploadUrlFile(photo, fname, 'lp').then(function(upRes){
                uploaded_files.push(fname);
            })
                inc++;
        })
    }
    // SYNC NEWS
    $scope.syncNews = function(){
        __.setCvrBtn('sec_4_btn' ,1, 'exchange');
        var url = "/socialposts/&v="+ user.v +"&offset=0&max=50&locationIds="+user.managedLocations.join()
        
        var dt={}, post={}, nid=-1, inc=0, imgName='', postsIds = '';
        
        __.getApi( url ).then(function (postsRes) {
            if(postsRes.data.status == "SUCCESS"){
                var inc=0
                angular.forEach(postsRes.data.response.socialPosts, function(post){
                    var fname_arr = [];
                    if(post.type == 'POST'){
                        for(var i in post.photos){
                            var fname_parse = post.photos[i].split('/')
                            var fname = fname_parse[fname_parse.length-2]+"_"+fname_parse[fname_parse.length-1] ;
                            fname_arr.push(fname)
                        }
                        dt = {
                            id: -1,
                            landingpage_id : $scope.landingpage.configs.id,
                            post_id : post.id,
                            news_title: post.title,
                            news_desc: post.description,
                            news_photos: fname_arr.join(),
                            rec_state:1
                        }
                        __.saveRec(dt.id, "news", dt).then(function(res2){
                            urlUploadPhotos(post.photos)
                            inc++;
                            if(postsRes.data.response.socialPosts.length == inc){
                                setTimeout(function(){ 
                                    __.opAlert('sync-success', 'success');
                                    __._getUpdate('sec_4');
                                    __.setCvrBtn('sec_4_btn' ,0, 'exchange');
                                }, 3000);
                            }
                        })
                        postsIds+=post.id+","
                    }
                })
                // DELETE NEWS
                __.getList($scope.landingpage.configs.id, "news").then(function(res){
                    if(res.data.length>0){
                        angular.forEach(res.data, function(nItm){
                            if(postsIds.indexOf(nItm.post_id) == -1){
                                __.delRec(nItm.id, "news").then(function(delRes){
                                    for(var foto of nItm.news_photos.split(",")){
                                        $http.get("./Server/ImagesHandler.php?del="+foto).then(function(r){
                                            console.log("Photo Deleted",r.data)
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }else{
                console.log("Error: ".postsRes);
            }
        })
    }
    
    
    
    
    
    
    
    // PHOTOS 
    // SYNC PHOTOS
    
    $scope.syncPhotos = function(_dt){
        __.setCvrBtn('sec_5_btn' ,1, 'exchange');
        console.log("_dt", _dt, $scope.landingpage)
        var photos_arr = [], photos_names_arr = [], imgName = [];
        
        // DELETE PHOTOS FIRST
        angular.forEach(_dt.content_object.photos, function(foto){
            $http.get("./Server/ImagesHandler.php?del="+foto).then(function(r){
                console.log("Photo Deleted",r.data)
            })
        })
        if($rootScope.profile.photos.length>0){
            for(var itm of $rootScope.profile.photos){
                imgName = itm.publicUrl.split('/')
                photos_arr.push(itm.publicUrl)
                photos_names_arr.push(imgName[imgName.length-2]+"_"+imgName[imgName.length-1])
            }
            urlUploadPhotos(photos_arr)
            _dt.content_object = JSON.stringify({"photos":photos_names_arr})
            __.saveRec(_dt.id, "contents", _dt).then(function(res){
                if(res.data.length>0){
                    setTimeout(function(){ 
                        __.opAlert('sync-success', 'success');
                        __._getUpdate('sec_5');
                        __.setCvrBtn('sec_5_btn' ,0, 'exchange');
                    }, 3000)
                }else{
                    console.log('Save section data failed ')
                }
            })
        }
    }
    $scope.syncContent = function(_section_id){
        __.setCvrBtn('sec_'+_section_id+'_sync_btn' ,1, 'exchange');
        
        // about us
        if(_section_id == 2){
            var obj = $scope.landingpage.aboutus
            obj.content_object.desc = $rootScope.profile.descriptionLong
            chkIsSection(obj)
        }
        // contact
        if(_section_id == 6){
            var obj = $scope.landingpage.contacts
            obj.content_object = JSON.stringify({
                address:$rootScope.profile.streetAndNumber+", "+$rootScope.profile.city+", "+$rootScope.profile.country,
                email:$rootScope.profile.email,
                phone:$rootScope.profile.phone,
                lng:$rootScope.profile.lng,
                lat:$rootScope.profile.lat
            })
            chkIsSection(obj)
        }
        setTimeout(function(){
        __.setCvrBtn('sec_'+_section_id+'_sync_btn' ,0, 'exchange');
        }, 3000)
    }
    $scope.syncLandingpage = function(){
        __.setCvrBtn('lp_btn' ,1, 'exchange');
        if($scope.landingpage.configs.id>0){
            var dt = $scope.landingpage.configs
            dt.seo_data = JSON.stringify([
                        {"attr":"keywords", "value":$rootScope.profile.keywords.join(","), "type":"name"},
                        {"attr":"description", "value":$rootScope.profile.descriptionLong, "type":"name"},
                        {"attr":"auther", "value":$rootScope.profile.name, "type":"name"}
                    ])
            dt.user_domain = _slug(dt.user_domain)
            __.saveRec(-1, "landingpages", dt).then(function(res2){
                if(res2.data.length>0){
                    __.opAlert('sync-success', 'success');
                    $scope.chkIsLandingPage();
                }
                __.setCvrBtn('lp_btn' ,0, 'exchange');
            })
        }
    }
});