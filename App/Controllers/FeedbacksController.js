angular.module('app.feedbacks', [])
    .controller('FeedbacksController', function ($scope, $rootScope, $http, $routeParams, CONS, $q, Schema, userDt) {
    // DIFINATIONS 
    $scope.param = $routeParams.id;
    $scope.param2 = $routeParams.p2;
    $scope.perPage = 12;
    $scope.currPage = 1;
    var user = userDt.data.userInfo
    if (user==''||user==null) {
        $(".preloader-all").css({"opacity":"1", "zIndex":"9999"})
        setTimeout(function(){
            $window.location.reload();
        }, 1000)
    }
    $rootScope.scrollTo()
    
    
    $scope.directoyTypes = ["BROWNBOOK", "BUNDES_TELEFONBUCH", "BUSINESSBRANCHENBUCH", "CYLEX", "DIALO", "FACEBOOK", "FIND_OPEN", "FOURSQUARE", "GOOGLE", "GO_LOCAL", "GUIDELOCAL", "INFO_IS_INFO", "INSTAGRAM", "I_GLOBAL", "KOOMIO", "MEINESTADT", "MEINUNGSMEISTER", "STADTBRANCHENBUCH", "UNTERNEHMENSAUSKUNFT", "WHERE_TO", "YALWA", "YELP_API"];

    $scope.ConstumersFeedbacks = []
    $scope.feedbackItem = {}
    $scope.isApproval = 0;

    $scope.filter = { "from": "", "to": "", "status": [], "statusHolder": [], "types":[], "typesHolder":[], "ratings":[] , "ratingsHolder":[], "dataPointTypes":[], "dataPointTypesHolder":[] }

    // RESET OBJECTS
    var _reset = function (key) {
        !key ? key = "" : key;
        var obj = {
            "filter" : { "from": "", "to": "", "status": [], "statusHolder": [], "types":[], "typesHolder":[], "ratings":[] , "ratingsHolder":[], "dataPointTypes":[], "dataPointTypesHolder":[] },
            "feedbacks": {},
            "url" : "/datapoints&v=" + user.v + "&max=" + $scope.perPage + "&page=" + $scope.currPage + "&locationIds=" + user.managedLocations.join() + "&minActionDate=" + setMilliSecond( $scope.filter.from ) + "&maxActionDate=" + setMilliSecond( $scope.filter.to ) + "&directoryTypes[]=" + $scope.filter.types.join("&directoryTypes[]=") + "&dataPointTypes[]=" + $scope.filter.dataPointTypes.join("&dataPointTypes[]=") + "&ratings[]=" + $scope.filter.ratings.join("&ratings[]=") + setStatus() 
        };
        return obj[key];
    }


    // FUNCTIONS
    $scope.setItem=function(itm){
        $scope.feedbackItem = itm
    }
    $scope.resetFilter=function(){
        $scope.filter = _reset('filter')
        $scope.datePicker.changeMinAndMaxDates()
    }
    var setMilliSecond = function(d){
        if(d==""){return "";}
        var res = new Date(d)
        return res.getTime()
    }
    var setStatus = function(){
        if($scope.filter.status.length<1){return "";}
        var res = "&"
        for(var i=0; i<$scope.filter.status.length; i++){
            if($scope.filter.status[i] == 'unread' || $scope.filter.status[i] == 'unreplied'){
                res+= $scope.filter.status[i].replace("un", "")+"=false&"
                if($scope.filter.status[i] == 'unreplied'){ res+= "&dataPointTypes=REVIEW" }
            }
        }
        return res
    }
    $scope.chkbxVal = function(obj, val){
        var ind = -1;
        for(var i=0; i<obj.length; i++){
            if( obj[i] == val){
                ind = i;
                break;
            }
        }

        if(ind > -1){
            obj.splice(ind, 1)
        }else{
            obj.push(val)
        }

    }
    // DatePicker Range setup
    $scope.datePicker = {
        toOptions: {
            showWeeks: false
        },
        fromOptions: {
            showWeeks: false
        },
        //Method for update minDate and MaxDate
        changeMinAndMaxDates: function () {
            $scope.datePicker.fromOptions.maxDate = $scope.filter.to==null ? null : new Date($scope.filter.to);
            $scope.datePicker.toOptions.minDate = $scope.filter.from==null ? null : new Date($scope.filter.from);
        }
    };
    // RESET FILTER
    $scope.clearer = function(tar){
        if(tar=='to'){
            $scope.filter.to = null
            $scope.datePicker.changeMinAndMaxDates()
        }
        if(tar=='from'){
            $scope.filter.from = null
            $scope.datePicker.changeMinAndMaxDates()
        }
        if(tar=='status'){
            $scope.filter.status = null
        }
        if(tar=='type'){
            $scope.filter.type = null
        }
        if(tar=='ratings'){
            $scope.filter.ratings = null
        }
    }
    $scope.selectAll = function(tar, val){
        var arr = $("."+tar);
        for(var i=0; i<arr.length; i++){
            $(arr[i]).prop("checked", val)
        }
    }
    // Customers feedbacks
    $scope.getFeedbacks = function(noExtend){

        !noExtend ? noExtend = false : noExtend;

        __.setCvrBtn("show_more", 1)
        var _url = _reset("url");
        if($scope.isApproval == 1){
            _url = "/datapoints&v="+ user.v +"&max=100&page=1&approvalNeededReplies=true&locationIds="+ user.managedLocations.join()
        }
        __.getApi( _url ).then(function (res) {
            __.setCvrBtn("show_more", 0)
            if (res.data.status == "SUCCESS") {
                $scope.total = res.data.response.inbox.count
                if($scope.ConstumersFeedbacks.length>0 && !noExtend){
                    var extendedObj = $scope.ConstumersFeedbacks.concat(res.data.response.inbox.dataPoints)
                    $scope.ConstumersFeedbacks = extendedObj
                    $scope.currPage = $scope.currPage+1
                }else{
                    $scope.feedbackItem = res.data.response.inbox.dataPoints[0]
                    $scope.ConstumersFeedbacks = res.data.response.inbox.dataPoints;
                }
            }
        })
    }

    // Mark as read
    $scope.MarkAsRead = function(){
        var itms = $(".feedbackItems");
        var dtArray = [];
        for(var i=0; i<itms.length; i++){
            if(  $( itms[i] ).is(":checked") ){
                dtArray.push($scope.ConstumersFeedbacks[i].id)
            }
        }
        if(dtArray.length>0){
            __.setCvrBtn("as_read_btn", 1)
            var dt={'read':false,'locationIds':user.managedLocations,'selectAll':false,'ids':dtArray,'excludeIds':[]}
                      
            __.getApi( "/datapoints/read", dt, "POST" ).then(function (res) {
                __.setCvrBtn("as_read_btn", 0, "envelope-open-o")
                if (res.data.status == "SUCCESS") {
                    __.opAlert("save-success", "success")
                }else{
                    __.opAlert(["save-fail", res.data.message], "error")
                }
            })
        }else{
            __.opAlert("please-select-items")
        }
    }

    // Mark as read
    $scope.reply = function(id){
        !id ? id = $scope.feedbackItem.id : id;
        __.setCvrBtn("reply_btn", 1)
        var dt={"reply": $scope.feedbackItem.reply}

        __.getApi( "/datapoints/"+id+"/reply", dt, "PATCH" ).then(function (res) {
            __.setCvrBtn("reply_btn", 0, "send")
            if (res.data.status == "SUCCESS") {
                __.opAlert("save-success", "success")
            }else{
                __.opAlert(["save-fail", res.data.message], "error")
            }
        })
    }
});
