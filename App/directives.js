
var ptrn=[];
	ptrn['isEmail'] 		= /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,7}$/;
	ptrn['isNumber'] 		= /^[0-9]{0,}$/;
	ptrn['isInteger'] 		= /^[\s\d]$/;
	ptrn['isFloat'] 		= /^[0-9]?\d+(\.\d+)?$/;
	ptrn['isVersion'] 		= /^(?:(\d+)\.)?(?:(\d+)\.)?(\*|\d+)$/
	ptrn['isPassword'] 		= /^((?!undefined).)[A-Za-z0-9@#$%^&*()!_-]{8,32}$/;
	ptrn['isParagraph'] 	= /^.{20,255}$/;
	ptrn['isEmpty'] 		= /^((?!undefined).){3,255}$/;
	ptrn['isSelected'] 		= /^((?!undefined)(?!null).){1,999}$/;
	ptrn['isZipcode'] 		= /^\+[0-9]{1,4}$/;
	ptrn['isPhone'] 		= /^[\s\d]{9,12}$/;
    ptrn['isDomain'] 		= /^.[a-zA-Z0-9-]{2,61}$/;
	ptrn['isTrue']          = /^(true|TRUE|1|'1')$/;
	
function _setError(elm, msg, clr){
        
    !msg ? msg='' : msg;
    !clr ? clr=false : clr;
        
    if(msg == ''){
        $(elm).parent().find(".nfoHolder").html(" <div class='fa fa-check-circle green'></div> ")
    }else{
        $(elm).parent().find(".nfoHolder").html(" <div uib-tooltip='"+msg+"' class='fa fa-info-circle red'></div> ")
    }
}

//function _chkFileError(obj){
//	//alert(obj.size+' ==> '+obj.type)
//	var max_size = 3; //mb
//	var allowed_ext = ['jpeg', 'jpg', 'txt'];
//	var res = '';
//	
//	if( (obj.size / 1024) / 1024 > max_size ){
//		res = 'File too big';
//	}
//	if( allowed_ext.indexOf( obj.type.split('/')[1] ) ){ 
//		res = 'Extension not supported' 
//	}
//	return res;
//}

function _dHandler(val){
	var now = new Date( Date.parse(val) )
    var year    = now.getFullYear();
    var month   = now.getMonth()+1; 
    var day     = now.getDate();
    var hour    = now.getHours();
    var minute  = now.getMinutes();
    var second  = now.getSeconds(); 
    if(month.toString().length == 1) {
        month = '0'+month;
    }
    if(day.toString().length == 1) {
        day = '0'+day;
    }   
    if(hour.toString().length == 1) {
        hour = '0'+hour;
    }
    if(minute.toString().length == 1) {
        minute = '0'+minute;
    }
    if(second.toString().length == 1) {
        second = '0'+second;
    }
    var onlydate = year+'-'+month+'-'+day
    var onlytime = hour+':'+minute+':'+second;
    
    var res= val.length < 11 ? onlydate : onlydate+' '+onlytime;
    return res;
}
angular.module('app.directives', [])

.factory("Schema", function($http, $q, CONS){ 
	return {
		get: function(obj, tbl){
			var deferred = $q.defer()
			$http.get(CONS.appHttpMain+'/DbHandler.php?s='+tbl).then(function(r){
				var res = [];
				angular.forEach(r.data, function(elm){
					if( typeof( obj[elm[0]] ) !== 'undefined' ){
                        
						if( elm[1].indexOf('int') > -1 ){
							res.push(obj[elm[0]]);	
						}else{
							var v = obj[elm[0]].replace("'", "''")
							res.push("'"+ v +"'");	
						}
					}else{
						res.push("''")
					}
				})
				deferred.resolve(res)
			})
        	return deferred.promise;
		}
	}
})
.factory("uberApi", function($http, $q, CONS){ 
	return {
		getUrl: function(tar, dt){
			var deferred = $q.defer();
            $http({
                method : "POST",
                url    : CONS.appApi+'?r_method=GET&url_target_page='+tar,
                data   : dt
            }).then(function(res){
				deferred.resolve(res);
            })
        	return deferred.promise;
        }
    }
})
.directive('dateHandler', function ($filter) {
	function link(scope, element, attrs, ngModel) {
		ngModel.$parsers.push(function () {
			return _dHandler( $(element).val() )
		});
		ngModel.$formatters.push(function (value) {
			if(value){	return new Date( _dHandler( value ) );	}
		});
	} return {
		restrict: 'A',
		require: 'ngModel',
		link: link
	}
})
.directive('chk', ['$translate', '$compile', function ($translate, $compile) {
    return {
        restrict: 'AE',
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {
            if(attrs.chk == 'allow'){return ;}
            ctrl.$parsers.unshift(function (viewValue) {
                var chkVal = ptrn[attrs.chk].test(viewValue)
                $translate( attrs.chk ).then(function(res){ 
                    _setError(element[0] , chkVal?'':res, chkVal);
                    var tar = $(element).parent().find(".nfoHolder")
                    $compile(tar)(scope);
                });
                ctrl.$setValidity('checked', chkVal);
                return viewValue;
            });
            ctrl.$formatters.push(function () {
                var chkVal = ptrn[attrs.chk].test(ctrl.$modelValue)
                $translate( attrs.chk ).then(function(res){ 
                    _setError(element[0] , chkVal?'':res, chkVal);
                    var tar = $(element).parent().find(".nfoHolder")
                    $compile(tar)(scope);
                });
                ctrl.$setValidity('checked', chkVal);
                return ctrl.$modelValue;
            });
        }   
    };
}])
//.directive("autoFill", ['$timeout', function ($timeout) {
//    return {
//        require: 'ngModel',
//        link: function (scope, element, attrs, ngModel) {
//            var fieldName = attrs.ngModel.split(".");
//            attrs.$set('name', fieldName[fieldName.length-1] )
//            //element.after('<div class="required-tick">*</div>');
//            element.on('change.autofill DOMAttrModified.autofill keydown.autofill propertychange.autofill', function (e) {
//                $timeout( function () {
//                    if (element.val() !== '') {
//                            element.trigger('input');
//                    }
//                }, 0);
//            });
//
//        }
//    }
//}])
.service('Menu', ['$http', '$q', function ($http, $q) {
    return {
        getMenu: function (lang, parent_id) {
            !parent_id ? parent_id=1 : parent_id;
            var url = parent_id == 0 ? 
                "./Server/Controllers/categories.php?lang="+lang : 
                "./Server/DbHandler.php?r=categories_list"
                return $http({
                    method : 'POST',
                    url	   : url,
                    data   : parent_id+', '+lang+', \'categoty_priority\', 0, 99',
                })
            }
        }
}])
.directive('masonry', function($timeout) {
    return {
        restrict: 'AC',
        link: function(scope, elem, attrs) {

            scope.$watch(function() {
                return elem[0].children.length;
            },

            function(newVal) {
                $timeout(function() {
                    elem.masonry('reloadItems');
                    elem.masonry();
                },500)
            })

            elem.masonry({
                columnSize: ".grid-sizer",
                itemSelector: '.item'
            });
            scope.masonry = elem.data('masonry');
        }
    }
})
.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function($scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            element.bind('change', function(changeEvent){
                angular.forEach(changeEvent.target.files, function(itm, k){
                    if($scope.filesInfo[attrs.name] === undefined || $scope.filesInfo[attrs.name] == null){
                        $scope.filesInfo[attrs.name] = []
                    }
                    // prepare file info
                    var reader = new FileReader();
                    reader.onload = function (loadEvent) {
                        $scope.$apply(function () {
                            if(attrs.multiple){
                                $scope.filesInfo[attrs.name].push({
                                    lastModified: itm.lastModified,
                                    lastModifiedDate: itm.lastModifiedDate,
                                    name: itm.name,
                                    size: itm.size,
                                    type: itm.type,
                                    tmp_name: loadEvent.target.result
                                })
                            }else{
                                
                                var u = URL.createObjectURL(element[0].files[k]);
                                var img = new Image;
                                var dim = []
                                img.onload = function() {
                                    element[0].files[k].dim = {w:img.width, h:img.height};
                                };
                                img.src = u;
                                
                                $scope.filesInfo[attrs.name][0] = ({
                                    lastModified: itm.lastModified,
                                    lastModifiedDate: itm.lastModifiedDate,
                                    name: itm.name,
                                    size: itm.size,
                                    type: itm.type,
                                    tmp_name: loadEvent.target.result
                                })
                            }
                        });
                    }
                    reader.readAsDataURL(itm);
                    // prepage file upload
                    $scope.$apply(function(){
                        modelSetter($scope, [ element[0].files[k] ]);
                    });
                })
            })
        }
    }
}])
.service('uploadFile_service', ['$http', function ($http) {
	return {
		uploadFileToUrl : function(file, uploadUrl, name){
			var fd = new FormData();
			fd.append('file', file);
			fd.append('name', name);
//			console.dir(fd);
			return $http.post(uploadUrl, fd, {
				 transformRequest: angular.identity,
				 headers: {'Content-Type': undefined,'Process-Data': false}
			}).then(function(res){
				 return res.data
			});
		}
	}
}])
.directive('datePicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
         link: function (scope, element, attrs, ngModelCtrl) {
            element.datepicker({
                dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
                onSelect: function (date) {
                    scope[attrs.datePicker] = date;
                    scope.$apply();
                }
            });
        }
    };
})
.directive("autoFill", ['$timeout', function ($timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            var fieldName = attrs.ngModel.split(".");
            attrs.$set('name', fieldName[fieldName.length-1] )
            //element.after('<div class="required-tick">*</div>');
            element.on('change.autofill DOMAttrModified.autofill keydown.autofill propertychange.autofill', function (e) {
                $timeout( function () {
                    if (element.val() !== '') {
                            element.trigger('input');
                    }
                }, 0);
            });

        }
    }
}])
.directive("showPassword", ['$timeout', function ($timeout) {
    return {
        link: function (scope, elm, attrs, ngModel) {
            elm.bind('click', function(e) {
                var field = $(elm).parent().find("input")
                if(field.attr("type") == "password"){
                    $(elm).parent().find("input").attr("type", "text");
                    $(elm).removeClass("fa-eye-slash").addClass("fa-eye");
                }else{
                    $(elm).parent().find("input").attr("type", "password");
                    $(elm).removeClass("fa-eye").addClass("fa-eye-slash");
                }
            })
        }
    }
}])
.directive("showImg", function () {
    return {
        link: function (scope, elm, attrs, ngModel) {
            elm.bind('click', function(e) {
                if(attrs.src.indexOf(".svg")>0){return "";}
                $('#imgHolder').html('<img src="'+attrs.src.replace("thumb/", "")+'"><i class="fa fa-times" aria-hidden="true"></i>');
                $('#imgHolder').attr("style","opacity: 1; z-index:999;");
            })
        }
    }
})
.directive('openDialog', function($compile){
    return {
        restrict: 'A',
        link: function(scope, elem, attr, ctrl) {
            elem.bind('click', function(e) {
                $('#modal-holder').html('<section></section>');
                var $dialog = $('#modal-holder section')
                    //.load(attr.openDialog)
                    //.html('<iframe style="border: 0px; text-align:center; height:100%; width:100%;" src="' + attr.openDialog + '" width="100%" height="100%"></iframe>')
                    .dialog({
                        autoOpen:false,
                        modal: true,
                        resizable: false,
                        closeOnEscape: true,
                        height:$(window).height(),
                        width:650,
                        title:scope.app_folder,
                        dialogClass: 'fixed-dialog',
                        show: {effect: 'fade', speed: 1000},
                        hide: {effect: 'fade', speed: 1000},
                        close: function () { $(this).remove(); },
                        buttons: { 
                            "Ok": function () {
                                $(this).dialog("close"); 
                            },
                            "Cancel": function () {
                                $(this).dialog("close"); 
                            } 
                        }
                    });
                //$dialog.dialog('open');
                //var holder = $('.ui-dialog-content').attr("ng-include", attr.openDialog);
                //$('.ui-dialog-content').html('<div ng-include="'+attr.openDialog+'"></div>');
                scope.template.modal = "'"+attr.openDialog+"'"
                $dialog.dialog('open')
                $compile(scope);
            }); 
        }
    };
})
.directive('tooltip', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            if(attrs.tooltip == 'html'){
                element.attr("data-html", true)
            }
            element.hover(function(){
                // on mouseenter
                element.tooltip('show');
            }, function(){
                // on mouseleave
                element.tooltip('hide');
            });
        }
    };
})
.filter('removal', [ function($scope) {
    return function (obj1, obj2) {
        var res = [];
        var obj = obj1.concat(obj2)
        for(var i  in obj){
            var ind = res.map(x=>x.externalId).indexOf(obj[i].externalId)
            if( ind === -1 ){
                res.push(obj[i])
            }
        }
        return res;
    };
}])
.factory('getApi', function($http, CONS) {
    return {
        call: function(tar) {
            return $http.get(CONS.httpApi+'/'+tar).then(function(response) {
                return response.data;
            });
        }
    };
})
.factory('$user', function ($q, $timeout, $http, CONS) {
    var dt = $http.get(CONS.appHttpMain + '/Acl.php', {
        headers: {
            'Cache-Control': 'no-cache'
        }
    })
    var user = {
        get: function () {
            var d = $q.defer();

//            $timeout(function () {
                d.resolve(dt);
//            }, 1);

            return d.promise;
        }
    };
    return user;
});
