var app = angular.module("app", [ 
        'ngRoute',
        'ui.bootstrap',
        'ngAnimate',
        'angular-loading-bar',
        'app.directives',
        'app.routes', 
        'app.users',
        'app.statistics',
        'app.profiles',
        'app.directories',
        'app.feedbacks',
        'app.contents',
        'pascalprecht.translate', 
        'ngTagsInput',
        'countTo',
        'chart.js',
        ]);
		
// CONSTANTS
var protocol = document.location.href.split('/')[0];
var app_folder = (document.location.hostname.indexOf('localhost') > -1) 
    || (document.location.hostname.indexOf('devzonia') > -1) ? 'admea/' : ''

var __ = this;

__.langs = ['en', 'de', 'fr', 'es', 'it', 'sv', 'no', 'da', 'nl'];    
__.AppUrl = protocol + '//' + document.location.hostname +'/'+app_folder
__.perpage = 20;
__.v = 20200101;
__.currLang = document.location.href.split(document.location.hostname+'/'+app_folder)[1].split('/')[0];

if(__.langs.indexOf( __.currLang ) == -1){
     window.location.href = protocol+'//'+document.location.hostname+'/'+app_folder+'en'
}


app.constant('CONS', {
    appHttpMain: protocol + '//' + document.location.hostname +'/'+app_folder+'Server',
    appApi: protocol + '//' + document.location.hostname +'/'+app_folder+'Server/Api.php',
    uberall: "https://uberall.com/",
	appHttp: protocol + '//' + document.location.hostname +'/'+app_folder+'Server/Controllers',
	appFolder: app_folder,
	serverUrl: 'http://' + document.location.hostname + app_folder
});


app.controller('MainAppCtrl', 
function($scope, $route, $routeParams, $rootScope, $compile, $timeout, $http, $filter, $location, $q, CONS, Schema, uploadFile_service, $translate, $document, $window  ) {
    
    $rootScope.isDisabled=true;
    $rootScope.perpage=__.perpage;
    $rootScope.modalId=-1;
    
    $rootScope.filesInfo={"photosMAIN":[], "photosLOGO":[], "photosSQUARED_LOGO":[], "photosLANDSCAPE":[], "photosPHOTO":[], "photosTemplate":[], "photosLP_LOGO":[], "photosLP_BACKGROUND":[], "photosLP_ABOUTUS1":[], "photosLP_ABOUTUS2":[], "photosLP_ABOUTUS3":[], "photosLP_PRODUCT":[], "photosLP_PHOTOS":[],};
    
    $rootScope.linksMissedActions = {
        "OPENINGHOURS":"profiles/basicdata?target=openinghours", 
        "ATTRIBUTES":"profiles/richdata?target=attributes", 
        "PHOTOS":"profiles/media?target=media", 
        "DESCRIPTION_SHORT":"profiles/basicdata?target=description_short"
    }
    $rootScope.menus = {
        "days":["", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"] ,
        "socialProfiles": [
            {"id":"1", "name":"INSTAGRAM"}, {"id":"2", "name":"YOUTUBE"}, {"id":"3", "name":"FOURSQUARE"}, {"id":"4", "name":"FACEBOOK"}, {"id":"5", "name":"LINKEDIN"}, {"id":"6", "name":"TWITTER"}, {"id":"7", "name":"XING"}, {"id":"8", "name":"PINTEREST"}, {"id":"9", "name":"VIMEO"}
        ], 
        "brands": [ 
            "Google", "Linkedin", "Google Maps", "Xing", "Yahoo", "Instagram", "You Tube", "Facebook", "Foursquare", "Bing"
        ],
        "services": [ 
            "SEO-Optimierung", "Web-Design", "Firmeneintragungen", "Webpräsenz-Analyse"
        ],
        "paymentOptions": [ 
            {"icn":"AMAZON", "name":"AMAZON"}, {"icn":"AMEX", "name":"AMEX"}, {"icn":"AMEXDIRECTDEBIT", "name":"AMEXDIRECTDEBIT"}, {"icn":"BITCOIN", "name":"BITCOIN"}, {"icn":"CASH", "name":"CASH"}, {"icn":"DISCOVER", "name":"DISCOVER"}, {"icn":"EBAY", "name":"EBAY"}, {"icn":"EC", "name":"EC"}, {"icn":"GIROPAY", "name":"GIROPAY"}, {"icn":"INVOICE", "name":"INVOICE"}, {"icn":"MAESTRO", "name":"MAESTRO"}, {"icn":"MASTERCARD", "name":"MASTERCARD"}, {"icn":"PAYPAL", "name":"PAYPAL"}, {"icn":"SOFORT", "name":"SOFORT"}, {"icn":"VISA", "name":"VISA"}, {"icn":"VISAELECTRON", "name":"VISAELECTRON"}, {"icn":"WESTERNUNION", "name":"WESTERNUNION"}, {"icn":"DBSPAYLAH", "name":"DBSPAYLAH"}, {"icn":"ALIPAY", "name":"ALIPAY"}, {"icn":"APPLEPAY", "name":"APPLEPAY"}, {"icn":"FAVEPAY", "name":"FAVEPAY"}, {"icn":"GOPAY", "name":"GOPAY"}, {"icn":"GRABPAY", "name":"GRABPAY"}, {"icn":"RABBITLINEPAY", "name":"RABBITLINEPAY"}, {"icn":"TRUEMONEY", "name":"TRUEMONEY"}, {"icn":"WECHATPAY", "name":"WECHATPAY"}, {"icn":"GOOGLEPAY", "name":"GOOGLEPAY"}
        ],
        "languages": [
            "Deutsch", "Englisch", "Afar", "Abkhazian", "Afrikaans", "Akan", "Albanian", "Amharic", "Arabic", "Aragonese", "Armenian", "Assamese", "Avaric", "Avestan", "Aymara", "Azerbaijani", "Bashkir", "Bambara", "Basque", "Belarusian", "Bengali", "Bihari", "Bislama", "Bosnian", "Breton", "Bulgarian", "Burmese", "Catalan", "Chamorro", "Chechen", "Chinese", "Church", "Chuvash", "Cornish", "Corsican", "Cree", "Czech", "Danish", "Maldivian", "Dutch", "Dzongkha", "English", "Esperanto", "Estonian", "Ewe", "Faroese", "Fijian", "Finnish", "French", "West", "Fula", "Georgian", "German", "Scottish", "Irish", "Galician", "Manx", "Greek", "Guarani", "Gujarati", "Haitian", "Hausa", "Hebrew", "Herero", "Hindi", "Hiri", "Croatian", "Hungarian", "Igbo", "Icelandic", "Ido", "Nuosu", "Inuktitut", "Interlingue", "Interlingua", "Indonesian", "Inupiat", "Italian", "Javanese", "Japanese", "Greenlandic", "Kannada", "Kashmiri", "Kanuri", "Kazakh", "Khmer", "Gikuyu", "Kinyarwanda", "Kyrgyz", "Komi", "Kongo", "Korean", "Kwanyama", "Kurdish", "Lao", "Latin", "Latvian", "Limburgish", "Lingala", "Lithuanian", "Luxembourgish", "Luba-Katanga", "Luganda", "Macedonian", "Marshallese", "Malayalam", "Māori", "Marathi", "Malay", "Malagasy", "Maltese", "Mongolian", "Nauruan", "Navajo", "Southern", "Northern", "Ndonga", "Nepali", "Nynorsk", "Bokmål", "Norwegian", "Chewa", "Occitan", "Ojibwe", "Odia", "Oromo", "Ossetian", "Punjabi", "Persian", "Pali", "Polish", "Portuguese", "Pashto", "Quechua", "Romansh", "Romanian", "Kirundi", "Russian", "Sango", "Sanskrit", "Sinhala", "Slovak", "Slovenian", "Northern", "Samoan", "Shona", "Sindhi", "Somali", "Sotho", "Spanish", "Sardinian", "Serbian", "Swazi", "Sundanese", "Swahili", "Swedish", "Tahitian", "Tamil", "Tatar", "Telugu", "Tajik", "Tagalog", "Thai", "Tibetan", "Tigrinya", "Tongan", "Tswana", "Tsonga", "Turkmen", "Turkish", "Twi", "Uyghur", "Ukrainian", "Urdu", "Uzbek", "Venda", "Vietnamese", "Volapük", "Welsh", "Walloon", "Wolof", "Xhosa", "Yiddish", "Yoruba", "Zhuang", "Zulu"
        ]
    };
    var setHours = function(){
        var res=[];
        var inc=-1;
        for(var i=0; i<24; i++){
            i>9 ? istwo = "" : istwo = "0"
            res[inc+1] = {"id":istwo + i + ":" + "00", "name":istwo + i + ":" + "00"}
            res[inc+2] = {"id":istwo + i + ":" + "15", "name":istwo + i + ":" + "15"}
            res[inc+3] = {"id":istwo + i + ":" + "30", "name":istwo + i + ":" + "30"}
            res[inc+4] = {"id":istwo + i + ":" + "45", "name":istwo + i + ":" + "45"}
            inc=inc+4
        }
        return res;
    }
    $rootScope.menus.hours = setHours();
    
	$rootScope.app_folder = CONS.appFolder;
    $rootScope.landingpage_path = window.location.host == "localhost" ? "http://localhost/LP" : "https://worknart.com";
	$rootScope.serverUrl = CONS.serverUrl;
	$rootScope.rec = {}

	
	var getOffset = function (el) {
		el = el.getBoundingClientRect();
		return {
			left: el.left + window.scrollX,
			top: el.top + window.scrollY
		}
	}
	
    $rootScope.deleteItm = function(obj, ind){
            return obj.splice(ind, 1)
//        if(obj.length>1){
//            return obj.splice(ind, 1)
//        }else{
//            return obj[0]=null
//        }
    }
    
    
    
    /////////// Auth ///////////////
	$rootScope.isAuthorized = function(){
		$timeout(function(){
            var pgInfo = $rootScope.pg;
            var allowed = ['login', 'logout'];
                                                 
            if($rootScope.in_array( pgInfo[0], allowed)){ return ; }

            if( ($rootScope.user.aclMap [ pgInfo[0] ][ pgInfo[1] ]*1 ) !== 1){
                __.opAlert("login-needed")
                $rootScope.referer = $location.path();
                $(".preloader-all").css({"opacity":"0", "zIndex":"-1"})
                return $rootScope.goTo('/login');
            }
            return true;
        }, 1)
	}
	$rootScope.isAuth = function(){
            var pgInfo = $rootScope.pg;
            var allowed = ['login', 'logout'];
                                                 
            if($rootScope.in_array( pgInfo[0], allowed)){ return ; }

            if( ($rootScope.user.aclMap [ pgInfo[0] ][ pgInfo[1] ]*1 ) !== 1){
                return false;
            }
            return true;
	}
	/////////// END Auth ///////////////
    
    
    
	/////////// Upload Photo ///////////////
    
    
    // Photos validation
    __.chkPhoto = function(img, rule){
        var msg = []
        if('jpg,png'.indexOf(__.getExt(img.type).toLowerCase()) == -1){
           msg.push(rule+' Photo format not support, only JPG, PNG allowed');
        }
        if(img.dim.w > 8000){
           msg.push(rule+' Photo is big, it should be smaller than 8000px');
        }
        if(img.dim.w < 711){
           msg.push(rule+' Photo is small, it should be bigger than 900px');
        }
        if(rule.indexOf('BACKGROUND') > -1){
            if(img.dim.w < img.dim.h){
               msg.push(rule+' Photo should be wide horizontal ');
            }
        }
        if(msg == ''){
            return true;
        }
        return msg.join('<br>');
    }
    
    
	__.uploadFile = function( file, fileName, deleteFile, pth){
        
        !fileName ? fileName='' : fileName;
        !deleteFile ? deleteFile='' : deleteFile;
        !pth ? pth='lp' : pth;
        
            return uploadFile_service.uploadFileToUrl(file, "./Server/ImagesHandler.php?fname="+fileName+"&del="+deleteFile+"&pth="+pth, fileName);
	};
	$rootScope.uploadFile = function( file, fileName, deleteFile, pth){
        
        !fileName ? fileName='' : fileName;
        !deleteFile ? deleteFile='' : deleteFile;
        !pth ? pth='lp' : pth;
        
        return __.uploadFile(file, fileName='', deleteFile='', pth);
    }
    
	__.uploadUrlFile = function( file, fileName, pth){
        
        !fileName ? fileName='' : fileName;
        !pth ? pth='lp' : pth;
        return $http.get("./Server/ImagesHandler.php?urlImg="+file+"&imgName="+fileName+"&pth="+pth)
	};
	/////////// End Upload Photo ///////////////
    
    
    
    
    /////////// Modal and Message ///////////////
    // for ok and cancel buttons
    var modalScope = $rootScope.$new();
    $rootScope.openModal = function (id, tmplt, ctrl, size) {
        
        !id ? id=-1 : id;
        !ctrl ? ctrl='MainAppCtrl' : ctrl;
        !size ? size='md' : size;
        !tmplt ? tmplt='Views/Layout/alert.php' : tmplt;
        
        $rootScope.modalTab='login';
        $rootScope.isDisabled=true;
        $rootScope.modalId = id;
        
        var parentElem = angular.element($document[0].querySelector('modal-container'));
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: tmplt,
            controller: ctrl,
            controllerAs: ctrl,
            scope: modalScope,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function () {
                  return ctrl.items;
                }
            }
        });

        modalScope.modalInstance = modalInstance;
        
        modalInstance.result.then(function (selectedItem) {
            ctrl.selected = selectedItem;
        }, function () {
            //$log.info('modal-component dismissed at: ' + new Date());
        });
    };
	
    $rootScope.cancel = function () {
        modalScope.modalInstance.dismiss('cancel');
    };
	
    $rootScope.ok = function () {
        modalScope.modalInstance.dismiss('cancel');
    };
    
    $rootScope.in_array = function (needle, haystack) {
        for(var i in haystack) {
            if(haystack[i] == needle) return true;
        }
        return false;
    }
    
	__.opAlert = function(msg, color){

        !color ? color='msg_white' : color;
        var error = '';
        if(msg[0].length > 1){
            var msgElm = msg[0];
            var error = msg[1]
        }else{
            var msgElm = msg
        }
        return $translate(msgElm).then(function(res){
            $rootScope.modalMsg = res;
            $rootScope.modalError = error;
            $rootScope.msgColor = color;
//            if(type == "modal"){
//                return $rootScope.openModal(null, 'Views/Layout/alert.php' )
//            }
            var tar = $("#msgHolder");
            if(tar.css("opacity") == "0"){
                tar.css({"visibility":"visible","opacity":"1", "padding-top":"10%", "z-Index":"999"})
            }else{
                tar.css({"visibility":"hidden", "opacity":"0", "padding-top":"0", "z-Index":"-10"})
                //tar.css()
            }
            
            //$('#messageModal').modal()
        });
    }
    $rootScope.opAlert = function(msg, color){
        
        !color ? color='msg_white' : color;
        
        __.opAlert(msg, color)
    }
    /////////// End Modal and Message ///////////////
    
    
    
    
    $rootScope.scrollTo = function(to){
        
        frm='html,body';
        !to ? to=0 : to;
        setTimeout(function(){
            var toElm = $location.search().target;
            if(toElm){
                to = $("#"+toElm).offset().top-200;
            }
            $(frm).animate({scrollTop: to+' '}, 500);
        },100)
    }
    __._v = function(v, els){
        !els ? els=0 : els;
        if(typeof v !== 'undefined'){return v;}else{return els;}
    }
	__._getUpdate = function(tbl){
        
        !tbl ? tbl='' : tbl;
        
		$timeout(function(){
			return document.getElementById('updt_'+tbl).click();
		}, 0);
	}
    __.keywordsFilter = function (val){
        val = val.replace(/[^A-Za-zا-ي\s]/g,'').replace(/[\r\n]/g, ' ');
        var valArr = val.split(" ")
        var res = []
        for(var i in valArr){
            if(valArr[i].length>3 && jQuery.inArray(valArr[i], res)<0){ 
                res.push(valArr[i]);
            }
        }
        return res;
    }
    __.setCvrBtn = function(tar ,param, icon){
        !icon ? icon = "plus" : icon;
		var elm = $( "#"+tar+ " span" );
		if(param==1){ 
			elm.html('<i class="fa fa-refresh fa-spin fa-fw"></i>');
			$("#"+tar.replace("_cvr","")).css("pointer-events", "none");
			$("#"+tar).attr("disabled", true);
		}else{ 
			elm.html('<i class="fa fa-'+ icon +'"></i>');
			$("#"+tar.replace("_cvr","")).css("pointer-events", "all");
			$("#"+tar).attr("disabled", false);	
		}
	}

	
    __.setCvr = function(tar, param){
		if(param==1){ 
            $(tar).addClass('showIt')
		}else{ 	
            $(tar).removeClass('showIt')
		}
	}
	$rootScope.goTo = function(path, ext){
        !ext ? ext=null : ext;
		
//		if(path=='-1'){	return $location.path( $rootScope.backStep );}
		if(path=='/--'){ return null; }
		if(path==''){ return '/'+__.currLang+$location.path('/');}
		if(ext!==null){ return window.location.href= '/'+__.currLang+path;} 
		var urlparams = path.split('?')
		if(urlparams[1]){
			var itms = urlparams[1].split('=')
			return $location.path( urlparams[0] ).search(itms[0], itms[1]);
		}
		return $location.path('/'+__.currLang+path)
	}
    
    $rootScope.isActive = function(tar, isSub){
        var page = !isSub ? $rootScope.currPage : $rootScope.currSubPage;
        if(tar === page){
            return 'currentUrl'
        }
        return '';
    }
    
    // set decmail number
    
    
    __.set_num = function(num){
        alert(Math.trunc(num))
        return parseInt(num)
    }
    
    //set social media parameters
    var set_share = function (tar){
        var pageUrl = document.URL;
        var title = document.title;
        var desc = $('meta[name=description]').attr("content");
        
        var url='';
        if(tar=='facebook'){url='https://www.facebook.com/sharer/sharer.php?u='+pageUrl}
        if(tar=='twitter'){url='https://twitter.com/home?status='+pageUrl}
        if(tar=='google-plus'){url='https://plus.google.com/share?url='+pageUrl}
        if(tar=='linkedin'){url='https://www.linkedin.com/shareArticle?mini=true&url='+pageUrl+'&title='+title+'&summary='+desc+'&source='+pageUrl}
        //if(tar=='pinterest'){url='https://pinterest.com/pin/create/button/?url='+pageUrl+'&media='+p+'&description='+p}
        if(tar=='email'){url='mailto:email?subject='+title+'&body='+desc+' \n\n'+pageUrl};
        return url
    }
	// open share window
    $rootScope.openWin = function (tar){
        var urlTar=set_share(tar);
        var newwindow;
        var myFeatures="directories=no, menubar=no, status=no, location=no, titlebar=no, toolbar=no, scrollbars=no, width=700, height=400, resizeable=no, top=10, left=10";
        var newwindow=window.open(urlTar,'newwindow',myFeatures).focus()
    }
    $rootScope.set_floor = function(val){
        return Math.floor(val);
    }
    __.goToTab = function(tar){
        !tar ? tar='' : tar;
		$timeout(function(){
			return document.getElementById('heading-'+tar).click();
		}, 1000);
    }
    $rootScope.goToTab = function(tar){
        return __.goToTab(tar)
    }
    __.openTab = function(tar){
        if(!$("#"+tar).hasClass('show')){
            $timeout(function(){
                return document.getElementById(tar+'_btn').click();
            }, 1000);
        }
    }
    $rootScope.openTab = function(tar){
        return __.openTab(tar)
    }
    $rootScope.set_disabled = function(obj, isInternal){
        
        !isInternal ? isInternal=false : isInternal;
        
        var res = 0;
        for(var i in obj){  res+=obj[i]*1 }
        if(isInternal){
            return res;
        }else{
            $rootScope.isDisabled=!res>0;
        }
    }
    $rootScope.set_img = function(image, tbl, replacement){
        
        !replacement ? replacement='uploadimg.png' : replacement;
        !tbl ? tbl='lp' : tbl;
        
        var img = image.indexOf('://')>-1 ? image : './Library/img/'+tbl+'_photos/'+image;
        return img;
    }
    $rootScope.set_tab = function(tab){
        $('#login').removeClass('active');
        $('#register').removeClass('active');
        $("#"+tab).addClass('active');
        $rootScope.modalTab = tab
    }
	$rootScope.set_slug = function(val){
        var newVal = val.trim().replace(/[^A-Z0-9ءأاإبتثجحخدذرزسشصضطظعغفقكلمنهوية]/ig, "-").toLowerCase();
        newVal = newVal.replace(/-{2,}/g,"-").replace(/^\-+|\-+$/g, '');
        if(newVal.charAt(0) == '-'){newVal = newVal.substr(1)}
        if(newVal.charAt(newVal.length-1) == '-'){newVal = newVal.substr(0,newVal.length-1)}
        return newVal;
	}
    $rootScope.set_rand = function (val) {
        !val ? val=5 : val;
        return Math.floor(Math.random() * Math.floor(val));
    }
    $rootScope.set_till_now = function(dt){
        var today = new Date();
        var form_date=new Date(dt)
        var difference=form_date > today ? form_date-today : today-form_date
        var diff_days=Math.floor(difference/(1000*3600*24))
        return diff_days;
    }
	$rootScope.set_date = function (dt, daysOfst) {
        !daysOfst ? daysOfst=0 : daysOfst;
	    var now = new Date();
        if(daysOfst != 0){
            var resdate = new Date();
            if(daysOfst>0){
                now = new Date(resdate.setDate(resdate.getDate() + daysOfst) ) ;
            }else{
                now = new Date(resdate.setDate(resdate.getDate() - Math.abs(daysOfst)) ) ;
            }
        }
	    var year    = now.getFullYear();
	    var month   = now.getMonth()+1; 
	    var day     = now.getDate();
	    var hour    = now.getHours();
	    var minute  = now.getMinutes();
	    var second  = now.getSeconds();
	    if(month.toString().length == 1) { var month = '0'+month; }
	    if(day.toString().length == 1) {   var day = '0'+day; }
	    if(hour.toString().length == 1) {  var hour = '0'+hour; }
	    if(minute.toString().length == 1) {var minute = '0'+minute; }
	    if(second.toString().length == 1) {var second = '0'+second; }
        
	    var res = year+'-'+month+'-'+day+' '+hour+':'+minute+':'+second; 
	    if(dt == 'onlydate'){  	res = year+'-'+month+'-'+day;   }; 
	    if(dt == 'string2'){  	res = new Date( year+'-'+month+'-'+day).toDateString().substr(4, 6);   };
        
        
	    return res;
	}
	$rootScope.getSubPage = function(){
        var tarpage = './Library/Views/Profiles/'+$rootScope.currSubPage+'.php'
        return tarpage;
    }
	
	__.getExt = function(fileext){
        var ext = fileext.split('/')[1];
		switch(ext){
			case 'jpg':
			case 'jpeg':
				return 'jpg';
			break;
			case 'plain':
				return 'txt';
			break;
			default:
				return ext;
		}
	}
    
    
    
	/////////// General CRUD /////////
    
    
    __.getApi = function(tar, dt, req){
        !req ? req='GET' : req=req;
        return $http({
            method : req,
            url    : CONS.appApi+'?r_method='+req+'&url_target_page='+tar,
            data   : dt
        })
    }
    $rootScope.getApi = function(tar, dt, req){
        return __.getApi(tar, dt, req);
    }
	//--------- GET RECORD ---------/
	__.getRec = function(id, tbl, ctrl){
        
        !id ? id=null : id;
        !tbl ? tbl=false : tbl;
        !ctrl ? ctrl=false : ctrl;
        
		var url = ctrl ? CONS.appHttp+'/'+ctrl : CONS.appHttpMain+'/DbHandler.php?r='+( tbl || $route.current.$$route.pg[0] )+'_one'
		return $http({
			method   : 'POST',
			url	     : url,
			data     : id
		 })
	};
	$rootScope.getRec = function(id, tbl, ctrl){
        
        !id ? id=null : id;
        !tbl ? tbl=false : tbl;
        !ctrl ? ctrl=false : ctrl;
        
		if(id==-1){return $rootScope.rec[tbl]={};}
		__.getRec(id, tbl, ctrl).then(function(req){
            if(req.data == -1){return ;}
			$rootScope.rec[tbl]=req.data[0];
		})
	};
    
	//--------- GET LIST ---------/
	__.getList = function(stat, tbl, obj, pg, ctrl, limit){
        
        !stat ? stat=-1 : stat;
        !tbl ? tbl=false : tbl;
        !obj ? obj=false : obj;
        !pg ? pg=false : pg;
        !ctrl ? ctrl=false : ctrl;
        !limit ? limit=0 : limit;
        
        
        var url = ctrl ? CONS.appHttp+'/'+ctrl : CONS.appHttpMain+'/DbHandler.php?r='+( tbl || $route.current.$$route.pg[0] )+'_list';
        
        var strt = ($location.search().pg||0) * $rootScope.perpage;
        var orderby = ($location.search().ordr||'');
//        var lng = $rootScope.currLangId || ($location.search().lng||'');
        var lng = false;
        var ofst =  strt +', '+ $rootScope.perpage;
        
        if(limit>0){ofst='0, '+limit; orderby=''}
        if(!lng){
            var dt = stat+', \''+orderby+'\', '+ofst
        }else{
            var dt = stat+', '+lng+', \''+orderby+'\', '+ofst;
        }
		return $http({
			method : 'POST',
			url	   : url,
			data   : dt
		 })
	};

	$rootScope.getList = function(stat, tbl, obj, pg, ctrl, limit){
        
        !stat ? stat=-1 : stat;
        !tbl ? tbl=false : tbl;
        !obj ? obj=false : obj;
        !pg ? pg=false : pg;
        !ctrl ? ctrl=false : ctrl;
        !limit ? limit=0 : limit;
        
        var tar = obj||tbl;
		$rootScope.list[tar]=[];
        
		__.getList(stat, tbl , obj, pg, ctrl, limit).then(function(req){
            if(req.data == -1){return ;}
			$rootScope.list[tar] = req.data;
		})
	};
	//--------- CREATE ---------/
	__.saveRec = function(id, tbl , obj, ctrl ){
        
        !id ? id=null : id;
        !tbl ? tbl=false : tbl;
        !obj ? obj=false : obj;
        !ctrl ? ctrl=false : ctrl;
        
		return Schema.get(obj||$rootScope.rec[tbl], tbl).then(function( dt ){
			return $http({
				method : 'POST',
				url	   : CONS.appHttpMain+(ctrl||'/DbHandler.php')+'?r='+( tbl || $route.current.$$route.pg[0] )+'_save',
				data   : dt.join(", ")
			})
		})
	};
	$rootScope.saveRec = function(id, tbl , obj, ctrl){
        
        !id ? id=null : id;
        !tbl ? tbl=false : tbl;
        !obj ? obj=false : obj;
        !ctrl ? ctrl=false : ctrl;
        
		__.saveRec(id, tbl, obj, ctrl).then(function(req){
            if(req.data == -1){
               __.opAlert('save-fail', 'msg', 'red'); 
            }else{
               __.opAlert('save-success', 'msg', 'green');
                if(goto){ $timeout(function(){ __.goTo(goto) }, 500); }
            }
			 __._getUpdate(tbl);
         })
	};
	//--------- DELETE RECORD ---------/
	__.delRec = function(id, tbl, ctrl){
        
        !id ? id=-1 : id;
        !tbl ? tbl=false : tbl;
        !ctrl ? ctrl=false : ctrl;
        
		return $http({
			method : 'POST',
			url	   : CONS.appHttpMain+(ctrl||'/DbHandler.php')+'?r='+( tbl || $route.current.$$route.pg[0] )+'_del',
			data   : id
		 })
	};
	$rootScope.delRec = function(id, tbl, ctrl){
        
        !id ? id=-1 : id;
        !tbl ? tbl=false : tbl;
        !ctrl ? ctrl=false : ctrl;
        
        __.opAlert('del-msg', 'confirm').then(function(){
            var currTable =  tbl ||$route.current.$$route.pg[0]
            __.delRec(id, currTable, ctrl ).then(function(req){
                if(req.data == -1){return ;}
                __._getUpdate(currTable);
            })
        })
	};
	//--------- DELETE PHOTO ---------/
	__.delPhoto = function(photo, path){
        
        !path ? path='lp' : path;
    
        return $http.get(CONS.appHttpMain+'/ImagesHandler.php?del='+photo+'&path='+path);
	};
	$rootScope.delPhoto = function(photo, path){
        
        !path ? path='lp' : path;
        
        __.opAlert('del-msg', 'confirm').then(function(){
            __.delPhoto(photo, path).then(function(req){
                if(req.data == -1){return ;}
                __._getUpdate(currTable);
            })
        })
	};
    
	/////////// General CRUD end /////////
	
	
    
    
    
    
	/////////// AutoComplete input ///////////////
	
	$rootScope.loadMenu = function(q, tar) {
//        var deferred = $q.defer();
//        deferred.resolve( $rootScope.menus[tar] );
//        var dtlist = deferred.promise;
        if(tar == 'days'){
            return $rootScope.menus[tar].filter(function(res) {
                return res.name.toLowerCase().indexOf(q.toLowerCase()) != -1;
            });
        }else if(tar == 'paymentOptions'){
            var res = $rootScope.menus[tar].filter(function(res) {
                return res.name.toLowerCase().indexOf(q.toLowerCase()) != -1;
            });
            if(res.length == 0){ return [{'icn':'card', 'name':q.toUpperCase()} ] };
            return res;
        }else{
            return $rootScope.menus[tar].filter(function(res) {
                return res.toLowerCase().indexOf(q.toLowerCase()) != -1;
            });
        }
    }
//    $scope.loadCountries = function($query) {
//        return $http.get('countries.json', { cache: true}).then(function(response) {
//          var countries = response.data;
//          return countries.filter(function(country) {
//            return country.name.toLowerCase().indexOf($query.toLowerCase()) != -1;
//          });
//        });
//    };
    
    
    
    
	$rootScope.loadTags = function(query, tar) {
        var defer = $q.defer();
        var url = '/categories&language='+__.currLang+'&q=';
        var obj = 'results.results'
        if(tar == 'places'){
            url = '/locations/places/&v='+$rootScope.user.userInfo.v+'&name=';
            obj = 'places'
        }
		__.getApi( url+query ).then(function(res){
            if(res.data.status == 'SUCCESS'){
                if(tar == 'places'){
                    defer.resolve(res.data.response.places);
                }else{
                    defer.resolve(res.data.response.results.results);
                }
            }else{
                defer.resolve(["Connect error"])
            }
        })
        return defer.promise;
	};
	$rootScope.addTag = function(tag){
	}
	$rootScope.removeTag = function(tag){
	}
	/////////// END AutoComplete input ///////////////
    
    
    
    
	/////////// LOCATION FUNCTIONS ( PROFILE ) ///////////////
    __.setOpeningHours = function(obj, stat){
        !stat ? stat=false : stat;
        var res = [];
        var days = [1,2,3,4,5,6,7];
        if(stat == 'forSave'){
            for(var i in obj){
                if( obj[i].val ){ res.push( obj[i] ) }
            }
        }else{
            for(var i in days){
                var ind = obj.map(x => x.dayOfWeek).indexOf(days[i])
                if( ind > -1  ){
                    obj[ind].val = true
                    res.push(obj[ind])
                }else{
                    res.push({"dayOfWeek":days[i], "from1":"09:00", to1:"17:00", val: false})
                }
            }
        }
        return res;
    }
    __.empty = function(val){
        if(val == '' || val == null){
            return true;
        }
        return false;
    }
    
    
    
    
    ////////////////////////////////////////////////////
    //////////////////GENERAL FUNCTIONS/////////////////
    ////////////////////////////////////////////////////    
    
	/////////// GET LOCATION ( PROFILE ) ///////////////
    __.getProfile = function(){
//        $rootScope.profile =  {"locations":{}};
        __.getApi( '/locations/'+user.managedLocations[0]+'&v='+__.v ,{headers: { 'Cache-Control' : 'no-cache' } }).then( function(res){
            $rootScope.profile = res.data.response.location;
            $rootScope.profile.openingHours = __.setOpeningHours( $rootScope.profile.openingHours )
            $scope.attributes_alt = res.data.response.location.attributes;
            
            // Preparing PHOTOS for show
            if($rootScope.profile){
                $rootScope.filesInfo["photosMAIN"]=[];
                $rootScope.filesInfo["photosLOGO"]=[];
                $rootScope.filesInfo["photosSQUARED_LOGO"]=[];
                $rootScope.filesInfo["photosLANDSCAPE"]=[];
                $rootScope.filesInfo["photosPHOTO"]=[];
                $rootScope.filesInfo["photosTemplate"]=[];
                angular.forEach( $rootScope.profile.photos, function(photo){
                    $rootScope.filesInfo['photos'+photo.type].push(photo.publicUrl)
                } )
            }
            
            // Set social media values
            var SM = []
            
            for( var i in $rootScope.menus.socialProfiles ){
                var ind = $rootScope.profile.socialProfiles.map(x=>x.type).indexOf( $rootScope.menus.socialProfiles[i].name );
                if(ind>-1){
                    SM.push($rootScope.profile.socialProfiles[ind]);
                }else{
                    SM.push({ "type": $rootScope.menus.socialProfiles[i].name, "url": "" });
                }
            }
            $rootScope.profile.socialProfiles = SM
            
            // Set Pyament logos
            var paymentOptions = []
            for( var i in $rootScope.profile.paymentOptions ){
                paymentOptions.push({"name":$rootScope.profile.paymentOptions[i], "icn":$rootScope.profile.paymentOptions[i].toLowerCase()})
            }
            $rootScope.profile.paymentOptions = paymentOptions
            
            // Bring Attributes menu
            __.getApi( '/categories/'+$rootScope.profile.categories[0]
                      +'/attributes&country=' +$rootScope.profile.country
                      +'&language='+__.currLang
                      +'&v='+__.v).then( function(attr){
                $scope.attributesList = attr.data.response.attributes;
            })
            
            // Preparing CATEGORIES
            if(res.data.response.location.categories.length > 0){
                var ids = res.data.response.location.categories.join(',');
                __.getApi( '/categories/&categories='+ids+'&language='+__.currLang)
                    .then( function(cat){
                    $scope.categories_alt = cat.data.response.results
                })
            }
//            var catArr = $rootScope.profile.categories
        })
    }
	/////////// END GET PROFILE ///////////////
    
	//Profile Completeness Average
    $rootScope.basicText = {"OPENINGHOURS":"Opening Hours", "ATTRIBUTES":"Attributes", "PHOTOS":"Photos", "DESCRIPTION_SHORT":"Description"}
    $rootScope.getCompleteness = function(){
        __.getApi( "/dashboard/profileCompleteness" ).then( function(res){
    //        $scope.CompletenessAverage.data= [
    //              ['Label', 'Value'],
    //              ['Completed', res.data.response.averageProfileCompleteness],
    //        ]
            $rootScope.CompletenessAverage = res.data.response
            //Profile Completeness
            __.getApi( "/locations/"+ user.managedLocations.join() +"/profileCompleteness&v="+user.v ).then( function(res){

                $rootScope.basicStatuses = "<span class='key_value_div'><div><b>Basic level</b></div>"
                var obj = res.data.response.completenessPerField[res.data.response.currentClassification]
                var orderArr = ["OPENINGHOURS", "DESCRIPTION_SHORT", "PHOTOS", "ATTRIBUTES"]
                var basicStatusesArr = []
                for(var k in obj){
                    var ind = orderArr.indexOf(k);
                    basicStatusesArr[ind]="<div>"+ $rootScope.basicText[k] +" <i class='fa fa-"+ ( obj[k] ? 'check greenText' : 'times redText' )+"'></i></div>"
                }
                $rootScope.basicStatuses += basicStatusesArr.join("");
                $rootScope.basicStatuses += "</span>"
                $rootScope.Completeness = res.data.response
            })
        })
    }
    
    
	
});



