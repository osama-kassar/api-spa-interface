var cpt = function(v){
	if(v !== ''){return v[0].toUpperCase() + v.slice(1)} 
	return v;
}
angular.module('app.routes', [])

.config(['$routeProvider','$locationProvider',
            function( $routeProvider, $locationProvider ) {

	// 1-allow access same routeDt info 	-- Example: /pages
	// 2-allow access params 				-- Example: /pages/1
	// 3-allow access single page 			-- Example: /page
	// 4-allow access param for single page	-- Example: /page/1
	
	$locationProvider.html5Mode(true);
	
	var routeDt = [
        // Front end
        {adrs:"statistics", tmp:null, ctrl:null, stat:"1", pg:"statistics", subpg:"", act:null},
        
        {adrs:"profiles", tmp:null, ctrl:null, stat:"1", pg:"profiles", subpg:"", act:null},
        {adrs:"profiles/basicdata", tmp:"Profiles/basicdata", ctrl:null, stat:"1", pg:"profiles", subpg:"basicdata", act:null},
        {adrs:"profiles/richdata", tmp:"Profiles/richdata", ctrl:null, stat:"1", pg:"profiles", subpg:"richdata", act:null},
        {adrs:"profiles/media", tmp:"Profiles/media", ctrl:null, stat:"1", pg:"profiles", subpg:"media", act:null},
        {adrs:"profiles/suggestions", tmp:"Profiles/suggestions", ctrl:null, stat:"1", pg:"profiles", subpg:"suggestions", act:null},
        {adrs:"profiles/landingpage", tmp:"Profiles/landingpage", ctrl:null, stat:"1", pg:"profiles", subpg:"landingpage", act:null},
        
        {adrs:"directories", tmp:null, ctrl:null, stat:"1", pg:"directories", subpg:"", act:null},
        
        {adrs:"feedbacks", tmp:null, ctrl:null, stat:"1", pg:"feedbacks", subpg:"", act:null},
        
        {adrs:"contents", tmp:null, ctrl:null, stat:"1", pg:"contents", subpg:"", act:null},
        
        {adrs:"", tmp:"Users/login", ctrl:"UsersController", stat:"1", pg:"login", subpg:"", act:null},
        {adrs:"login", tmp:"Users/login", ctrl:"UsersController", stat:"1", pg:"login", subpg:"", act:null},
        {adrs:"logout",	tmp:"Users/logout", ctrl:"UsersController", stat:"1", pg:"logout", subpg:"", act:null},
        {adrs:"loading",	tmp:"Users/loading", ctrl:"UsersController", stat:"1", pg:"login", subpg:"", act:null},
        {adrs:"settings",	tmp:"Users/settings", ctrl:"UsersController", stat:"1", pg:"settings", subpg:"", act:null},

        ];


angular.forEach(routeDt, function (itm) {  
        
        
		if(itm.tmp == null){ itm.tmp = cpt(itm.adrs)+'/'+itm.adrs }
		if(itm.ctrl == null){ var parts = itm.adrs.split('/'); itm.ctrl = cpt(parts[0])+'Controller' }

		if(itm.stat.indexOf('a') > -1 ){
			if(itm.adrs.indexOf('admin')>-1){itm.adrs=''}
			itm.adrs = 'admin/'+itm.adrs;
			itm.tmp = 'Admin/'+itm.tmp;
		}
        
		// 1-allow access same routeDt info
		if(itm.stat.indexOf('1') > -1 ){
			$routeProvider.when("/:lang/"+itm.adrs, { 
				templateUrl   : "Views/" + itm.tmp+".php", 
				controller 	: itm.ctrl,
				pg			: [itm.pg, (itm.act||'r'), itm.subpg],
                resolve: {
                    userDt: function($user) {
                        return $user.get();
                    }
                }
			})
		}
		
		// 2-allow access params 
		if( itm.stat.indexOf('2') > -1 ){	
			$routeProvider.when("/:lang/"+itm.adrs+"/:id", { 
				templateUrl : "Views/" + itm.tmp+".php", 
				controller : itm.ctrl ,
				pg			: [itm.pg, (itm.act||'r'), itm.subpg],
                resolve: {
                    userDt: function($user) {
                        return $user.get();
                    }
                }
			})
		}

		// 3-allow access single page 
		if(itm.stat.indexOf('3') > -1 ){	
			$routeProvider.when("/:lang/"+itm.adrs.substr(0, itm.adrs.length-1), { 
				templateUrl : "Views/" + itm.tmp.substr(0, itm.tmp.length-1) +".php", 
				controller 	: itm.ctrl ,
				pg			: [itm.pg, (itm.act||'c'), itm.subpg],
                resolve: {
                    userDt: function($user) {
                        return $user.get();
                    }
                }
			})
		}

		// 4-allow access param for single page
		if( itm.stat.indexOf('4') > -1 ){
			$routeProvider.when("/:lang/"+itm.adrs.substr(0, itm.adrs.length-1)+"/:id", { 
				templateUrl : "Views/" + itm.tmp.substr(0, itm.tmp.length-1) +".php", 
				controller 	: itm.ctrl ,
                controllerAs : 'Your',
                resolve: {
                    factory : checkLoginRedirect
                },
				pg			: [itm.pg, (itm.act||'u'), itm.subpg],
                resolve: {
                    userDt: function($user) {
                        return $user.get();
                    }
                }
			})
		}
		
	})
	
	$routeProvider.otherwise({
		redirectTo : "/"+__.currLang+"/",
        pg		   : ['login', '', []],
        resolve: {
            userDt: function($user) {
                return $user.get();
            }
        }
	});
}]);







// INISIATION ACL
app.run(function( $rootScope, $http, CONS, $route, $location, Menu, $translate, $window) {
    
    // Define variables
	$rootScope.user = {"aclMap":"", "userInfo":""};
	$rootScope.rec = {"categories":{},"users":{},"polls":{"options":{"_selected":null}},"options":{}}
	$rootScope.list = {"categories":[],"users":[],"polls":[],"options":[],"mmenu":[]}
    $rootScope.v = __.v;
    $rootScope.langs = __.langs;
    
    $rootScope.online = navigator.onLine;
      $window.addEventListener("offline", function() {
        $rootScope.$apply(function() {
          $rootScope.online = false;
        });
      }, false);

    $window.addEventListener("online", function() {
        $rootScope.$apply(function() {
          $rootScope.online = true;
        });
    }, false);

    
    
    
//    $http.get(CONS.appHttpMain+'/getIp.php').then(function(r){
//        $rootScope.userIp = r.data;
//    })
    
//    if(typeof $rootScope.list['latestPolls'] !== 'object'){
//        $rootScope.list['latestPolls']=[];
//        $http({
//			method : 'POST',
//			url	   : CONS.appHttpMain+'/DbHandler.php?r=polls_list',
//			data   : "-1, -1, "+__.langs_reverse[__.currLang]+", 'id:desc', 0, "+__.perpage
//        }).then(function(req){
//			$rootScope.list['latestPolls'] = req.data;
//		})
//    }
    
	$rootScope.$on('$routeChangeSuccess', function (currentRoute, previousRoute) {
        if ($route.current.pg) {
            $rootScope.pg = $route.current.pg;
            $rootScope.currPage = $route.current.pg[0]
            $rootScope.currSubPage = $route.current.pg[2];
            var currUrl = window.location.href.replace($rootScope.app_folder, "").split("/")
            if(__.currLang != currUrl[3] ){
                __.currLang = currUrl[3]
            }
            $translate.use(__.currLang);
            $rootScope.currUrl = $location.path();
            $rootScope.currLang = __.currLang;
//            $rootScope.currLangId = __.langs_reverse[$rootScope.currLang]
        }
		$http.get(CONS.appHttpMain+'/Acl.php' ,{headers: { 'Cache-Control' : 'no-cache' } }).then(function(res){
			$rootScope.user = res.data; 
            __.user = $rootScope.user.userInfo;
            if(!$rootScope.profile && $rootScope.user.userInfo !== null && $rootScope.user.userInfo !== ''){
                __.getProfile()
            }
            if(res.data.userInfo !== null){
               $http.defaults.headers.common.Authorization = res.data.userInfo.access_token
            }
		}).then(function(){
			$rootScope.isAuthorized();
		})
    });
    
	$rootScope.$on('$routeChangeStart', function (event, next) {
        if($rootScope.user.userInfo == null && (next.$$route.pg[0] !== 'login' || next.$$route.pg[0] !== 'logout') ){
            $location.path('/'+__.currLang+'/login')
        }
    });
});

// Preloader
app.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
//    cfpLoadingBarProvider.spinnerTemplate = '<div id="preloader"><a href><img src="Library/img/clock-preloader.gif"></a></div>';
    
    cfpLoadingBarProvider.spinnerTemplate = '';
    cfpLoadingBarProvider.spinnerTemplate = '<div class="preloader"><span class="fa fa-cog fa-spin fa-2x fa-fw"></span></div>';
    /*if(cfpLoadingBarProvider.$rootScope.stopPreloader){
    	cfpLoadingBarProvider.includeBar = false;
    	cfpLoadingBarProvider.includeSpinner = false;
    }*/
	//cfpLoadingBarProvider.parentSelector = '#mybody'
    	
}]);
app.config(['ChartJsProvider', function (ChartJsProvider) {
    // Configure all charts
    ChartJsProvider.setOptions({
        colors : [ "#3ee22e", "#03a1fc", "#ff8585" ,"#fde800" ]
      //animation: false,
      //responsive: false
    });
}]);
// localization
app.config(['$translateProvider', function ($translateProvider) {
	$translateProvider
	.useStaticFilesLoader({
		prefix: './Views/Layout/Locale/',
	    suffix: '.json',
        type: "application/json"
	})
    ///.useMissingTranslationHandlerLog()
	.preferredLanguage( __.currLang )
    .useSanitizeValueStrategy('escape')
    .useLoaderCache(true)
    .forceAsyncReload(true);
}]);
app.config(function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
});
