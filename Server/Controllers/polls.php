<?php
require_once('../DbHandler.php');

// parameters
$_content = file_get_contents('php://input');
$_id = isset($_GET['id']) ? $_GET['id'] : $_content;

// polls list
$get1 = new dbHandler();
$polls =  @$get1->doRequest("polls_list(". $_id .")");

for($i=0; $i<count($polls); $i++){
    // options list
    $get2 = new dbHandler();
    $options = @$get2->doRequest("options_list(". $polls[$i]["id"] .", '', 0, 999)");
    if(!empty($options)){
        $polls[$i]['options']=$options;
    }
}

echo json_encode ( $polls ); die();
/*echo '<pre>';
print_r ( $polls );
echo '</pre>';*/