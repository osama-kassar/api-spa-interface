<?php
    $dt = json_decode('{
        "site_main_title":"Admea",
        "_title":"Admea Online Solutions GmbH",
        "page_title":"",
        "_description":"Admea - Admea Online Solutions GmbH",
        "_keywords":"",
        "home_photo":"Library/img/meta_home.jpg",
        "_photo":"Library/img/meta_home.jpg"
        }', true);
    
    $protocol = @$_SERVER["HTTPS"] == "on" ? "https" : "http";
    $app_folder = strpos("localhost", $_SERVER['HTTP_HOST'])>-1 || strpos("devzonia.com", $_SERVER['HTTP_HOST'])>-1 ? '/admea' : '';

    $mainDt = [
        "site_main_title"=>"AdMea",
        "server_url"=> $protocol . '://' . $_SERVER['SERVER_NAME'] . $app_folder .'/',
        "current_url"=>$protocol . '://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'],
    ];
    $page = explode('/', $_SERVER['REQUEST_URI']);

    switch( urldecode( $page[count($page)-1] ) ){
        case 'home':
            $elm = 'home';
            break;
        default:
            $elm = '';
    }
    
    $metaDt =  [
        "_title"=>urldecode( $dt[ $elm.'_title' ] ),
        "_description"=>urldecode($dt[ $elm.'_description' ]),
        "_keywords"=>urldecode($dt[ $elm.'_keywords' ]),
        "_photo"=>urldecode($dt[ $elm.'_photo' ])
        ];
?>