<?php


//print_r ( $_FILES ); die();

class ImagesHandler {
	var $allowed_ext = array(
							 'images'=>array('jpg','jpeg','gif','png'),
							 'media'=>array('swf','flv'),
							 'doc'=>array('doc','pdf', 'itt', 'rtf', 'txt')
							 );
	var $max_upload_size = 3000;// per kilobyte
	var $min_upload_size = 1;
	var $photosname = array();
	var $Error_Msg = array();
	
	function chk($file, $thumb){
		list($width, $height) = getimagesize($file['tmp_name']);
		for($i=0; $i<count($thumb); $i++){
			if($thumb[$i]['doThumb']){
				if( $thumb[$i]['w'] > $width ||  $thumb[$i]['h'] > $height){
					$this->Error_Msg['d'] = 'Photo dimensions too small!';
				}
			}
		}
		$inarray = false;
		foreach($this->allowed_ext as $arr){
			if( in_array( $this->getExt($file) , $arr ) ){
				$inarray = true;
			}
		}
		if(!$inarray){
			$this->Error_Msg[] = 'File type not supported!';
		}
		if( ($file['size'] / 1024) > $this->max_upload_size  ){
			$this->Error_Msg[] = 'File size too big!';
		}
		if( ($file['size'] / 1024) < $this->min_upload_size  ){
			$this->Error_Msg[] = 'File size too small!';
		}
		if(!empty( $this->Error_Msg )){
			return false;
		}else{
			return true;
		}
	}
	
    function url_uploader($path, $file, $name, $thumb){
        @$rawImage = file_get_contents($file);
        if($rawImage) {
            if(file_put_contents($path.'/'.$name, $rawImage) ){
				for($i=0; $i<count($thumb); $i++){
					if($thumb[$i]['doThumb']){
						$this->resizer($path.'/'.$name, $path.'/'.$thumb[$i]['dst'].'/'.$name, $thumb[$i]['w'], $thumb[$i]['h'], 0);
					}
				}
                return true;
            }
        } else {
            return false;
        }
    }
    
    function uploader($path, $file, $name, $thumb){
		strlen($name) < 3 ?  $newName = @date('ymd').@time().$name.'.'.$this->getExt($file) : $newName = $name .'.'. $this->getExt($file);
		//if($this->chk($file, $thumb)){
			if(move_uploaded_file($file['tmp_name'], $path.'/'.$newName)) {
				$this->photosname[]=$newName;
				for($i=0; $i<count($thumb); $i++){
					if($thumb[$i]['doThumb']){
						$this->resizer($path.'/'.$newName, $path.'/'.$thumb[$i]['dst'].'/'.$newName, $thumb[$i]['w'], $thumb[$i]['h'], 0);
					}
				}
				$this->Error_Msg=[];
				//echo json_encode($this->photosname);
                return true;
			}else{
				$this->Error_Msg[] = 'File upload fail!';
				return false;
			}
		//}
	}
	
	function resizer($src, $dst, $width, $height, $crop=0){
		if(!list($w, $h) = getimagesize($src)){
			$this->Error_Msg[] = 'Dimensions not correct!'; 
			return false;;
		}
		$type = strtolower(substr(strrchr($src,"."),1));
		if($type == 'jpeg') $type = 'jpg';
		switch($type){
			case 'bmp': $img = @imagecreatefromwbmp($src); break;
			case 'gif': $img = @imagecreatefromgif($src); break;
			case 'jpg': $img = @imagecreatefromjpeg($src); break;
			case 'png': $img = @imagecreatefrompng($src); break;
			default : $this->Error_Msg[] = 'This file type not supported!'; return false;
			break;
		}
		
		// resize
		if($crop){
			if($w < $width or $h < $height){
				$this->Error_Msg[] = 'File is too small!';
				return false;;
			}
			$ratio = max($width/$w, $height/$h);
			$h = $height / $ratio;
			$x = ($w - $width / $ratio) / 2;
			$w = $width / $ratio;
		}else{
			if($w < $width and $h < $height){
				$this->Error_Msg[] = 'File is too small!';
				return false;;
			}
			$ratio = min($width/$w, $height/$h);
			$width = $w * $ratio;
			$height = $h * $ratio;
			$x = 0;
		}
		
		$new = imagecreatetruecolor($width, $height);
		
		// preserve transparency
		if($type == "gif" or $type == "png"){
			imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
			imagealphablending($new, false);
			imagesavealpha($new, true);
		}
		
		imagecopyresampled($new, $img, 0, 0, $x, 0, $width, $height, $w, $h);
		
		switch($type){
			case 'bmp': imagewbmp($new, $dst); break;
			case 'gif': imagegif($new, $dst); break;
			case 'jpg': imagejpeg($new, $dst); break;
			case 'png': imagepng($new, $dst); break;
		}
		return true;
	}
	
	function getExt($file){
		$fileext = explode('/',$file['type'])[1];
		switch($fileext){
			case 'jpg':
			case 'jpeg':
				return 'jpg';
			break;
			default:
				return $fileext;
		}
	}
	
	public function deleteFile($path, $img){
		@unlink($path.'/'.$img);
		@unlink($path.'/thumb/'.$img);
		@unlink($path.'/middle/'.$img);
        return true;
	}
}


//var_dump($_FILES);die();

$do = new ImagesHandler();

$_fname = !empty($_GET['fname']) ? $_GET['fname'] : '';
$_del = !empty($_GET['del']) ? $_GET['del'] : '';
$_pth = !empty($_GET['pth']) ? $_GET['pth'] : 'lp';
$_obj = !empty($_FILES['file']) ? 'file' : 'upload';

$_urlImg = !empty($_GET['urlImg']) ? $_GET['urlImg'] : '';
$_imgName = !empty($_GET['imgName']) ? $_GET['imgName'] : '';


$msg=[];
if(!empty($_FILES[$_obj]['name'])){
    $thumb_params = array(
        array('doThumb'=>true, 'w'=>650, 'h'=>650, 'dst'=>'middle'),
        array('doThumb'=>true, 'w'=>250, 'h'=>250, 'dst'=>'thumb')
        );
    if($do->uploader('../Library/img/'.$_pth.'_photos', $_FILES[$_obj], $_fname, $thumb_params)){
        $msg = ["status"=>"SUCCESS", "uploadedFileName"=>$do->photosname[0] ];
    }else{
        $msg = ["status"=>"FAIL", "uploadedFileName"=>$do->photosname[0] ];
    }
}
if(!empty( $_urlImg ) ){
    
    $thumb_params = array(
        array('doThumb'=>true, 'w'=>650, 'h'=>650, 'dst'=>'middle'),
        array('doThumb'=>true, 'w'=>250, 'h'=>250, 'dst'=>'thumb')
        );
    if($do->url_uploader('../Library/img/'.$_pth.'_photos', $_urlImg, $_imgName, $thumb_params)){
        $msg = ["status"=>"SUCCESS", "uploadedFileName"=>$do->photosname[0] ];
    }else{
        $msg = ["status"=>"FAIL", "uploadedFileName"=>$do->photosname[0] ];
    }
    
    
//    $url_upload = $do->url_uploader($_urlImg, '123456789');
//    if($url_upload){
//        echo json_encode(["status"=>"SUCCESS", "file"=>$url_upload]);
//    }
}

if(strlen( $_del ) > 3){
    $deleteThumb = $do->deleteFile('../Library/img/'.$_pth.'_photos', $_del);
    if($deleteThumb){
        $msg = ["status"=>"SUCCESS", "deletedFile"=>$deleteThumb ];
    }else{
        $msg = ["status"=>"FAIL", "deletedFile"=>$deleteThumb ];
    }
}

echo !empty($msg) ? json_encode($msg) : "";
?>