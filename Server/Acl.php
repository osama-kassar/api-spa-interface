<?php 


class Acl{
	
	public $content;
	public $usersRoles;
	
	public function __construct() {
		$this->content = json_decode ( file_get_contents('php://input') );
        $this->usersRoles = [
            "visitor"=>[ 
                "statistics"=>  ["c"=>0, "r"=>0, "u"=>0, "d"=>0, "a"=>0],
                "profiles"=>    ["c"=>0, "r"=>0, "u"=>0, "d"=>0, "a"=>0],
                "directories"=> ["c"=>0, "r"=>0, "u"=>0, "d"=>0, "a"=>0],
                "feedbacks"=>   ["c"=>0, "r"=>0, "u"=>0, "d"=>0, "a"=>0],
                "contents"=>    ["c"=>0, "r"=>0, "u"=>0, "d"=>0, "a"=>0],
                "settings"=>    ["c"=>0, "r"=>0, "u"=>0, "d"=>0, "a"=>0],
            ],
            "user"=>[ 
                "statistics"=>  ["c"=>1, "r"=>1, "u"=>1, "d"=>1, "a"=>1],
                "profiles"=>    ["c"=>1, "r"=>1, "u"=>1, "d"=>1, "a"=>1],
                "directories"=> ["c"=>1, "r"=>1, "u"=>1, "d"=>1, "a"=>1],
                "feedbacks"=>   ["c"=>1, "r"=>1, "u"=>1, "d"=>1, "a"=>1],
                "contents"=>    ["c"=>1, "r"=>1, "u"=>1, "d"=>1, "a"=>1],
                "settings"=>    ["c"=>1, "r"=>1, "u"=>1, "d"=>1, "a"=>1],
            ]
        ];    
	}
	
	public function checkSession( $rl='' ){
		if(!isset( $_SESSION )){ session_start(); }
        $domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : $_SERVER['HTTP_HOST'];
        switch($rl){
            case 'user':
                if( !empty($this->content->access_token) ){
                    $_SESSION['udata']= [
                        'userInfo' =>  $this->content,
                        'aclMap' =>  $this->usersRoles[ 'user' ]
                    ];
                    setcookie("uberallAccessCookie", $this->content->access_token, (time()+3600)*24, "/", $domain, false);
                }
                break;
                
            case 'visitor':
                $_SESSION['udata']= [
                    'userInfo' =>  null,
                    'aclMap' =>  $this->usersRoles[ 'visitor' ]
                ];
                setcookie("uberallAccessCookie", null, -1, '/'); 
                setcookie("uberallRememberMe", null, -1, '/'); 
                break;
                
            default:
            case '':
                if( empty( $_COOKIE['uberallAccessCookie'] ) || empty($_SESSION['udata']['userInfo']) ){
                    return $this->checkSession('visitor');
                }else{
                    return $this->checkSession('user');
                }
                break;
        }
		return $_SESSION['udata'];
	}
}

$do = new Acl();
echo json_encode( $do->checkSession( @$_GET['rl'] ) ); die();

?>