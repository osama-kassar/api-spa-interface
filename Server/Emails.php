<?php 

include_once 'DbHandler.php';

class Emails extends DbHandler{
	
	public function __construct() {
		$this->content = json_decode( file_get_contents('php://input'), true);
	}
    
    private function _replacer($body, $obj){
        $body_arr = explode("[]", $body);
        for($i=1; $i<count( $body_arr ); $i=$i+2){
            $new_val = $obj[ $body_arr[$i] ];
            $body_arr[$i] = $new_val;
        }
        return implode( " ", $body_arr );
    }
    
	public function sendEmails( $conds, $type='bulk' ){
        $header_array = [
            "MIME-Version: 1.0",
            "Content-type: text/html; charset=UTF-8",
            "From: noreply@devzonia.com",
            "X-Mailer: php",
            "Reply-To: noreply@devzonia.com"
        ];
        $headers = implode("\r\n", $header_array);
        
        if($type == 'loop'){
            foreach($this->content['dt'] as $itm){
                $msg = $this->_replacer( $this->content['msg'], $itm );
                echo $msg;
                mail(
                    trim( $itm['email_address'] ).', info@devzonia.com', 
                    $this->content['subject'] , 
                    $msg , 
                    $headers
                );
            }
        }else{
            $tolist_array=[];
            foreach($this->content['dt'] as $itm){
                if(!empty( $itm['email_address'] ) ){
                    $tolist_array[] = trim($itm['email_address']);
                }
            }
            $tolist = implode(", ", $tolist_array);
            if( mail($tolist.', info@devzonia.com', $this->content['subject'], $this->content['msg'], $headers) ){
                echo 1;
            }else{
                echo 0;
            }
        }
	}
}

$_conds = isset($_GET['conds']) ? $_GET['conds'] : '';
$_type = isset($_GET['type']) ? $_GET['type'] : '';

$do = new Emails();
$do->sendEmails( $_conds, $_type );

?>