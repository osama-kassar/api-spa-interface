<?php 
class dbHandler{
	
	public function __construct() 
    {
		$this->content = file_get_contents('php://input');
		if($_SERVER['HTTP_HOST']!=='localhost'){
            $this->conn = mysqli_connect('localhost', 'devzonia_admea', '--9999Pass--', 'devzonia_admea') or die($this->conn->connect_error);
            //var_dump($this->conn);
        }else{
            $this->conn = mysqli_connect('localhost', 'root', '', 'admea') or die($this->conn->connect_error);
        }
		mysqli_set_charset($this->conn ,"utf8");
        
        $http_origin = @$_SERVER['HTTP_ORIGIN'];
        
        if (in_array($http_origin, ["https://worknart.com", "http://localhost", "https://devzonia.com"])){  
            header("Access-Control-Allow-Origin: $http_origin");
        }
	}
    
	public function _get_column_names($table) 
    {
		$sql = 'DESCRIBE '.$table;
		$res = mysqli_query($this->conn, $sql);
		$rows = [];
		while($row = mysqli_fetch_assoc($res)) {
			$rows[] = [$row['Field'], $row['Type']];
		}
		return $rows;
	}
    
	
	public function getLandingPage($uid) 
    {
        $res = ["news"=>[], "products"=>[]];
        $q_landpage = mysqli_query($this->conn, "SELECT * FROM landingpages WHERE user_id = '{$uid}'");
        if( !empty(mysqli_error($this->conn)) ){ return print_r(mysqli_error($this->conn)); }
        $sections = ["", "header", "aboutus", "products", "news", "photos", "contacts", "pages", "settings", "configs"];
            
        if($q_landpage->num_rows>0){
            while($row = @mysqli_fetch_assoc($q_landpage)) {
                $res["configs"] = $row;

                $q_content = mysqli_query($this->conn, "SELECT * FROM contents WHERE landingpage_id = ".$res["configs"]['id']);

                $q_product = mysqli_query($this->conn, "SELECT * FROM products WHERE landingpage_id = ".$res["configs"]['id']);

                $q_news = mysqli_query($this->conn, "SELECT * FROM news WHERE landingpage_id = ".$res["configs"]['id']);

                while($row2 = @mysqli_fetch_assoc($q_content)) {
                    $row2['content_object'] = json_decode($row2['content_object'], true);
                    $res[ $sections[$row2['section_id']] ] = $row2;
                }
                while($row3 = @mysqli_fetch_assoc($q_product)) {
                    array_push($res[ 'products' ] , $row3);
                }
                while($row4 = @mysqli_fetch_assoc($q_news)) {
                    $row4["news_photos"] = explode(",", $row4["news_photos"]);
                    array_push($res[ 'news' ] , $row4);
                }
            }
        }else{
            return -1;
        }
        
        return $res;
    }
	
	public function doRequest($request='')
    {
		$params = strpos($request, '(')>-1 ? "" : "(". $this->content .")";
		$q = mysqli_query($this->conn, "call $request $params");
        if( !empty(mysqli_error($this->conn)) ){ return print_r(mysqli_error($this->conn)); }
		$res = [];
        while($row = @mysqli_fetch_assoc($q)) {
            $res[] = $row;
        }
		return $res;
	}
}


//get tables schama to frontend
$_schema = isset($_GET['s']) ? $_GET['s'] : '';
if( !empty($_schema) ){ 
	$do = new dbHandler();
    echo json_encode( $do->_get_column_names($_schema) ); die();
}

//call stored procedure with params
$_request = isset($_GET['r']) ? $_GET['r'] : '';
if( !empty( $_request ) ){
	$do = new dbHandler();
	echo json_encode( $do->doRequest( $_request ) );die();
}

//call landingpage with params
$_lp = isset($_GET['lp']) ? $_GET['lp'] : '';
if( !empty( $_lp ) ){
	$do = new dbHandler();
	echo json_encode( $do->getLandingPage( $_lp ) );die();
}



?>