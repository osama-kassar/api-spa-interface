<?php

class Api
{
    public function __construct() 
    {
        $this->content = json_decode( file_get_contents('php://input') ,true );
        $this->url = "https://uberall.com/api";
        $this->pkey = "l3MslTOStc8qy4yPFYaXN5o1dEFZRJEa9FTZytsh6aRJsUPwurLWfT3ozcp8nUNt";
    }

    public function call($page, $requestMethod)
    {
        if($page == '/users/login'){
            $dt = $this->toImplode($this->content);
        }else{
            $dt = file_get_contents('php://input');
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url . $page . $this->setParams(),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $requestMethod,
            CURLOPT_POSTFIELDS => $dt,
            CURLOPT_HTTPHEADER => $this->setHeaders($page, $requestMethod)
        ));
        $response = curl_exec($curl) ;
        curl_close($curl);
        echo $response;
    }



    /*************** INTERNAL HANDLING FUNCTIONS ******************/
    private function toImplode($array)
    {
        if(!is_array($array)){return "";}
        $res = [];
        foreach($array as $k=>$v){
            if(is_array( $v )){
                foreach($v as $itm){
                    $res[] = $k.'='.$itm;
                }
            }else{
                $res[] = $k.'='.$v;
            }
        }
        return implode("&", $res);
    }
    private function setParams()
    {
        $params='';
        if(count($_GET) > 1){
            $urlWithoutPage = $_GET;
            unset($urlWithoutPage['url_target_page']);
            unset($urlWithoutPage['r_method']);
            if(count($urlWithoutPage) > 0){
                $params = "?".$this->toImplode( $urlWithoutPage );
            }
        }
        return $params;
    }

    private function setHeaders($page, $requestMethod)
    {
        if( $requestMethod == 'POST' || $requestMethod == 'PATCH' ){
            $headers = [
                "Content-Type: application/json; charset=utf-8;",
                "privateKey: ".$this->pkey,
            ];
        }
        if( $requestMethod == 'DELETE' ){
            $headers=[
                "Content-Type: application/x-www-form-urlencoded",
                "privateKey: ".$this->pkey,
            ];
        }
        if( $requestMethod == 'GET' ){
            $headers=[
                "Cookie: uberallAccessCookie=".@$_COOKIE['uberallAccessCookie'],
            ];
        }
        if($page == '/users/login'){
            $domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : $_SERVER['HTTP_HOST'];
            if(!empty( $this->content['remember_me'] )){
                setcookie("uberallRememberMe", $this->content['remember_me'], (time()+3600)*24, "/", $domain, false); 
            }
            $headers = [
                'Content-Type: application/x-www-form-urlencoded'
            ];
        }
        return $headers;
    }

}  
    

$_target = empty( $_GET['url_target_page'] ) ? "" : $_GET['url_target_page'];
$_method = empty( $_GET['r_method'] ) ? "GET" : $_GET['r_method'];

if(!empty($_target)){
    $API = new Api();
    return $API->call( $_target, $_method );
}else{
    echo 'Wrong URL!';
}
?>