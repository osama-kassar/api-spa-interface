<style>

.photoPaginator a{
	border:1px solid var(--lightblue); 
	text-decoration:none;
	display:inline-block;
	margin:2px;
	height:20px;
	width:20px;
	line-height:20px;
	text-align:center;
}
.photoPaginator .active{
	background:var(--lightblue);
	font-weight:bold;
}
.photolist span{
	width:140px;
	height:110px;
	display:inline-block;
	overflow:hidden;
	margin:4px;
	border:1px solid var(--lightblue);
	font-size:10px;
}
.photolist img{
	width:100%;
	height:70px;
}
</style>


<?php
function readFolder($dir, $page=0){
    if(!empty($_GET['p'])){
        $page = $_GET['p'];
    }else{
        $page = 0;
    }
    $res=[]; $inc=0; $offset=50;
    if (is_dir('..'.$dir)) {
        if (chdir('..'.$dir)) {
            array_multisort(array_map('filemtime', ($files = glob("*.*"))), SORT_DESC, $files);
            for ($i=($page*$offset); $i<($page+1)*$offset; $i++) {
                if(!empty($files[$i])){
                    if($files[$i] != '..' && $files[$i] != '.' && $files[$i] != 'thumb'){
                        $inc++;
                        $res[] = $files[$i];
                    }
                }
            }
            $pages = ceil(count($files) / $offset);
            echo "<div class='photoPaginator'>";
            for($j=0; $j<$pages; $j++){
                if($j == $page){
                    echo '<a class="active">'.($j+1).'</a>';
                }else{
                    echo '<a href="?p='.$j.'">'.($j+1).'</a>';
                }
            }
            echo "</div>";
            //krsort($res);
            //debug($res);
            
            
            $protocol = strpos($_SERVER['SERVER_PROTOCOL'], 'HTTPS') > -1 ? 'https://' : 'http://';
            $app_folder = strpos("localhost", $_SERVER['HTTP_HOST'])>-1 || strpos("devzonia.com", $_SERVER['HTTP_HOST'])>-1 ? '/admea' : '';
            $website_url = $protocol.$_SERVER['HTTP_HOST'].$app_folder;
            
            echo '<div>';
            foreach($res as $photo){
                echo '
                <span>
                    <a href="javascript:setLink(\''.$website_url.$dir.$photo.'\');">
                        <img src="'.$website_url.$dir.$photo.'">
                    </a>'.$photo.'
                </span>';
            }
            echo '</div>';
        }
    }else{
        echo 'not dir';
    }
}
?>


<div id="upload-wrapper" class="photolist">
	<?php echo readFolder("/Library/img/articles_photos/thumb/");?>   
</div>

<script>
/**************** add photo to CFEditor from selector *******************/
function setLink(url) {
	window.opener.CKEDITOR.tools.callFunction(1, url);
	window.close();
}
/**************** end add photo to CFEditor from selector *******************/
</script>