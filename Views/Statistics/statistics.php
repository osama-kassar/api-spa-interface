
<section class="container" ng-init="
        datePicker.changeMinAndMaxDates(); 
        directory_type='all';
        getRatings();
        getFeeds();
        getGMetrics('G_Impressions');
        getGMetrics('G_Clicks');
        getGMetrics('G_Photos');
        getGMetrics('G_Queries');
        getListings();
    ">
        <!-- 
        getCompleteness();
        getDuplicates(); 
        getVisibilityIndexs();-->
    <div class="col">
        <h2>{{'statistics_p'|translate}}</h2>
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    
                    <div class="col-md-6 card_style">
                        <div ng-include="'Views/Layout/Completeness.php'"></div>
                    </div>
                    
                    <div class="col-md-6 card_style">
                        <div ng-include="'Views/Layout/Listings.php'"></div>
                    </div>
                    
<!--
                    <div class="col-md-6 card_style">
                        <div>
                            <h2>{{'Duplicates'|translate}} 
                                <i class="fa fa-info-circle" tooltip title="{{'Duplicates_desc'|translate}}"></i>
                            </h2>
                            <div google-chart chart="Duplicates"></div>
                            <canvas id="doughnut" class="chart chart-doughnut"
                              chart-data="Duplicates.data" chart-labels="Duplicates.labels">
                            </canvas>
                            <div></div>
                        </div>
                    </div>
-->
                    
                    
                    
                    
                    
                    <div class=" card_style">
                        <nav class="col-md-12 navbar bg-light myNavBar mb-5">
                            <span class="navbar-brand" href="#"><i class="fa fa-filter"></i> {{'filter'|translate}}</span>

                            <form class="form-inline " >
                                <div class="input-group right-rounded mr-sm-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" >
                                            <b>{{'from'|translate}}</b>&nbsp; <i class="fa fa-calendar"></i> 
                                        </span>
                                    </div>
                                    <input type="text" class="form-control datepicker "
                                        placeholder="{{'from' | translate}}" 
                                        uib-datepicker-popup="yyyy-MM-dd"
                                        datepicker-options="datePicker.fromOptions" 
                                        is-open="startDate_is_opened" 
                                        ng-model="filter.from" 
                                        ng-focus="startDate_is_opened=true" 
                                        ng-click="startDate_is_opened=true" 
                                        onkeydown="return false" 
                                        ng-change="datePicker.changeMinAndMaxDates()"

                                        show-button-bar="false"
                                        popup-placement="auto bottom-right"
                                        date-handler />
                                </div>

                                <div class="input-group right-rounded mr-sm-2 ">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" >
                                            <b>{{'to'|translate}}</b>&nbsp; <i class="fa fa-calendar"></i> 
                                        </span>
                                    </div>
                                    <input type="text" class="form-control datepicker"
                                        placeholder="{{'to' | translate}}" 
                                        uib-datepicker-popup="yyyy-MM-dd"
                                        datepicker-options="datePicker.toOptions" 
                                        is-open="endDate_is_opened" 
                                        ng-model="filter.to" 
                                        ng-focus="endDate_is_opened=true" 
                                        ng-click="endDate_is_opened=true" 
                                        onkeydown="return false" 
                                        ng-change="datePicker.changeMinAndMaxDates()"

                                        popup-placement="auto bottom-right"
                                        show-button-bar="false"
                                        date-handler />
                                </div>

                                <div class="input-group right-rounded mr-sm-2">
                                    <button class="btn btn-info" ng-click="startDate=filter.from; endDate=filter.to; getPeriodicStats();">{{'GO'|translate}}</button>
                                </div>
                            </form>
                        </nav>
                    </div>
                
                
                
                    <h2 class="col-12">{{'periodic_stats'|translate}}</h2>
<!--
                    <div class="col-md-6 card_style">
                        <div>
                            <h2>{{'VisibilityIndexs'|translate}} 
                                <i class="fa fa-info-circle" tooltip title="{{'VisibilityIndexs_desc'|translate}}"></i>
                            </h2>
                            <canvas id="line" class="chart chart-line" chart-data="VisibilityIndexs.data"
                            chart-labels="VisibilityIndexs.labels" chart-series="VisibilityIndexs.series" chart-options="VisibilityIndexs.options" chart-colors="VisibilityIndexs.colors" chart-dataset-override="override"
                                    chart-click="onClick">
                            </canvas>
                        </div>
                    </div>
-->
                    
                    <div class="col-md-6 card_style">
                        <div>
                            <h2>{{'AverageRating'|translate}} 
                                <i class="fa fa-info-circle" tooltip title="{{'AverageRating_desc'|translate}}"></i>
                            </h2>
                            
                            <div class="chartParts">
                                <span class="part2" style="display: inline-block;">
                                    <label class="myradiobtn">
                                        <input type="radio" name="directory_type" 
                                               ng-model="directory_type" value="all"
                                               ng-change="getRatings()"> 
                                        <span></span> {{'average_rating_alldir'|translate}}
                                    </label>
                                    <label class="myradiobtn">
                                        <input type="radio" name="directory_type"
                                               ng-model="directory_type" value="facebook"
                                               ng-change="getRatingsDirectory()">
                                        <span></span> {{'average_rating_facebook'|translate}}
                                    </label>
                                    <label class="myradiobtn">
                                        <input type="radio" name="directory_type"
                                               ng-model="directory_type" value="google"
                                               ng-change="getRatingsDirectory()">
                                        <span></span> {{'average_rating_google'|translate}}
                                    </label>
                                </span>
                                <span class="part1" style="float: right;">
                                    <strong>{{'total'|translate}} {{AverageRating.total||0}}</strong>
                                </span>
                            </div>
                            <div class="chartParts">
                                <div class="part1">
                                    <b>{{AverageRating.averageRating.toFixed(1)||0.0}}</b>
                                    <div>{{'outof'|translate}} 5</div>
                                    <div><img src="./Library/img/stars{{ AverageRating.starsVal }}.svg" alt=""></div>
                                </div>
                                <div class="part2">
                                    <canvas id="base" class="chart chart-horizontal-bar"
                                            chart-data="AverageRatingChart.data" chart-labels="AverageRatingChart.labels"
                                            chart-series="AverageRatingChart.series" chart-colors="AverageRatingChart.colors"
                                            chart-options="AverageRatingChart.options" 
                                            chart-dataset-override="AverageRatingChart.DataSetOverride">
                                    </canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6 card_style">
                        <div>
                            <h2>{{'G_Impressions'|translate}} 
                                <i class="fa fa-info-circle" tooltip title="{{'G_Impressions_desc'|translate}}"></i>
                            </h2>
<!--                            <div google-chart chart="CustomerFeedback"></div>-->
                            
                            <canvas id="line" class="chart chart-line" chart-data="insights.G_Impressions.data"
                                chart-labels="insights.G_Impressions.labels" chart-series="insights.G_Impressions.series" chart-options="insights.G_Impressions.options" chart-colors="insights.G_Impressions.colors" chart-dataset-override="override">
                            </canvas>
                        </div>
                    </div>
                    
                    <div class="col-md-6 card_style">
                        <div>
                            <h2>{{'G_Clicks'|translate}} 
                                <i class="fa fa-info-circle" tooltip title="{{'G_Clicks_desc'|translate}}"></i>
                            </h2>
<!--                            <div google-chart chart="CustomerFeedback"></div>-->
                            
                            <canvas id="line" class="chart chart-line" chart-data="insights.G_Clicks.data"
                                chart-labels="insights.G_Clicks.labels" chart-series="insights.G_Clicks.series" chart-options="insights.G_Clicks.options" chart-colors="insights.G_Clicks.colors" chart-dataset-override="override">
                            </canvas>
                        </div>
                    </div>
                    
                    <div class="col-md-6 card_style">
                        <div>
                            <h2>{{'G_Photos'|translate}} 
                                <i class="fa fa-info-circle" tooltip title="{{'G_Photos_desc'|translate}}"></i>
                            </h2>
<!--                            <div google-chart chart="CustomerFeedback"></div>-->
                            
                            <canvas id="line" class="chart chart-line" chart-data="insights.G_Photos.data"
                                chart-labels="insights.G_Photos.labels" chart-series="insights.G_Photos.series" chart-options="insights.G_Photos.options" chart-colors="insights.G_Photos.colors" chart-dataset-override="override">
                            </canvas>
                        </div>
                    </div>
                    
                    <div class="col-md-6 card_style">
                        <div>
                            <h2>{{'G_Queries'|translate}} 
                                <i class="fa fa-info-circle" tooltip title="{{'G_Queries_desc'|translate}}"></i>
                            </h2>
<!--                            <div google-chart chart="CustomerFeedback"></div>-->
                            
                            <canvas id="doughnut" class="chart chart-doughnut" 
                                chart-data="insights.G_Queries.data"
                                chart-labels="insights.G_Queries.labels" chart-series="insights.G_Queries.series" chart-options="insights.G_Queries.options" chart-colors="insights.G_Queries.colors" chart-dataset-override="override">
                            </canvas>
                        </div>
                    </div>
                    
                    
                    
                </div>
            </div>
            
            <div class="  col-md-3">
<!--
                <div ng-repeat="itm in Feeds" class="row">
                    <div class="col-2">
                        <img src="./Library/img/icn-tracking-{{itm.properties.type.split('_').join('-').toLowerCase()}}.svg" alt="">
                    </div>
                    <div class="col-10">
                        <small>{{set_till_now( itm.date )}} {{'days_ago'|translate}}</small>
                        <a href="{{currLang}}/profiles/basicdata">{{user.userInfo.salesPartner.name}}</a> {{'has'|translate}} <span>{{itm.properties.count}}</span> {{'new'|translate}} <span>{{itm.properties.type}}</span> {{'on'|translate}} <span>{{itm.properties.directories.join()}}</span>
                        <span>{{itm.type == "LISTING_EVENT" ? itm.properties.event.type : itm.properties.type}}</span>
                        <hr>
                    </div>
                </div>
-->
            </div>
        </div>
    </div>
    
    <div class=" sidebar col-md-3" full-height>
        <div ng-repeat="itm in Feeds" class="row">
            <div class="col-2">
                <img src="./Library/img/icn-tracking-{{( itm.properties.type||itm.type ).split('_').join('-').toLowerCase()}}.svg" alt="">
            </div>
            <div class="col-10">
                <div ng-if="itm.type !== 'LISTING_EVENT'">
                    <small>{{set_till_now( itm.date )}} {{'days_ago'|translate}} ({{itm.type}})</small>
                    <a href="{{currLang}}/profiles/basicdata">{{user.userInfo.salesPartner.name}}</a> {{'has'|translate}} <span>{{itm.properties.count}}</span> {{'new'|translate}} <span>{{itm.properties.type}}</span> {{'on'|translate}} <span>{{itm.properties.directories.join()}}</span>
                </div>
                <div ng-if="itm.type == 'LISTING_EVENT'">
                    <small>{{set_till_now( itm.date )}} {{'days_ago'|translate}}</small>
                    <a href="{{currLang}}/profiles/basicdata">{{user.userInfo.salesPartner.name}}</a>'s {{'listing_created_on'|translate}} <span>{{itm.properties.event.payload.data.directoryType}}</span>
                </div>
                <hr>
            </div>
        </div>
    </div>
</section>