




<section class="container profilesPages suggestions" ng-init="getSuggestions()">
    <h2 class="row">{{'suggestions'|translate}}</h2>
    <section class="row">
        <div class="col-12">
            <div class=" row gridHeader hideMob">
                <div class="col-sm-2"> {{'Field'|translate}} </div>
                <div class="col-sm-5"> {{'Data'|translate}} </div>
                <div class="col-sm-5"> {{'Suggestions'|translate}} </div>
            </div>

            <div class=" row gridContent row-striped" ng-repeat="itm in suggestions">
                <div class="col-sm-2"> {{itm.fieldName|translate}} </div>
                <div class="col-sm-5">
                    <span ng-if="itm.fieldName == 'photos'">
                        <span ng-repeat="imgItm in itm.currentValue">
                            <img src="{{imgItm.publicUrl}}" alt="" class="img-thumbnail">
                        </span>
                    </span>
                    <span ng-if="itm.fieldName == 'openingHours'">
                        <span ng-repeat="hoursItm in itm.currentValue" class="badge badge-info" style="margin: 0 2px">
                            {{menus.days[hoursItm.dayOfWeek]}}, {{hoursItm.from1}}-{{hoursItm.to1}} 
                        </span>
                    </span>
                    <span ng-if="itm.fieldName != 'photos' && itm.fieldName != 'openingHours'">
                        <span>{{itm.currentValue}}</span>
                        <span ng-repeat="sugItm in itm.currentValue">
                            <img src="./Library/img/directories/{{sugItm.directory}}.png" alt="">
                        </span>
                    </span>
                </div>
                
                <div class="col-sm-5">
                    <div ng-if="itm.fieldName == 'photos'">
                        <span ng-repeat="imgItm in itm.suggestions">
                            <img src="{{imgItm.value[0].url}}" alt="" class="img-thumbnail">
                        </span>
                    </div>
                    <div ng-if="itm.fieldName == 'openingHours'">         
                        <span ng-repeat="hoursItm in itm.suggestions[0].value" class="badge badge-success" style="margin: 0 2px">
                            {{menus.days[hoursItm.dayOfWeek]}}, {{hoursItm.from1}}-{{hoursItm.to1}}
                        </span>
                    </div>
                    <div ng-if="itm.fieldName != 'photos' && itm.fieldName != 'openingHours'">
                        <span>{{itm.suggestions[0].value}}</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>