
<section class="container profilesPages">
    <div class="row">
        <div class="col-sm-9" >
            <h2 class="row">{{'photos'|translate}}</h2>
            <section class="row">
                <div class="col-sm-6" 
                     ng-repeat="photoType in ['MAIN']" 
                     ng-include="'./Views/Layout/uploader.php'"></div>
                <div class="col-sm-6" 
                     ng-repeat="photoType in ['LOGO']" 
                     ng-include="'./Views/Layout/uploader.php'"></div>
                <div class="col-sm-6" 
                     ng-repeat="photoType in ['SQUARED_LOGO']" 
                     ng-include="'./Views/Layout/uploader.php'"></div>
                <div class="col-sm-6" 
                     ng-repeat="photoType in ['LANDSCAPE']" 
                     ng-include="'./Views/Layout/uploader.php'"></div>
                <div class="col-sm-12" 
                     ng-repeat="photoType in ['PHOTO']" 
                     ng-include="'./Views/Layout/uploader.php'"></div>
            </section>
            <div id="PHOTOS"></div>
            <h2 class="row">{{'videos'|translate}}</h2>
            <section class="row" >
                <div class="col-sm-1">
                    <i class="fa fa-video-camera"></i>
                </div>
                <div class="col-sm-11">
                    <div ng-repeat="itm in profile.videos" class="row">
                        <div class="col-sm-6">
                            <label>{{'video_url'|translate}}</label>
                            <input class="form-control" type="text" ng-model="profile.videos[$index].url">
                        </div>
                        <div class="col-sm-6">
                            <label>{{'description'|translate}}</label>
                            <textarea class="form-control" type="text" ng-model="profile.videos[$index].description"></textarea>
                        </div>
                    </div>
                    
                    <a href ng-click="profile.videos.push({'url':'', 'description':''})">
                        <i class="fa fa-plus"></i> {{'add'|translate}} {{'videos'|translate}}
                    </a> 
                    <a href ng-if="profile.videos.length>1" ng-click="profile.videos.pop()">
                        <i class="fa fa-minus"></i> {{'delete'|translate}} {{'videos'|translate}}
                    </a>
                </div>
            </section>
            


            <div class="row">
                <button class="btn btn-primary" ng-click="saveProfile()">{{'save'|translate}}</button>
            </div>
        </div>

        <div class="col-sm-3" style="padding-top : 20px;">
            <div ng-include="'Views/Layout/Completeness.php'" class="side_card_style"></div>
            <div ng-include="'Views/Layout/Cleaning.php'" class="side_card_style"></div>
            <div ng-include="'Views/Layout/Map.php'" class="side_card_style"></div>
<!--            <div ng-include="'Views/Layout/LocationOptions.php'" class="side_card_style2"></div>-->
        </div>
    </div>
</section>