<section class="container profilesPages" 
         ng-init="
                  chkIsLandingPage();
                  cur='p2';
                ">

    <div class="row">
        <button class="hideIt" ng-click="getLPSection(1)" id="updt_sec_1"></button>
        <button class="hideIt" ng-click="getLPSection(2)" id="updt_sec_2"></button>
        <button class="hideIt" ng-click="getProducts(landingpage.configs.id)" id="updt_sec_3"></button>
        <button class="hideIt" ng-click="getNews(landingpage.configs.id)" id="updt_sec_4"></button>
        <button class="hideIt" ng-click="getLPSection(5)" id="updt_sec_5"></button>
        <button class="hideIt" ng-click="getLPSection(6)" id="updt_sec_6"></button>
        <button class="hideIt" ng-click="getLPSection(7)" id="updt_sec_7"></button>
        <button class="hideIt" ng-click="getLPSection(8)" id="updt_sec_8"></button>

        <!--    
    Side bar
-->
        <div class="col-lg-4 col-12 position-fixed sidemenu">
            <ul class="nav nav-tabs">
                <li><a data-toggle="tab" data-target="#header_section" href 
                       ng-click="scrollTo(); getLPSection(1);"
                        class="nav-link">
                    <i class="fa fa-credit-card"></i> {{'header_section'|translate}}
                    </a>
                </li>
                <li><a data-toggle="tab" data-target="#about_us_section"  href 
                       ng-click="scrollTo(); getLPSection(2);" 
                        class="nav-link" >
                    <i class="fa fa-university"></i> {{'about_us_section'|translate}}
                    </a>
                </li>
                <li><a data-toggle="tab" data-target="#our_products_section"  href 
                       ng-click="scrollTo(); getProducts(); getProduct(-1);" 
                        class="nav-link">
                    <i class="fa fa-archive"></i> {{'our_products_section'|translate}}
                    </a>
                </li>
                <li><a data-toggle="tab" data-target="#latest_news_section"  href 
                       ng-click="scrollTo(); getNews(); " 
                        class="nav-link">
                    <i class="fa fa-rss"></i> {{'latest_news_section'|translate}}</a>
                </li>
                <li><a data-toggle="tab" data-target="#photos"  href 
                       ng-click="scrollTo(); getLPSection(5);"
                        class="nav-link">
                    <i class="fa fa-camera"></i> {{'photos_section'|translate}}</a>
                </li>
                <li><a data-toggle="tab" data-target="#contact_us_section"  href 
                       ng-click="scrollTo(); getLPSection(6);"
                        class="nav-link">
                    <i class="fa fa-envelope"></i> {{'contact_us_section'|translate}}</a>
                </li>
                <li><a data-toggle="tab" data-target="#info_pages_section"  href 
                       ng-click="scrollTo();  getLPSection(7);"
                        class="nav-link">
                    <i class="fa fa-file-text"></i> {{'info_pages_section'|translate}}</a>
                </li>
                <li class="mb-3"><a data-toggle="tab" data-target="#landingpage_configs"  href 
                        ng-click="scrollTo(); getLPSection(8);"
                        class="nav-link">
                    <i class="fa fa-cogs"></i> {{'landingpage_configs_section'|translate}}</a>
                </li>
                
                <li>
                    <a class="btn btn-info whiteText" 
                            href="{{landingpage_path}}/{{landingpage.configs.user_id}}" target="test">
                        <i class="fa fa-eye whiteText"></i> {{'page_preview'|translate}}
                    </a>
                </li>
            </ul>
        </div>
        
        <div class="col-lg-8 col-12 offset-sm-4 tab-content">
            
            <div class="hideIt" id="page_cvr">
                <i class="fa fa-cog fa-spin fa-fw fa-2x"></i>
            </div>
            <!--    
    Section Header
-->
            <section class="row tab-pane fade active" id="header_section" ng-init="
                  getLPSection(1);">
                <h2>{{'header_section'|translate}} </h2>
                <div class="row">
                    <div class="col-sm-1">
                        <i class="fa fa-credit-card"></i>
                    </div>
                    <div class="col-sm-11">
                        <form>
                        <div class="row">
                            <h4 class="col-12">{{'header_info'|translate}}</h4>
                            <div class="col-sm-12">
                                <label>{{'title'|translate}}
                                <i class="fa fa-info-circle" tooltip="" title="" data-original-title="{{'lp_title_info'}}"></i>
                                </label>
                                <input class="form-control" type="text" ng-model="landingpage.header.content_object.title" maxlength="200">
                            </div>
                            <div class="col-sm-12">
                                <label>{{'subtitle'|translate}}
                                <i class="fa fa-info-circle" tooltip="" title="" data-original-title="{{'lp_subtitle_info'}}"></i></label>
                                <input class="form-control" type="text" ng-model="landingpage.header.content_object.subtitle" maxlength="24">
                            </div>

                            <div class="col-md-6">
                                <div ng-repeat="photoType in ['LP_LOGO']" ng-include="'./Views/Layout/uploader.php'"></div>
                                <i class="fa fa-info-circle" tooltip="" title="" data-original-title="{{'lp_logo_info'}}"></i>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div ng-repeat="photoType in ['LP_BACKGROUND']" ng-include="'./Views/Layout/uploader.php'"></div>
                                <i class="fa fa-info-circle" tooltip="" title="" data-original-title="{{'lp_background_info'}}"></i>
                            </div>
                            <div class="col-sm-12">
                                <button class="btn btn-info" 
                                        ng-click="saveLPSection(1)"
                                        id="sec_1_btn">
                                    <span><i class="fa fa-save"></i></span> {{'save'|translate}}
                                </button>                      
                            </div>
                        </div>
                        </form>
                    </div>
                </div>

            </section>

            <!--    
    Section About us
-->

            <section class="row tab-pane fade" id="about_us_section" ng-init="
                  getLPSection(2);">
                <h2>{{'about_us_section'|translate}} </h2>
                <div class="row">
                    
                    <div class="col-sm-1">
                        <i class="fa fa-university"></i>
                    </div>

                    <div class="col-sm-11">
                        <div class="row">
                            
                            <h4 class="col-12">{{'aboutus_info'|translate}}</h4>  
                            <div class=" col-12">
                                <a class="btn btn-success"  href ng-click="syncContent(2)"
                                        tooltip="" title="{{'sync_info'|translate}}" id="sec_2_sync_btn">
                                    <span><i class="fa fa-exchange"></i></span>
                                </a>
                            </div>
                            
                            <div class="col-sm-12">
                                <label>{{'aboutus_desc'|translate}}</label>
                                <div>{{landingpage.aboutus.content_object.desc}}
                                        <a href="{{currLang}}/{{linksEditData['description']}}">
                                            <i class="fa fa-pencil"></i> {{'edit'|translate}}
                                        </a>
                                </div>
                            </div>

                            <div class="col-sm-6 mb-3">
                                <div ng-repeat="photoType in ['LP_ABOUTUS1']" ng-include="'./Views/Layout/uploader.php'"></div>
                            </div>

                            <div class="col-sm-6 mb-3">
                                <div ng-repeat="photoType in ['LP_ABOUTUS2']" ng-include="'./Views/Layout/uploader.php'"></div>
                            </div>

                            <div class="col-sm-6 mb-3">
                                <div ng-repeat="photoType in ['LP_ABOUTUS3']" ng-include="'./Views/Layout/uploader.php'"></div>
                            </div>
                            
                            <div class="col-sm-12">
                                <button class="btn btn-info" ng-click="saveLPSection(2);" id="sec_2_btn"><span><i class="fa fa-save"></i></span> {{'save'|translate}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!--    
    Section Our products / Services
-->


            <section class="row tab-pane fade" id="our_products_section">
                <h2>{{'our_products_section'|translate}}</h2>
                <div class="row">
                    <div class="col-sm-1">
                        <i class="fa fa-archive"></i>
                    </div>
                    <div class="col-sm-11">
                            <form>
                        <div class="row collapse" id="add_product">
                            <div class="col-sm-12">
                                <label>{{'product_name'|translate}}</label>
                                <input class="form-control" type="text" ng-model="landingpage.product.product_title" maxlength="90">
                            </div>
                            <div class="col-sm-12">
                                <label>{{'product_desc'|translate}}</label>
                                <textarea class="form-control" type="text" ng-model="landingpage.product.product_desc" maxlength="200"></textarea>
                            </div>
                            <div class="col-sm-12 mb-3">
                                <div ng-repeat="photoType in ['LP_PRODUCT']" ng-include="'./Views/Layout/uploader.php'"></div>
                            </div>
                            <div class="col-sm-12 mb-3">
                                <button class="btn btn-info" 
                                        ng-click="saveProduct(landingpage.product)"
                                        id="sec_3_btn">
                                    <span><i class="fa fa-save"></i></span> {{'save'|translate}}
                                </button>
                            </div>
                        </div>
                            </form>
                        <div class="row">
                            <div class="col-sm-12">
                                <div ng-include="'./Views/Layout/LP_ProductsList.php'"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <!--    
    Section Latest news
-->


            <section class="row tab-pane fade" id="latest_news_section">
                <h2>{{'latest_news_section'|translate}} </h2>
                <div class="row">
                    <div class="col-sm-1">
                        <i class="fa fa-rss"></i>
                    </div>
                    <div class="col-sm-11">
                        <div class="row">
                            <div class="col-sm-12 mb-3">
                                <div ng-include="'./Views/Layout/LP_NewsList.php'"></div>
                            </div>
<!--
                            <div class="col-sm-12">
                                <a class="btn btn-info" 
                                        href="{{currLang}}/contents">
                                    <i class="fa fa-plus"></i> {{'add_news'|translate}}
                                </a>
                            </div>
-->
                        </div>
                    </div>
                </div>
            </section>

            <!--    
    Section Photos
-->

            <section class="row tab-pane fade" id="photos">
                <h2>{{'photos'|translate}} </h2>
                <div class="row">
                    <div class="col-sm-1">
                        <i class="fa fa-camera"></i>
                    </div>
                    <div class="col-sm-11">
                        <div class="row">
                            <div class="col-sm-12 mb-3">
                                <div ng-include="'./Views/Layout/LP_PhotosList.php'"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!--    
    Section Contact us
-->

            <section class="row tab-pane fade" id="contact_us_section">
                <h2>{{'contact_us_section'|translate}} </h2>
                <div class="row">
                    <div class="col-sm-1">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <div class="col-sm-11">
                        <div class="row">
                            
                            <h4 class="col-12">{{'contacts_info'|translate}}</h4>  
                            <div class=" col-12">
                                <a class="btn btn-success" href ng-click="syncContent(6)"
                                        tooltip="" title="{{'sync_info'|translate}}" id="sec_6_sync_btn">
                                    <span><i class="fa fa-exchange"></i> </span>
                                </a>
                            </div>
                            
                            <div class="col-sm-6">
                                <label>{{'contactus_address'|translate}}</label>
                                <div>{{landingpage.contacts.content_object.address}}
                                        <a href="{{currLang}}/profiles/basicdata">
                                            <i class="fa fa-pencil"></i> {{'edit'|translate}}
                                        </a>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label>{{'contactus_email'|translate}}</label>
                                <div>{{landingpage.contacts.content_object.email}}
                                        <a href="{{currLang}}/{{linksEditData['email']}}">
                                            <i class="fa fa-pencil"></i> {{'edit'|translate}}
                                        </a>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label>{{'contactus_phone'|translate}}</label>
                                <div>{{landingpage.contacts.content_object.phone}}
                                        <a href="{{currLang}}/{{linksEditData['phone']}}">
                                            <i class="fa fa-pencil"></i> {{'edit'|translate}}
                                        </a>
                                </div>
                            </div>
                            <div class="col-sm-12 mb-3">
                                <div class="side_card_style">
                                    <div>
                                        <div class="row">
                                            <div class="col-12 map">
                                                <img ng-src="https://maps.googleapis.com/maps/api/staticmap?center={{landingpage.contacts.content_object.lat}},{{landingpage.contacts.content_object.lng}}&zoom=16&maptype=roadmap&markers=color:red|{{landingpage.contacts.content_object.lat}},{{landingpage.contacts.content_object.lng}}&size=320x160&scale=1&language=en&key=AIzaSyCqChdwcS_OfmDJWh7LaexI-hJR2WV6aCc" alt="">
                                            </div>
                                            <div class="col-2 ">
                                                <img src="./Library/img/address.png" alt="">
                                            </div>
                                            <div class="col">
                                                <p>{{landingpage.contacts.content_object.address}}</p>
                                            </div>
                                        </div>
                                    </div>
                                
                                </div>
                            </div>
<!--
                            <div class="col-sm-12">
                                <button class="btn btn-info"><i class="fa fa-save"></i> {{'save'|translate}}</button>
                            </div>
-->
                        </div>
                    </div>
                </div>
            </section>


            <!--    
    Section Information pages
-->

            <section class="row tab-pane fade" id="info_pages_section">
                <h2>{{'info_pages_section'|translate}} </h2>
                <div class="row">
                    <div class="col-sm-1">
                        <i class="fa fa-file-text"></i>
                    </div>
                    <div class="col-sm-11">
                        <div class="row">
                            <h4 class="col-12">{{'pages_info'|translate}}</h4>
                            <div class="col-sm-12">
                                <label>{{'page_title'|translate}}</label>
<!--
                                <select class="form-control" ng-model="landingpage.pages.content_object.title">
                                    <option value="privacy_policy">{{'privacy_policy'|translate}}</option>
                                    <option value="terms_conditions">{{'terms_conditions'|translate}}</option>
                                </select>
-->
                                <div class="col-12">
                                    <h1>
                                    <label class="myradiobtn row" style="margin: 2px 0 0 0">
                                        <input type="radio" ng-model="landingpage.pages.content_object['p1'].title"
                                               value="Impressum" name="val" ng-click="cur='p1'">
                                        <span></span> {{'Impressum'|translate}}
                                    </label>
                                    <label class="myradiobtn row" style="margin: 2px 0 0 0">
                                        <input type="radio" ng-model="landingpage.pages.content_object['p2'].title" 
                                               value="Data privacy" name="val" ng-click="cur='p2'">
                                        <span></span> {{'Data privacy'|translate}}
                                    </label>
                                    </h1>
                                </div>
                            </div>
                            <div class="col-sm-12 mb-3">
                                <label>{{'page_desc'|translate}}</label>
                                <textarea class="form-control" type="text" ng-model="landingpage.pages.content_object[cur].desc">
                                </textarea>
                            </div>
                            
                            <div class="col-sm-12">
                                <button class="btn btn-info" ng-click="saveLPSection(7);" id="sec_7_btn"><span><i class="fa fa-save"></i></span> {{'save'|translate}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <!--    
    Section Configuration
-->
     
            <section class="row tab-pane fade" id="landingpage_configs">
                <h2>{{'landingpage_configs'|translate}} </h2>
                <div class="row">
                    <div class="col-sm-1">
                        <i class="fa fa-cogs"></i>
                    </div>
                    <div class="col-sm-11"> 
                        <div class="row">
                            <h4 class="col-12">{{'configs_info'|translate}}</h4>  
                            <div class=" col-12 mb-3">
                                <a class="btn btn-success"  href ng-click="syncLandingpage()"
                                        tooltip="" title="{{'sync_info'|translate}}" id="lp_btn">
                                    <span><i class="fa fa-exchange"></i></span>
                                </a>
                            </div>
                            <div class="col-6 ">{{'lp_show_hide'|translate}} 
                                <i class="fa fa-info-circle" tooltip title="{{'lp_show_hide_desc'|translate}}"></i> 
                            </div>
                            
                            <div class="col-6 no_label_padding">
                                <label class="switch">
                                    <input type="checkbox" id="lp_show_hide"
                                        ng-model="landingpage.settings.content_object.isPublished"
                                           ng-false-value="false"
                                           ng-true-value="true"
                                        ng-change="saveLPSection(8)">
                                    <span class="slider round"></span>
                                </label>
                                <label for="lp_show_hide">{{'isPublished'|translate}}</label>
                            </div>

                            <hr class="col-12 hr_center">

<!--
                            <div class="col-12 mb-3">
                                <label>{{'user_domain'|translate}}</label>
                                <input type="text" ng-model="landingpage.configs.user_domain" class="form-control" maxlength="191">
                            </div>
-->
                            <div class="col-sm-6 mb-3">
                                <label>{{'products_sec_title'|translate}}</label>
                                <select class="form-control" type="text" 
                                        ng-model="landingpage.settings.content_object.products_sec_title"
                                        ng-change="saveLPSection(8)">
                                    <option value="Our Products">Our Products</option>
                                    <option value="Our Services">Our Services</option>
                                    <option value="Our Products &amp; Sevices">Our Products &amp; Sevices</option>
                                </select>
                            </div>
                            
                            <div class="col-sm-12" >
                            <label class="mb-2">
                                {{'metatags_title'|translate}}
                                <i class="fa fa-info-circle" tooltip title="{{'metatags_desc'|translate}}"></i> 
                            </label>
                                <div ng-repeat="meta in landingpage.configs.seo_data">
                                    <b>{{meta.attr|translate}}</b>
                                    <p>{{meta.value}}
                                        <a href="{{currLang}}/{{linksEditData[meta.attr]}}">
                                            <i class="fa fa-pencil"></i> {{'edit'|translate}}
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>

<script>
        $("#header_section").addClass("show");
        $('[data-target="#header_section"]').addClass("active");
</script>