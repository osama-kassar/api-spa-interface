
<section class="container profilesPages " ng-init="
            t_datepicker=false;">
    <div class="row" id="socialProfiles">
        <div class="col-sm-9">
            <h2 class="row">{{'general_info'|translate}} </h2>
            <section class="row">
                <div class="col-sm-1">
                    <i class="fa fa-home"></i>
                </div>
                <div class="col-sm-11" id="name">
                    <label>{{'name'|translate}}</label>
                    <input class="form-control" type="text" ng-model="profile.name">
                </div>

                <div class="col-sm-1">
                </div>
                <div class="col-sm-11">
                    <label>{{'identifier'|translate}}</label>
                    <input class="form-control" type="text" ng-model="profile.identifier">
                </div>

                <div class="col-sm-1">
                    <i class="fa fa-compass"></i>
                </div>
                <div class="col-sm-11">
                    <label>{{'country'|translate}}</label>
                    <input class="form-control" type="text" ng-model="profile.country">
                </div>

                <div class="col-sm-1">
                </div>
                <div class="col-sm-8">
                    <label>{{'street'|translate}}</label>
                    <input class="form-control" type="text" ng-model="profile.street">
                </div>

                <div class="col-sm-3">
                    <label>{{'streetNo'|translate}}</label>
                    <input class="form-control" type="text" ng-model="profile.streetNo">
                </div>

                <div class="col-sm-1">
                </div>
                <div class="col-sm-3">
                    <label>{{'zip'|translate}}</label>
                    <input class="form-control" type="text" ng-model="profile.zip">
                </div>

                <div class="col-sm-8">
                    <label>{{'city'|translate}}</label>
                    <input class="form-control" type="text" ng-model="profile.city">
                </div>

                <div class="col-sm-1">
                </div>
                <div class="col-sm-11">
                    <div class="form-group ">
                        <label class="mycheckbox">
                        <input type="checkbox" ng-model="profile.addressDisplay">
                            <span></span> {{'address_hidden'|translate}}
                        </label>
                    </div>
                </div>

                <div class="col-sm-1">
                    <i class="fa fa-podcast"></i>
                </div>
                <div class="col-sm-11">
                    <label>{{'service_area'|translate}}</label>
                    <tags-input  
                            placeholder="{{'add_area'|translate}}"
                            ng-model="profile.service_area"
                            display-property="name"
                            key-property="placeId"
                            replace-spaces-with-dashes="false"
                            add-from-autocomplete-only="true">
                        <auto-complete 
                                       source="loadTags($query, 'places')"
                                       debounce-delay="1000"></auto-complete>
                    </tags-input>
                </div>
                <div class="col-sm-1">
                    <i class="fa fa-list"></i>
                </div>
                <div class="col-sm-11">
                    <label>{{'categories'|translate}}</label>
                    <tags-input  
                            placeholder="{{'add_category'|translate}}"
                            ng-model="categories_alt"
                            display-property="fullName"
                            key-property="id"
                            replace-spaces-with-dashes="false"
                            add-from-autocomplete-only="true">
                        <auto-complete 
                                       source="loadTags($query, 'categories')" 
                                       debounce-delay="1000"></auto-complete>
                    </tags-input>
                </div>
            </section>





            <h2 class="row">{{'contact_info'|translate}} </h2>
            <section class="row">

                <div class="col-sm-1" id="phone">
                    <i class="fa fa-phone"></i>
                </div>
                <div class="col-sm-11">
                    <label>{{'phone'|translate}}</label>
                    <input class="form-control" type="text" ng-model="profile.phone">
                </div>

                <div class="col-sm-1">
                </div>
                <div class="col-sm-11">
                    <label>{{'mobile'|translate}}</label>
                    <input class="form-control" type="text" ng-model="profile.cellphone">
                </div>

                <div class="col-sm-1">
                </div>
                <div class="col-sm-11">
                    <label>{{'fax'|translate}}</label>
                    <input class="form-control" type="text" ng-model="profile.fax">
                </div>

                <div class="col-sm-1">
                    <i class="fa fa-link"></i>
                </div>
                <div class="col-sm-11">
                    <label>{{'website'|translate}}</label>
                    <input class="form-control" type="text" ng-model="profile.website">
                </div>
                <div class="col-sm-1" id="email">
                    <i class="fa fa-at"></i>
                </div>
                <div class="col-sm-11">
                    <label>{{'email'|translate}}</label>
                    <input class="form-control" type="text" ng-model="profile.email">
                </div>
                
                
                
                
                
                <div class="col-sm-1" id="openingHours">
                    <i class="fa fa-clock-o"></i>
                </div>
                <div class="col-sm-11 openingHoursDiv">
                    <div class="row" ng-repeat="itm in profile.openingHours track by $index">
                        <div class="col-sm-2">
                            <div><label ng-if="$index==0">{{'openingDays'|translate}}</label></div>
                            <label class="mycheckbox">
                                <input type="checkbox" ng-model="profile.openingHours[$index].val"
                                       class="form-control">
                                <span></span> {{menus.days[itm.dayOfWeek]}}
                            </label>
                        </div>
                        <div class="col-sm-2" ng-if="profile.openingHours[$index].val">
                            <div><label ng-if="$index==0">{{'from'|translate}}</label></div>
                            <select ng-options="itm.id as itm.name for itm in menus['hours']"
                                    ng-model="profile.openingHours[$index].from1"
                                    class="form-control">
                                <option value=""></option>
                            </select>
                        </div>
                        <div class="col-sm-2" ng-if="profile.openingHours[$index].val">
                            <div><label ng-if="$index==0">{{'to'|translate}}</label></div>
                            <select ng-options="itm.id as itm.name for itm in menus['hours']"
                                    ng-model="profile.openingHours[$index].to1"
                                    class="form-control">
                                <option value=""></option>
                            </select>
                        </div>
                        <div class="col-sm-2" ng-if="profile.openingHours[$index].val">
                            <div><label ng-if="$index==0">{{'from'|translate}}</label></div>
                            <select ng-options="itm.id as itm.name for itm in menus['hours']"
                                    ng-model="profile.openingHours[$index].from2"
                                    class="form-control">
                                <option value=""></option>
                            </select>
                        </div>
                        <div class="col-sm-2" ng-if="profile.openingHours[$index].val">
                            <div><label ng-if="$index==0">{{'to'|translate}}</label></div>
                            <select ng-options="itm.id as itm.name for itm in menus['hours']"
                                    ng-model="profile.openingHours[$index].to2"
                                    class="form-control">
                                <option value=""></option>
                            </select>
                        </div>
                    </div>
                </div>
                
                
                

                <div class="col-sm-1">
                </div>
                <div class="col-sm-11">
                    <label>{{'openingHoursNotes'|translate}}</label>
                    <input class="form-control" type="text" ng-model="profile.openingHoursNotes">
                </div>

                <div class="col-sm-1">
                </div>
                <div class="col-sm-11">
                    <div class="row" ng-repeat="itm in profile.specialOpeningHours track by $index">
                        <div class="col-sm-3">
                            <label ng-if="$index<1">{{'specialOpeningHours'|translate}}</label>
                            <div class="input-group mb-3 right-rounded">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" >
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>
                                    <input 
                                        class="form-control" 
                                        type="text"
                                        ng-model="profile.specialOpeningHours[$index].date"
                                        uib-datepicker-popup="yyyy-MM-dd"
                                        is-open="t_datepicker" 
                                        ng-focus="t_datepicker=true"
                                        placeholder="{{'select_date' | translate}}"
                                        show-button-bar="false"
                                        datepicker-options="{showWeeks:false}"
                                        date-handler >

                            </div>
                        </div>
                        <div class="col-sm-3 ">
                            <label ng-if="$index<1">{{'openingHoursClosed'|translate}}</label>
                            <select class="form-control mb-3" 
                                ng-model="profile.specialOpeningHours[$index].closed"
                                ng-options="opt.closed as (opt.name|translate) for opt in [{closed:true, name:'closed'}]">
                                <option value="">{{'opened'|translate}}</option>
                            </select>
                        </div>
                        <div class="col-sm-2" ng-if="profile.specialOpeningHours[$index].closed !== true">
                            <div><label ng-if="$index==0">{{'from'|translate}}</label></div>
                            <select ng-options="itm.id as itm.name for itm in menus['hours']"
                                    ng-model="profile.specialOpeningHours[$index].from1"
                                    class="form-control">
                                <option value="">{{'from'|translate}}</option>
                            </select>
                        </div>
                        <div class="col-sm-2" ng-if="profile.specialOpeningHours[$index].closed !== true">
                            <div><label ng-if="$index==0">{{'to'|translate}}</label></div>
                            <select ng-options="itm.id as itm.name for itm in menus['hours']"
                                    ng-model="profile.specialOpeningHours[$index].to1"
                                    class="form-control">
                                <option value="">{{'to'|translate}}</option>
                            </select>
                        </div>
                        <div class="col-sm-1">
                             <div><label ng-if="$index<1"> &nbsp; </label></div>
                            <a href ng-click="profile.specialOpeningHours.splice($index,1)"><span class="lh38 fa fa-times"></span></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-1">
                </div>
                <div class="col-11 ">
                    <a href ng-click="profile.specialOpeningHours.push({'date':'', 'closed':''})">
                        <i class="fa fa-plus"></i> {{'add'|translate}} {{'openingHoursClosed'|translate}}
                    </a>  &nbsp;
<!--
                    <a href ng-if="profile.specialOpeningHours.length>1" ng-click="profile.specialOpeningHours.pop()">
                        <i class="fa fa-minus"></i> {{'delete'|translate}} {{'openingHoursClosed'|translate}}
                    </a>
-->
                </div>
            </section>
            
            <h2 class="row" id="keywords">{{'business_info'|translate}} </h2>
            <section class="row">
                
                <div class="col-sm-1">
                    <i class="fa fa-key"></i>
                </div>
                <div class="col-sm-11">
                    <label>{{'keywords'|translate}}</label>
                    <tags-input  placeholder="{{'add_keyword'|translate}}"
                            ng-model="profile.keywords"
                            use-strings="true"></tags-input>
                </div>
                <div class="col-sm-1" id="descriptionShort">
                    <i class="fa fa-file-o"></i>
                </div>
                <div class="col-sm-11">
                    <label>{{'descriptionShort'|translate}}</label>
                    <textarea class="form-control" type="text" ng-model="profile.descriptionShort"></textarea>
                </div>
                
                <div class="col-sm-1">
                    <i class="fa fa-files-o"></i>
                </div>
                <div class="col-sm-11" id="descriptionLong">
                    <label>{{'descriptionLong'|translate}}</label>
                    <textarea class="form-control" type="text" ng-model="profile.descriptionLong"></textarea>
                </div>
                <div class="col-sm-1">
                    <i class="fa fa-folder-o"></i>
                </div>
                <div class="col-sm-11">
                    <label>{{'imprint'|translate}}</label>
                    <input class="form-control" type="text" ng-model="profile.imprint">
                </div>
            </section>






            <h2 class="row">{{'labels'|translate}} </h2>
            <section class="row">
                <div class="col-sm-1">
                    <i class="fa fa-hashtag"></i>
                </div>
                <div class="col-sm-11">
                    <label>{{'labels'|translate}}</label>
                    <tags-input  placeholder="{{'add_labels'|translate}}"
                            ng-model="profile.labels"
                            use-strings="true"
                            on-tag-added="addTag($query, 'labels')">
                    </tags-input>
                </div>
            </section>


            <div class="row">
                <button class="btn btn-primary" ng-click="saveProfile()">{{'save'|translate}}</button>
            </div>

        </div>

        <div class="col-sm-3" style="padding-top : 20px;">
            <div ng-include="'Views/Layout/Completeness.php'" class="side_card_style"></div>
            <div ng-include="'Views/Layout/Cleaning.php'" class="side_card_style"></div>
            <div ng-include="'Views/Layout/Map.php'" class="side_card_style"></div>
<!--            <div ng-include="'Views/Layout/LocationOptions.php'" class="side_card_style2"></div>-->
        </div>
    </div>
</section>