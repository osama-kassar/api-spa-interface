
<section class="container profilesPages " ng-init="
            t_datepicker=false;
    ">
    <div class="row">
        <div class="col-sm-9">
            <h2 class="row">{{'social_media_profiles'|translate}} </h2>
            <section class="row">
                <div class="col-sm-1">
                    <i class="fa fa-thumbs-o-up"></i>
                </div>
                <div class="col-sm-11">
                    <div class="row">
                        <div
                             ng-repeat="itm in profile.socialProfiles track by $index"
                             class="col-sm-6">
                            <label>{{itm.type.toLowerCase()|translate}}</label>                    
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon{{$index}}">
                                        <i class="fa fa-{{profile.socialProfiles[$index].type.toLowerCase()}}"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" 
                                       ng-model="profile.socialProfiles[$index].url" 
                                       placeholder="{{'add_'+itm.type|translate}}" aria-describedby="basic-addon{{$index}}">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            
            
            <h2 class="row"  id="attributes">{{'attributes'|translate}} </h2>
            <section class="row">
                <div class="col-sm-1">
                    <i class="fa fa-diamond"></i>
                </div>
                <div class="col-sm-11">
                    <div class="row" ng-repeat="itm in attributes_alt track by $index">
                        <div class="col-sm-5">
                            <label ng-if="$index==0">{{'attributes'|translate}}</label>
                            <select ng-model="attributes_alt[$index].externalId"
                                    class="form-control mb-3">
                                <option ng-repeat="opt in attributesList" 
                                        value="{{opt.externalId}}">{{opt.displayName}}</option>
                            </select>
                        </div>
                        <div class="col-sm-5">
                            <label ng-if="$index==0">{{'value'|translate}}</label>

                            <input ng-if="attributes_alt[$index].externalId.indexOf('url_') > -1" 
                                   class="form-control" type="text" 
                                   ng-model="attributes_alt[$index].value">
                            
                            <div ng-if="attributes_alt[$index].externalId.indexOf('url_') == -1">
                                <label class="myradiobtn" style="margin:0 10px;; line-height: 40px;">
                                    <input type="radio" ng-model="attributes_alt[$index].value"
                                           value="true" name="val">
                                    <span></span> {{'yes'|translate}}
                                </label>
                                <label class="myradiobtn" style="margin:0 10px; line-height: 40px;">
                                    <input type="radio" ng-model="attributes_alt[$index].value" 
                                           value="false" name="val">
                                    <span></span> {{'no'|translate}}
                                </label>
                            </div>

<!--
                            <select ng-if="attributes_alt[$index].externalId.indexOf('url_') == -1" class="form-control" 
                                    ng-model="attributes_alt[$index].value" >
                                <option value="true">{{'yes'|translate}}</option>
                                <option value="false">{{'no'|translate}}</option>
                            </select>
-->
                        </div>
                        <div class="col-sm-1">
                             <div><label ng-if="$index<1"> &nbsp; </label></div>
                            <a href ng-click="attributes_alt.splice($index,1)"><span class="lh38 fa fa-times"></span></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-1">
                </div>
                <div class="col-11 ">
                    <a href ng-if="attributes_alt.length<attributesList.length" 
                       ng-click="attributes_alt.push({}); 
                                 inc=inc+1; ">
                        <i class="fa fa-plus"></i> {{'add'|translate}} {{'attribute'|translate}}
                    </a>
                </div>
            </section>
            
            
            
            <h2 class="row">{{'payments_options'|translate}} </h2>
            <section class="row">
                <div class="col-sm-1">
                    <i class="fa fa-credit-card-alt"></i>
                </div>
                <div class="col-sm-11">
                    <label>{{'paymentOptions'|translate}}</label>
                    <tags-input  
                            placeholder="{{'add_payment'|translate}}"
                            ng-model="profile.paymentOptions"
                            display-property="name"
                            replace-spaces-with-dashes="false"
                            template="payment-tag-template"
                            min-length="1">
                        <auto-complete 
                                       source="loadMenu($query, 'paymentOptions')" 
                                       load-on-focus="true"
                                       load-on-empty="true"
                                       template="payment-options-template"
                                       min-length="1"></auto-complete>
                    </tags-input>
                    
                    
                    
                    <script type="text/ng-template" id="payment-tag-template">
                      <div class="tag-template">
                        <div class="left-panel">
                          <img ng-src="./Library/img/paymentOptions/{{data.icn.toUpperCase()}}.png"/>
                        </div>
                        <div class="right-panel">
                          <span>{{$getDisplayText()}}</span>
                          <a class="remove-button" ng-click="$removeTag()">&#10006;</a>
                        </div>
                      </div>
                    </script>
                    
                    
                    <script type="text/ng-template" id="payment-options-template">
                    <div class="row">
                      <div class="col-sm-2">
                        <img ng-src="./Library/img/paymentOptions/{{data.icn.toUpperCase()}}.png" style="height:50px;"/>
                      </div>
                      <div class="col-sm-6">
                        <span ng-bind-html="$highlight($getDisplayText())"></span>
                      </div>
                    </div>
                    </script>
                </div>
            </section>
            
            
            
            <h2 class="row">{{'brands'|translate}} </h2>
            <section class="row">
                <div class="col-sm-1">
                    <i class="fa fa-tag"></i>
                </div>
                <div class="col-sm-11">
                    <label>{{'brands'|translate}}</label>
                    <tags-input  
                            placeholder="{{'add_brand'|translate}}"
                            ng-model="profile.brands"
                            replace-spaces-with-dashes="false"
                            add-from-autocomplete-only="true" 
                            use-strings="true">
                        <auto-complete 
                                       source="loadMenu($query, 'brands')" 
                                       load-on-focus="true"
                                       load-on-empty="true"
                                       min-length="1"></auto-complete>
                    </tags-input>
                </div>
            </section>
            
            
            
            <h2 class="row">{{'languages'|translate}} </h2>
            <section class="row">
                <div class="col-sm-1">
                    <i class="fa fa-comments-o"></i>
                </div>
                <div class="col-sm-11">
                    <label>{{'languages'|translate}}</label>
                    <tags-input  
                            placeholder="{{'add_language'|translate}}"
                            ng-model="profile.languages"
                            replace-spaces-with-dashes="false"
                            add-from-autocomplete-only="true"
                             use-strings="true">
                        <auto-complete 
                                       source="loadMenu($query, 'languages')" 
                                       load-on-focus="true"
                                       load-on-empty="true"
                                       min-length="1"></auto-complete>
                    </tags-input>
                </div>
            </section>
            
            
            
            <h2 class="row">{{'services'|translate}} </h2>
            <section class="row">
                <div class="col-sm-1">
                    <i class="fa fa-lightbulb-o"></i>
                </div>
                <div class="col-sm-11">
                    <label>{{'services'|translate}}</label>
                    <tags-input  
                            placeholder="{{'add_service'|translate}}"
                            ng-model="profile.services"
                            replace-spaces-with-dashes="false"
                            add-from-autocomplete-only="true"
                            use-strings="true">
                        <auto-complete 
                                       source="loadMenu($query, 'services')" 
                                       load-on-focus="true"
                                       load-on-empty="true"
                                       min-length="1"></auto-complete>
                    </tags-input>
                </div>
            </section>


            <div class="row">
                <button class="btn btn-primary" ng-click="saveProfile()">{{'save'|translate}}</button>
            </div>
        </div>

        <div class="col-sm-3" style="padding-top : 20px;">
            <div ng-include="'Views/Layout/Completeness.php'" class="side_card_style"></div>
            <div ng-include="'Views/Layout/Cleaning.php'" class="side_card_style"></div>
            <div ng-include="'Views/Layout/Map.php'" class="side_card_style"></div>
<!--            <div ng-include="'Views/Layout/LocationOptions.php'" class="side_card_style2"></div>-->
        </div>
    </div>
</section>