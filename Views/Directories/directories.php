<section class="container" 
         ng-init="getDirectories();
                  getListings2();" >
    <div class="row">
        <div class="col-sm-9">
            <section class="row" 
                     ng-if="pending.length>0">
                <h5 class="seperator">{{'pending_actions'|translate}}</h5>
                <figure class="col-sm-4 dir_card"
                        ng-repeat="itm in pending">
                    <div>
                        <span>
                            <img src="./Library/img/directories/{{itm.type}}.png" alt="" class="img">
                        </span>
                        <span>
                            {{itm.typeName}}<br>
                            <small ng-if="itm.accountInfo.name">
                                {{'details'|translate}} <i class="fa fa-info-circle" tooltip title="{{'connected_with'|translate}}: {{itm.accountInfo.name}}"></i>
                                <br>
                                <i class="fa fa-chain-broken"></i> <a href ng-click="" target="_blank">{{'disconnect_listing'|translate}}</a>
                            </small>
                        </span>
                        <span>
                            <img src="./Library/img/directories/edit-needed.svg" alt="" tooltip title="{{'dir_edit_required_icon_msg'|translate}}" class="icn">
                        </span>
                        <p>
                            <a class="btn btn-info btn-sm"
                                    href="{{itm.resolveUrl}}" target="_blank">{{itm.actionNeeded|translate}} {{itm.typeName}}</a>
                        </p>
                    </div>
                </figure>
            </section>
            
            
            
            
            <section class="row " ng-repeat="section in directories track by $index">
                    <h5 class="seperator">{{cat_titles[$index] | translate}}</h5>
                    <figure ng-repeat="itm in section" class="col-sm-4 dir_card">
                        <div>
                            <span>
                                <img src="./Library/img/directories/{{itm.type}}.png" alt="" class="img">
                            </span>
                            <span>
                                <a href="{{itm.listingUrl}}" target="_blank" ng-disabled="itm.listingUrl==null" >
                                    {{itm.typeName}}
                                </a>
                            </span>
                            <span>
                                <img src="./Library/img/directories/{{itm.icon}}" alt="" tooltip title="{{itm.stateMessage|translate}}" class="icn">
                            </span>
                        </div>
                    </figure>
            </section>
        </div>
        
        <div class="col-sm-3" style="padding-top : 20px;">
            <div ng-include="'Views/Layout/Completeness.php'" class="side_card_style"></div>
            <div ng-include="'Views/Layout/Listings.php'" class="side_card_style"></div>
        </div>
    </div>
</section>