<section class="container contents_style " 
         ng-init="
                  doReset('post');
                  getPosts();
                  getTemplates();
                  getPostableDirectories();
                  startDate_is_opened=false; 
                  endDate_is_opened=false;
                  photoType = 'Template';">
    <div class="row">
        <div class="col-sm-12">
            <h2 class="">{{'publications'|translate}} </h2>

            <div id="accordion">


                <!-- POST SECTION -->
                <div class="card">
                    <div class="card-header" id="heading-create_post" data-toggle="collapse" data-target="#create_post" aria-expanded="false" aria-controls="create_post" >
                        <h5 class="mb-0">
                            <button class="btn btn-link ">
                                <i class="fa fa-pencil"></i> {{'create_post'|translate}}
                            </button>
                        </h5>
                    </div>
                    <div id="create_post" class="collapse collapsed" aria-labelledby="create_post" data-parent="#accordion">
                        <div class="card-body">
                            <div class="row">
                                <!-- CREATE POST    -->
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            {{'type_of_post'|translate}}:
                                        </div>
                                        <div class="col-sm-8">
                                            <label class="myradiobtn 
                                                          squareOptions{{post.type=='POST' ? 'Active' : ''}}">
                                                <div><i class="fa fa-rss"></i>
                                                </div>
                                                <input type="radio" ng-model="post.type" value="POST"> {{'news'|translate}}
                                            </label>
                                            <label class="myradiobtn 
                                                          squareOptions{{post.type=='OFFER' ? 'Active' : ''}}">
                                                <div><i class="fa fa-asterisk"></i>
                                                </div>
                                                <input type="radio" ng-model="post.type" value="OFFER"> {{'offer'|translate}}
                                            </label>
                                            <label class="myradiobtn 
                                                          squareOptions{{post.type=='QUESTION_AND_ANSWER' ? 'Active' : ''}}">
                                                <div><i class="fa fa-comments"></i>
                                                </div>
                                                <input type="radio" ng-model="post.type" value="QUESTION_AND_ANSWER"> {{'question'|translate}}
                                            </label>
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <div class="col-sm-4">
                                            {{'post_on'|translate}}:
                                        </div>
                                        <div class="col-sm-8">

                                            <div ng-if="post.type=='POST'">
                                                <label class="mycheckbox">
                                                    <input type="checkbox" ng-model="post.postOn.facebook"> 
                                                    <span></span>
                                                    <i class="fa fa-facebook-square blueText"></i> {{'facebook'|translate}} 
                                                </label>
                                                <i class="fa fa-info-circle" tooltip title="{{'facebook_status'|translate}}"></i>
                                            </div>

                                            <div>
                                                <label class="mycheckbox">
                                                    <input type="checkbox" ng-model="post.postOn.google"> 
                                                    <span></span>
                                                    <i class="fa fa-google-plus-square redText"></i> {{'google_search'|translate}} 
                                                </label>
                                                <i class="fa fa-info-circle" tooltip title="{{'google_status'|translate}}"></i>
                                            </div>

                                            <div ng-if="post.type!=='QUESTION_AND_ANSWER'">
                                                <label class="mycheckbox">
                                                    <input type="checkbox" ng-model="post.postOn.other"> 
                                                    <span></span> {{'other_directories'|translate}} ({{postableDirectories.length}})
                                                </label>
                                                <div>
                                                    <span ng-repeat="(k, v) in postableDirectories" tooltip title="{{v}}">
                                                        <img src="./Library/img/directories/{{v}}.png" 
                                                             style="height: 15px;">
                                                    </span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-4">
                                            {{post.type=='QUESTION_AND_ANSWER' ? 'question' : 'title'|translate}}:
                                        </div>
                                        <div class="col-sm-8 form-group">
                                            <input type="text" ng-model="post.title" auto-fill class="form-control">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-4">
                                            {{post.type=='QUESTION_AND_ANSWER' ? 'answer' : 'content'|translate}}:
                                        </div>
                                        <div class="col-sm-8 form-group">
                                            <textarea ng-model="post.description" class="form-control"></textarea>
                                        </div>
                                    </div>


                                    <div class="row" ng-if="post.type!='QUESTION_AND_ANSWER'">
                                        <div class="col-sm-4">
                                            &nbsp;
                                        </div>
                                        <div class="col-sm-8">
                                            <label>
                                                <div class="upload_style upload_style2">
                                                    <span ng-repeat="itm in filesInfo['photos'+photoType] track by $index" class="photo">

                                                        <a class="floatBtn" href ng-click="deleteItm( filesInfo['photos'+photoType], $index)">
                                                            <i class="fa fa-times"></i>
                                                        </a>
                                                        <img src="{{itm.tmp_name||set_img(itm)}}" class="img-thumbnail" >
                                                    </span>


                                                </div>

                                                <div>
                                                    <i class="fa fa-plus"></i> {{'add_photos_videos'|translate}}
                                                    <input type="file" class="form-control hideElm" multiple file-model="files.photos" name="photos{{photoType}}" ng-model="post.media[photoType]">
                                                </div>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="row"  ng-if="post.type!='QUESTION_AND_ANSWER'">
                                        <div class="col-sm-4">
                                            {{'link'|translate}}:
                                        </div>
                                        <div class="col-sm-8 form-group">
                                            <input type="text" auto-fill ng-model="post.url" class="form-control" placeholder="https://">
                                        </div>
                                    </div>

                                    <div ng-if="post.type=='OFFER'">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                {{'coupon_code'|translate}}:
                                                <i class="fa fa-info-circle" tooltip title="{{'coupon_code_tt'|translate}}"></i>
                                            </div>
                                            <div class="col-sm-8 form-group">
                                                <input type="text" auto-fill ng-model="post.couponCode" class="form-control">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-4">
                                                {{'t_cs'|translate}}:
                                                <i class="fa fa-info-circle" tooltip title="{{'t_cs_tt'|translate}}"></i>
                                            </div>
                                            <div class="col-sm-8 form-group">
                                                <textarea ng-model="post.termsAndConditions" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" ng-if="post.postOn.google && post.type == 'POST'">
                                        <div class="col-sm-4">
                                            {{'button'|translate}}: 
                                            <i class="fa fa-info-circle" tooltip title="{{'btn_g_search'|translate}}"></i>
                                        </div>
                                        <div class="col-sm-8 form-group">
                                            <select class="form-control" 
                                                    ng-model="post.g_button" 
                                                    ng-disabled="post.url.length<5">
                                                <option ng-repeat="(k,v) in ['learn_more', 'book', 'order_online', 'sign_up', 'get_offer', 'buy']"
                                                        value="{{v}}">{{v|translate}}</option>
                                            </select>
                                        </div>
                                    </div>





<!-- POST Publication date -->
                                    <div class="row" ng-if="post.type=='POST'">
                                        <div class="col-sm-4">
                                            {{'publication_date'|translate}}:
                                        </div>
                                        <div class="col-sm-4 form-group">
                                            <div class="input-group mb-3 right-rounded">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" >
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </div>
                                                <input 
                                                    class="form-control" 
                                                    type="text"
                                                    ng-model="post.publishDate"
                                                    uib-datepicker-popup="yyyy-MM-dd"
                                                    is-open="t_datepicker" 
                                                    ng-focus="t_datepicker=true"
                                                    placeholder="{{'select_date' | translate}}"
                                                    show-button-bar="false"
                                                    datepicker-options="{showWeeks:false}"
                                                    date-handler >

                                            </div>
                                        </div>
                                        <div class="col-sm-4 form-group">
                                            <div class="input-group mb-3 right-rounded">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" >
                                                        <i class="fa fa-clock-o"></i>
                                                    </span>
                                                </div>
                                                <div class="form-control">
                                                <span class="dropdown-menu" style="padding: 0 20px;">
                                                    <div uib-timepicker ng-model="timePicker" 
                                                         ng-change="post.publishTime = get_time( timePicker )"
                                                         show-meridian="false"></div>
                                                </span>
                                                <input 
                                                    style="border: none; width: 90%;"
                                                    ng-model="post.publishTime"
                                                    placeholder="{{'select_time' | translate}}" 
                                                    data-toggle="dropdown" 
                                                    aria-haspopup="true" 
                                                    aria-expanded="false"></div>
                                            </div>
                                        </div>
                                    </div>



<!-- OFFER Range date -->
                                    <div class="row"  ng-if="post.type=='OFFER'">
                                        <div class="col-sm-4">
                                            {{'publication_date'|translate}}:
                                        </div>
                                        <div class="col-sm-4 form-group">
                                            <div class="input-group mb-3 right-rounded">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" >
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control datepicker"
                                                    placeholder="{{'from' | translate}}" 
                                                    uib-datepicker-popup="yyyy-MM-dd"
                                                    datepicker-options="datePicker.fromOptions" 
                                                    is-open="startDate_is_opened" 
                                                    ng-model="filter.from" 
                                                    ng-focus="startDate_is_opened=true" 
                                                    ng-click="startDate_is_opened=true" 
                                                    onkeydown="return false" 
                                                    ng-change="datePicker.changeMinAndMaxDates()"

                                                    show-button-bar="true"
                                                    popup-placement="auto bottom-right"
                                                    date-handler />

                                            </div>
                                        </div>
                                        <div class="col-sm-4 form-group">
                                            <div class="input-group mb-3 right-rounded">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" >
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control datepicker"
                                                    placeholder="{{'to' | translate}}" 
                                                    uib-datepicker-popup="yyyy-MM-dd"
                                                    datepicker-options="datePicker.toOptions" 
                                                    is-open="endDate_is_opened" 
                                                    ng-model="filter.to" 
                                                    ng-focus="endDate_is_opened=true" 
                                                    ng-click="endDate_is_opened=true" 
                                                    onkeydown="return false" 
                                                    ng-change="datePicker.changeMinAndMaxDates()"

                                                    show-button-bar="true"
                                                    popup-placement="auto bottom-right"
                                                    date-handler />

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-4">
                                            &nbsp;
                                        </div>
                                        <div class="col-sm-8">
                                            <button class="btn btn-info" ng-click="savePost()">
                                                <i class="fa fa-save"></i> {{'save_post'|translate}}
                                            </button><br>
                                            <button class="btn btn-default btn-sm" ng-click="saveTemplate(null)">
                                                <i class="fa fa-save"></i> {{'save_as_template'|translate}}
                                            </button> 
                                            <button class="btn btn-default btn-sm" ng-click="saveTemplate(post.id)" ng-if="post.id">
                                                <i class="fa fa-pencil"></i> {{'update_template'|translate}}
                                            </button>
                                            <button class="btn btn-default btn-sm" ng-click="doReset('post'); filesInfo.photosTemplate=[];">
                                                <i class="fa fa-times"></i> {{'reset'|translate}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                                
                                

                                <!--                            PREVIEW POST    -->
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-12">
                                            
                                            <div class="nav nav-tabs" role="tablist">
                                                <a href="javascript:void()" data-target="#tab1" data-toggle="tab" class="nav-item nav-link active {{post.postOn.facebook ? '' : 'disableTab'}}" > {{'facebook'|translate}}</a>
                                                <a href="javascript:void()" data-target="#tab2" data-toggle="tab" class="nav-item nav-link {{post.postOn.google ? '' : 'disableTab'}}"> {{'google'|translate}}</a>
                                                <a href="javascript:void()" data-target="#tab3" data-toggle="tab" class="nav-item nav-link {{post.postOn.other ? '' : 'disableTab'}}">{{'directories'|translate}}</a>
                                            </div>
                                            
                                            <div class="tab-content mytab" id="myTabContent">
                    <!--                            FACBOOK -->
                                                <div class="tab-pane fade active show row" id="tab1" role="tabpanel">
                                                    <div class="col-12 post_preview">
                                                        <div class="post_header">
                                                            <img src="./Library/img/facebook-avatar.svg" alt="">&nbsp;&nbsp;
                                                            <img src="./Library/img/facebook-bars.svg" alt="">
                                                        </div>
                                                        <div>
                                                            <p>{{post.description}}</p>
                                                            <a href="{{post.url}}">{{post.url}}</a>
                                                        </div>
                                                        <div class="photos">
<!--
                                                            <div ng-repeat="itm in filesInfo.photosTemplate track by $index" img-preview>
                                                                <img src="{{itm.tmp_name||itm}}">
                                                            </div>
-->
                                                            <div class="{{set_post_class( filesInfo.photosTemplate.length )}}"
                                                                 ng-if="filesInfo.photosTemplate[0]">
                                                                <img src="{{filesInfo.photosTemplate[0].tmp_name||filesInfo.photosTemplate[0]}}">
                                                            </div>
                                                            <div class="row nopadding">
                                                                <div class="col {{set_post_class( filesInfo.photosTemplate.length+100 )}}"
                                                                     ng-if="filesInfo.photosTemplate[1]">
                                                                    <img src="{{filesInfo.photosTemplate[1].tmp_name||filesInfo.photosTemplate[1]}}">
                                                                </div>
                                                                <div class="col thumb" ng-if="filesInfo.photosTemplate[2]">
                                                                    <img src="{{filesInfo.photosTemplate[2].tmp_name||filesInfo.photosTemplate[2]}}">
                                                                </div>
                                                                <div class=" col thumb" ng-if="filesInfo.photosTemplate[3]">
                                                                    <img src="{{filesInfo.photosTemplate[3].tmp_name||filesInfo.photosTemplate[3]}}">
                                                                    <div class="counter" ng-if="filesInfo.photosTemplate.length>4">+{{filesInfo.photosTemplate.length-4}}</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="icons">
                                                            <i class="fa fa-thumbs-up"></i> 
                                                            <i class="fa fa-comment"></i> 
                                                            <i class="fa fa-share"></i> 
                                                        </div>
                                                    </div>
                                                </div>
                    <!--                            GOOGLE -->
                                                <div class="tab-pane fade row" id="tab2" role="tabpanel">
                                                    <div class="google_post">
                                                        <div class="col-12 post_preview">
                                                            <div class="post_header">
                                                                <img src="./Library/img/circle.svg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="row post_body">
                                                            <div class=" col">
                                                                <b ng-if="post.type=='QUESTION_AND_ANSWER'">{{post.title}}</b>
                                                                <h5>{{set_date('string2')}}</h5>
                                                                <p>{{post.description}}</p>
                                                            </div>
                                                            <div class="col-4"  ng-if="post.type!='QUESTION_AND_ANSWER'">
                                                                <img src="{{filesInfo.photosTemplate[0].tmp_name||filesInfo.photosTemplate[0]}}" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="post_footer"  ng-if="post.type!='QUESTION_AND_ANSWER'">
                                                            <a href="{{post.url}}">{{post.g_button|translate}}</a>
                                                        </div>
                                                    </div>
                                                </div>
                    <!--                            DIRECTORIES -->
                                                <div class="tab-pane fade row" id="tab3" role="tabpanel">
                                                        <div class="col-12 post_preview dir">
                                                            
                                                            <div class="post_header">
                                                                <div class="row">
                                                                    <div class="col-6 part1">
                                                                        <img src="./Library/img/stars5.svg" alt="">
                                                                        <p></p>
                                                                    </div>
                                                                    <div class="col-6 part2">
                                                                        <img src="./Library/img/bars2.svg" alt="">
                                                                        <img src="./Library/img/bars2.svg" alt="">
                                                                        <div>
                                                                            <div><i class="fa fa-bullhorn"></i></div>
                                                                            <div>{{post.title}}</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div>
                                                                <div class="nav nav-tabs" role="tablist">
                                                                    <a href="javascript:void()" data-target="#tab1" data-toggle="tab" class="nav-item nav-link "> </a>
                                                                    <a href="javascript:void()" data-target="#tab2" data-toggle="tab" class="nav-item nav-link"> </a>
                                                                    <a href="javascript:void()" data-target="#tab3" data-toggle="tab" class="nav-item nav-link active">{{'news'|translate}}</a>
                                                                </div>

                                                                <div class="tab-content mytab" id="myTabContent">
                                                                    <div class="tab-pane fade active show row" id="tab1" role="tabpanel">
                                                                        <b>{{post.title}}</b>
                                                                        <p>{{post.description}}</p>
                                                                        <a href="{{post.url}}">{{post.url}}</a>
                                                                    </div>
                                                                </div>
                                                                <i class="disclaimer">{{'some_dir_no_photos'|translate}}</i>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




                <!-- POSTS LIST SECTION -->
                <div class="card">
                    <div class="card-header" id="heading-posts_list" data-toggle="collapse" data-target="#posts_list" aria-expanded="true" aria-controls="posts_list">
                        <h5 class="mb-0">
                            <button class="btn btn-link">
                                <i class="fa fa-bars"></i> {{'posts_list'|translate}}
                            </button>
                      </h5>
                    

                    </div>

                    <div id="posts_list" class="collapse show" aria-labelledby="posts_list" data-parent="#accordion">
                        <div class="card-body">
                            <div class="row gridHeader hideMob">
                                <div class="col-sm-1"> {{'Type'|translate}} </div>
                                <div class="col-sm-2"> {{'Status'|translate}} </div>
                                <div class="col-sm-3"> {{'Description'|translate}} </div>
                                <div class="col-sm-2"> {{'Details'|translate}} </div>
                                <div class="col-sm-3"> {{'Insights'|translate}} </div>
                                <div class="col-sm-1"> {{'Actions'|translate}} </div>
                            </div>
                            <div class="row gridContent row-striped" ng-repeat="postItm in posts">
                                <div class="col-sm-1"> {{postItm.type|translate}} </div>
                                <div class="col-sm-3">
                                    <i class="fa fa-{{statusIcons[postItm.status.toLowerCase()]}}"></i> {{postItm.status}}
                                    <small>{{set_till_now( postItm.dateStart )}} {{'days_ago'|translate}}</small>
                                </div>
                                <div class="col-sm-2">
                                    <b>{{postItm.title}}</b>
                                    <p>{{postItm.description.substr(0, 40)}}...</p>
                                </div>
                                <div class="col-sm-2 dirs">
                                    <span ng-repeat="directory in postItm.directories">
                                        <a href="{{directory.url}}" target="_blank">
                                            <button class="btn btn-link" tooltip title="{{directory.type}}">
                                                <img src="./Library/img/directories/{{directory.type}}.png" alt="" style="height: 30px;">
                                            </button>
                                        </a>
                                    </span>
                                </div>
                                <div class="col-sm-3">
                                    <span href ng-repeat="insight in postItm.insights">
                                        <button class="btn btn-link" tooltip title="{{insight.directoryType}}">
                                            <img src="./Library/img/directories/{{insight.directoryType}}.png" alt="" style="height: 30px;">
                                        </button>
                                        <span ng-repeat="info in insight.metrics">
                                            <button class="btn btn-link" tooltip title="{{info.name}}">
                                                <i class="fa fa-{{metricsIcons[info.name]}}"></i> {{info.value}}
                                            </button>
                                        </span>
                                    </span>
                                </div>
                                <div class="col-sm-1 linkbtns">
                                    <a href ng-click="getPost(postItm.id);" tooltip title="{{'edit'|translate}}">
                                        <i class="fa fa-pencil"></i>
                                    </a> 
                                    <a href ng-click="deletePost(postItm.id);" tooltip title="{{'delete'|translate}}">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <!-- TEMPLATES LIST SECTION -->
                <div class="card">
                    <div class="card-header" id="heading-template_list" data-toggle="collapse" data-target="#template_list" aria-expanded="false" aria-controls="template_list">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed">
                                <i class="fa fa-clone"></i> {{'templates_list'|translate}}
                            </button>
                        </h5>
                    </div>
                    <div id="template_list" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            <div class="row">
                                <div class="add_btn">
                                    <button class="btn btn-success" tooltip title="{{'add_template'|translate}}"
                                            ng-click="goToTab('create_post');">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                                <div class="col-sm-2 tmpltDiv" ng-repeat="tmplt in templates">
                                    <h3>{{tmplt.title}}</h3>
                                    <div>
                                        <img src="{{tmplt.photos[0]||'./Library/img/user.png'}}" alt="">
                                    </div>
                                    <p>{{tmplt.description}}</p>
                                    <span>
                                        <button class="btn btn-info" tooltip title="{{'edit_template'|translate}}"
                                                ng-click="getTemplate(tmplt.id);">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                        <button class="btn btn-danger" tooltip title="{{'delete_template'|translate}}"
                                                ng-click="deleteTemplate(tmplt.id);">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </span>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<script>
    $(document).on('click', '.dropdown-menu', function (e) {
      e.stopPropagation();
    });
</script>