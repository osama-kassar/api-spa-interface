<section class="container customers_feedbackStyle " 
         ng-init="
                  startDate_is_opened=false; 
                  endDate_is_opened=false;
                  getFeedbacks();
                  ">
    <div class="row">




        
        
        
        
        <!--    FILTER NAV BAR    -->
        <nav class="col-12 navbar navbar-expand-lg navbar-light bg-light myNavBar">
            <a class="navbar-brand" href="#"><i class="fa fa-filter"></i> {{'filter'|translate}}</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar2" aria-controls="navbar2" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        

            <div class="collapse navbar-collapse" id="navbar2">
                <ul class="navbar-nav mr-auto">
                    
                    
                    <!--      RATINGS FILTER              -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          {{'ratings'|translate}}
                        </a>
                        <div class="dropdown-menu stars" aria-labelledby="navbarDropdown">
                            <div class="dropdown-item" href="#" 
                                 ng-repeat="(k, v) in [5,4,3,2,1] track by $index">
                                <label class="mycheckbox">
                                    <input type="checkbox" 
                                           ng-model="filter.ratingsHolder[$index]" 
                                           ng-change="chkbxVal( filter.ratings,  v )">
                                    <span></span>
                                    <img src="./Library/img/stars{{v}}.svg">
                                </label>
                            </div>
                        </div>
                    </li>
                    
                    
                    <!--      STATUS FILTER              -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          {{'status'|translate}}
                        </a>
                        <div class="dropdown-menu stars" aria-labelledby="navbarDropdown">
                            <div class="dropdown-item" href="#" 
                                 ng-repeat="(k, v) in ['read', 'unread', 'replied', 'unreplied'] track by $index">
                                <label class="mycheckbox">
                                    <input type="checkbox" name="{{v}}" 
                                           ng-model="filter.statusHolder[$index]"
                                           ng-change="chkbxVal( filter.status,  v )">
                                    <span></span> {{v|translate}}
                                </label>
                            </div>
                        </div>
                    </li>
                    
                    
                    <!--      TYPES FILTER              -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          {{'directoryTypes'|translate}}
                        </a>
                        <div class="dropdown-menu stars" aria-labelledby="navbarDropdown" style="overflow-y: scroll; max-height: 250px;">
                            <div class="dropdown-item" href="#" 
                                 ng-repeat="(k, v) in directoyTypes track by $index">
                                <label class="mycheckbox">
                                    <input type="checkbox" 
                                           ng-model="filter.typesHolder[$index]"
                                           ng-change="chkbxVal( filter.types,  v )"
                                           > 
                                    <span></span>
                                    {{v}}
                                </label>
                            </div>
                        </div>
                    </li>
                    
                    
                    <!--      Feedback Type FILTER              -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          {{'feedback_types'|translate}}
                        </a>
                        <div class="dropdown-menu stars" aria-labelledby="navbarDropdown">
                            <div class="dropdown-item" href="#" 
                                 ng-repeat="(k, v) in ['PHOTO', 'REVIEW', 'QUESTION'] track by $index">
                                <label class="mycheckbox">
                                    <input type="checkbox" 
                                           ng-model="filter.dataPointTypesHolder[$index]" 
                                           ng-change="chkbxVal( filter.dataPointTypes,  v )">
                                    <span></span>
                                    {{v|translate}}
                                </label>
                            </div>
                        </div>
                    </li>
                    
                    
                </ul>
                
                
                
                
                <form class="form-inline ">
                    <div class="input-group right-rounded mr-sm-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text" >
                                <b>{{'from' | translate}}</b>&nbsp; <i class="fa fa-calendar"></i> 
                            </span>
                        </div>
                        <input type="text" class="form-control datepicker"
                            placeholder="{{'from' | translate}}" 
                            uib-datepicker-popup="yyyy-MM-dd"
                            datepicker-options="datePicker.fromOptions" 
                            is-open="startDate_is_opened" 
                            ng-model="filter.from" 
                            ng-focus="startDate_is_opened=true" 
                            ng-click="startDate_is_opened=true" 
                            onkeydown="return false" 
                            ng-change="datePicker.changeMinAndMaxDates()"
                               
                            show-button-bar="false"
                            popup-placement="auto bottom-right"
                            date-handler />
                    </div>
                    
                    <div class="input-group right-rounded mr-sm-2 ">
                        <div class="input-group-prepend">
                            <span class="input-group-text" >
                                <b>{{'to' | translate}}</b>&nbsp; <i class="fa fa-calendar"></i> 
                            </span>
                        </div>
                         <input type="text" class="form-control datepicker"
                            placeholder="{{'to' | translate}}" 
                            uib-datepicker-popup="yyyy-MM-dd"
                            datepicker-options="datePicker.toOptions" 
                            is-open="endDate_is_opened" 
                            ng-model="filter.to" 
                            ng-focus="endDate_is_opened=true" 
                            ng-click="endDate_is_opened=true" 
                            onkeydown="return false" 
                            ng-change="datePicker.changeMinAndMaxDates()"
                                
                            popup-placement="auto bottom-right"
                            show-button-bar="false"
                            date-handler />
                    </div>
                    
<!--
                    <div class="input-group right-rounded mr-sm-2">
                        <input class="form-control mr-sm-2" type="search" placeholder="{{'Search'|translate}}" aria-label="Search">
                    </div>
-->
                    
                    <div class="input-group right-rounded mr-sm-2">
                        <button class="btn btn-info" ng-click="ConstumersFeedbacks={}; currPage=1; getFeedbacks();">{{'GO'|translate}}</button>
                    </div>
                </form>
            </div>
        </nav>
        
        
        <!--        SUB FILTER          -->
        <div class="col-12 filter_show">
            <div class="row">
                <div class="col-sm-1">{{'result'|translate}} {{total}}</div>
                <div class="col-sm-11">
                    <div class="filter_group" ng-if="filter.ratings.length>0">
                        <span ng-repeat="(k, v) in filter.ratings track by $index">
                            {{v}} <i class="fa fa-star goldText"></i>
                        </span>
                    </div>
                    <div class="filter_group" ng-if="filter.status.length>0">
                        <span ng-repeat="(k, v) in filter.status track by $index">
                            {{v|translate}}
                        </span>
                    </div>
                    <div class="filter_group" ng-if="filter.types.length>0">
                        <span ng-repeat="(k, v) in filter.types">
                            {{v}}
                        </span>
                    </div>
                    <div class="filter_group" ng-if="filter.dataPointTypes.length>0">
                        <span ng-repeat="(k, v) in filter.dataPointTypes">
                            {{v}}
                        </span>
                    </div>
                    <div class="filter_group">
                        <span ng-if="filter.from">
                            {{filter.from}} <i class="fa fa-arrow-right"></i>
                        </span>
                        <span ng-if="filter.to">
                            {{filter.to}} <i class="fa fa-arrow-left"></i>
                        </span>
                    </div>
                    
                        <br>

                    <div class="filter_group" ng-if="filter.status.length > 0 || filter.types.length > 0 || filter.ratings.length > 0 || filter.from.length > 0 || filter.to.length > 0 ">
<!--                        <a href><i class="fa fa-save"></i> {{'save_current_filter'|translate}}</a> -->
                        <a href ng-click="resetFilter()"><i class="fa fa-times"></i> {{'reset_filter'|translate}}</a>
                    </div>
                </div>
            </div>
        </div>
        
        
        
        
        <hr style="width: 100%;">
        
        
        <!--        MULTIPLE HANDLING          -->
        <div class="col-12 ">
            <label class="mycheckbox" tooltip title="{{'select_all'|translate}}">
                <input type="checkbox" ng-change="selectAll('feedbackItems', switcher)" ng-model="switcher">  &nbsp; &nbsp; <span></span> &nbsp; &nbsp;
            </label>
            <div class="btn-group btn-group-sm" role="group">
                <button class="btn btn-primary btn-sm "
                        ng-disabled="!isApproval"
                        ng-click="isApproval=0;  currPage=1; getFeedbacks(true);">
                    <i class="fa fa-bars"></i> {{'all'|translate}}
                </button>
                <button class="btn btn-primary btn-sm"
                        ng-disabled="isApproval"
                        ng-click="isApproval=1; currPage=1;  getFeedbacks(true);">
                    <i class="fa fa-bookmark"></i> {{'approval'|translate}}
                </button>
            </div>
            <button class="btn btn-default btn-sm" ng-click="MarkAsRead()" id="as_read_btn"><span><i class="fa fa-envelope-open-o"></i></span> {{'mark_as_read'|translate}}</button>
        </div>
        
        
        
        
        
        
        
        <hr style="width: 100%;">







        <!-- FEEDBACKS LIST       -->
        <div class="row" style="background: #fff; margin-right:0px; margin-left: 0px; width: 100%;">
            <div class="col-md-3 feedbacks_list">
                <section class="row ">
                    <div ng-repeat="itm in ConstumersFeedbacks" 
                         ng-click="setItem(itm)"
                         class="col-12">
                        <label class="mycheckbox">
                            <h3>
                                <input type="checkbox" name="itm_{{$index}}" class="feedbackItems" 
                                       ng-false-value="false"
                                       ng-ture-value="{{itm.id}}">
                                <span></span> {{user.userInfo.salesPartner.name}}
                            </h3>
                        </label>
                        <div>{{itm.directoryType}}</div>
                        <div ng-if="itm.rating != null">
                            <img src="./Library/img/stars{{itm.rating}}.svg" alt="">
                        </div>
                        <img src="{{itm.data}}" alt="" style="height:100px; width:100px;" ng-if="itm.type=='PHOTO'" class="img-thumbnail" >
                        <p ng-if="itm.type!=='PHOTO'">{{itm.data}}</p>
                        <div>
                            <a href="{{itm.directLink}}" target="_blank">
                                <i class="fa fa-external-link"></i> {{'show_original'|translate}}
                            </a>
                        </div>
                        <hr>
                    </div>
                    <button ng-if="total > ConstumersFeedbacks.length" 
                            class="col-12 mb-5" id="show_more" 
                            ng-click="getFeedbacks()">
                        <span><i class="fa fa-plus"></i></span> 
                    {{total +"/"+ ConstumersFeedbacks.length}} {{'show_more'|translate}}
                    </button>
                </section>
            </div>


            <!-- FEEDBACK ONE ITEM SHOW        -->
            <div class="col-md-9 feedback_view" >
                <section class="mb-3">
                    <h3>{{user.userInfo.salesPartner.name}}</h3>
                    <span>
                        <img src="./Library/img/directories/{{feedbackItem.directoryType}}.png" style="height: 30px;" class="img-thumbnail" >
                    </span>
                    <small>{{feedbackItem.directoryType}} - {{set_till_now(feedbackItem.dateCreated)}} {{'days_ago'|translate}}</small>
                    <div ng-if="feedbackItem.rating != null">
                        <img src="./Library/img/stars{{feedbackItem.rating}}.svg" alt="">
                    </div>
                </section>
                <div ng-if="feedbackItem.type.length > 0">
                    <section class="mb-3">
                        <span>
                            <img src="{{feedbackItem.authorImage||'./Library/img/user.png'}}" style="height: 30px;" class="img-thumbnail" >
                        </span>
                        <small>{{feedbackItem.author}}</small>
                    </section>

                    <section class="mb-3">
                        <div ng-if="feedbackItem.type == 'PHOTO'" style=" padding: 0 20%">
                            <img src="{{feedbackItem.data}}" class="img-thumbnail" alt="" style="max-height:300px;" 
                                 show-img>
                            <div>
                                <a href="{{feedbackItem.directLink}}" target="_blank">
                                    <i class="fa fa-external-link"></i> {{'show_original'|translate}}
                                </a>
                            </div>
                        </div>

                        <div ng-if="feedbackItem.type !== 'PHOTO'">
                            <div class="mb-5">
                                <p>{{feedbackItem.data}}</p>
                                <a href="{{feedbackItem.directLink}}" target="_blank">
                                    <i class="fa fa-external-link"></i> {{'show_original'|translate}}
                                </a>
                            </div>
                            <div class="mb-5">
                                <div class="mb-1">
                                    <textarea class="form-control" ng-model="feedbackItem.reply"></textarea>
                                </div>
                                <button class="btn btn-info " ng-click="reply()" id="reply_btn">
                                    <span><i class="fa fa-send"></i> </span> {{'reply'|translate}}
                                </button>
                            </div>
                        </div>
                    </section>
                </div>

                <div ng-if="!feedbackItem.type">
    <!--                <img src="./Library/img/nodatafound.png" alt="" style="height: 140px"> <br>{{'empty_result'|translate}}</div>-->

                <section class="mb-5"> </section>
            </div>
        </div>
    </div>
</section>



<script>
    $(document).on('click', '#navbar2 .dropdown-menu div.dropdown-item', function (e) {
      e.stopPropagation();
    });
</script>