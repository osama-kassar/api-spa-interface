<!-- Products Tabs -->
      <section class="pro-content pro-tab-content">
        <div class="container"> 
          <div class="products-area">

            <div class="row justify-content-center">
              <div class="col-12 col-lg-12">
                <div class="pro-heading-title">
                  <h1> Welcome to Store</h1>
                </div>
              </div>
            </div>
            

            <!-- ..........tabs start ......... -->
            <div class="row ">
              <div class="col-md-12">
                <div class="nav" role="tablist" id="tabCarousel">
                  <a class="nav-link  active" data-toggle="tab" href="#featured" role="tab" >Top Sales</a> 

                  <a class="nav-link"  data-toggle="tab" href="#special" role="tab" aria-controls="special" aria-selected="true">Trending</a> 

                  <a class="nav-link" data-toggle="tab"  href="#liked" role="tab" aria-controls="liked" aria-selected="true">Winter Sale</a> 
                </div> 
                <!-- Tab panes -->
                <div class="tab-content">
                
                  <div role="tabpanel" class="tab-pane fade active show" id="featured">
                      <div class="tab-carousel-js row">                         
                          <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                              <div class="product">
                                  <article>
                                      <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                                          <a href="wishlist.html" class="icon active swipe-to-top">
                                            <i class="fas fa-heart"></i>
                                          </a>
                                          <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                            <i class="fas fa-eye"></i>
                                          </div>
                                          <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                          </div>
                                                                      
                                    <div class="pro-thumb "> 
                                      <div class="pro-tag bg-primary">Featured</div>
                                      <a href="product-page1.html">
                                          <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_01.jpg" alt="Product Image"></span>
                                          <span class="pro-image-hover swipe-to-top  d-none d-lg-block d-xl-block"><img class="img-fluid" src="images/product_images/product_image_01_02.jpg" alt="Product Image"></span>
                                      </a>
                                      <div class="pro-buttons d-none d-lg-block d-xl-block">
                                            <div class="pro-icons">
                                            <a href="wishlist.html" class="icon active swipe-to-top">
                                              <i class="fas fa-heart"></i>
                                            </a>
                                            <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                              <i class="fas fa-eye"></i>
                                            </div>
                                            <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                            </div>
                                          
          
                                          <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                        </div>
                                      
                                    </div>
                                    <div class="pro-description">
                                        <span class="pro-info">
                                            Ring Collection                   
                                          </span>
                                          <h2 class="pro-title"><a href="product-page1.html">Austrian Crystals Engagement Jewelry Ring</a></h2>
                                          
                                          <div class="pro-price">
                                            <ins>$285</ins>
                                        </div>
                                        <div class="pro-mobile-buttons d-lg-none d-xl-none">
                                            <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                            
                
                                      </div>
                                    </div>
                                        
                                  </article>
                                </div>
                          </div>
                          <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                              <div class="product">
                                  <article>
                                      <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                                          <a href="wishlist.html" class="icon active swipe-to-top">
                                            <i class="fas fa-heart"></i>
                                          </a>
                                          <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                            <i class="fas fa-eye"></i>
                                          </div>
                                          <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                          </div>
                                                                    
                                    <div class="pro-thumb "> 
                                      
                                      <a href="product-page1.html">
                                          <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_02.jpg" alt="Product Image"></span>
                                          <span class="pro-image-hover swipe-to-top  d-none d-lg-block d-xl-block">
                                            <img class="img-fluid" src="images/product_images/product_image_02_02.jpg" alt="Product Image"></span>
                                      </a>
                                      <div class="pro-buttons d-none d-lg-block d-xl-block">
                                            <div class="pro-icons">
                                            <a href="wishlist.html" class="icon active swipe-to-top">
                                              <i class="fas fa-heart"></i>
                                            </a>
                                            <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                              <i class="fas fa-eye"></i>
                                            </div>
                                            <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                            </div>
                                          
          
                                          <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                        </div>
                                      
                                    </div>
                                    <div class="pro-description">
                                        <span class="pro-info">
                                            Earrings                   
                                          </span>
                                          <h2 class="pro-title"><a href="product-page1.html">Crystal Water Drop Wedding Function  Earrings</a></h2>
                                          
                                          <div class="pro-price">
                                            <del>$450</del><ins>$285</ins>
                                        </div>
                                        <div class="pro-mobile-buttons d-lg-none d-xl-none">
                                            <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                            
                
                                      </div>
                                    </div>
                                        
                                  </article>
                              </div>
                            </div>
                          <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="product">
                              <article>     
                                  <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                                      <a href="wishlist.html" class="icon active swipe-to-top">
                                        <i class="fas fa-heart"></i>
                                      </a>
                                      <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                        <i class="fas fa-eye"></i>
                                      </div>
                                      <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                      </div>                              
                                <div class="pro-thumb "> 
                                    <div class="pro-tag">Sale</div>
                                  <a href="product-page1.html">
                                      <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_03.jpg" alt="Product Image"></span>
                                      <span class="pro-image-hover swipe-to-top d-none d-lg-block d-xl-block"><img class="img-fluid" src="images/product_images/product_image_03_02.jpg" alt="Product Image"></span>
                                  </a>
                                  <div class="pro-buttons d-none d-lg-block d-xl-block">
                                        <div class="pro-icons">
                                        <a href="wishlist.html" class="icon active swipe-to-top">
                                          <i class="fas fa-heart"></i>
                                        </a>
                                        <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                          <i class="fas fa-eye"></i>
                                        </div>
                                        <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                        </div>
                                      
    
                                      <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                    </div>
                                
                                </div>
                                <div class="pro-description">
                                    <span class="pro-info">
                                      Ring Collection                          
                                      </span>
                                      <h2 class="pro-title"><a href="product-page1.html">Crytal Wedding Function Rings</a></h2>
                                      
                                      <div class="pro-price">
                                        <del>$120</del><ins>$85</ins>
                                    </div>
                                    <div class="pro-mobile-buttons d-lg-none d-xl-none">
                                        <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                        
            
                                  </div>
                                </div>
                                    
                              </article>
                            </div>
                          </div>
                          <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="product">
                                <article>
                                    <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                                        <a href="wishlist.html" class="icon active swipe-to-top">
                                          <i class="fas fa-heart"></i>
                                        </a>
                                        <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                          <i class="fas fa-eye"></i>
                                        </div>
                                        <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                        </div>
                                                                  
                                  <div class="pro-thumb "> 
                                    <div class="pro-tag bg-success">NEW</div>
                                    <a href="product-page1.html">
                                        <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_04.jpg" alt="Product Image"></span>
                                        <span class="pro-image-hover swipe-to-top  d-none d-lg-block d-xl-block"><img class="img-fluid" src="images/product_images/product_image_04_02.jpg" alt="Product Image"></span>
                                    </a>
                                    <div class="pro-buttons d-none d-lg-block d-xl-block">
                                          <div class="pro-icons">
                                          <a href="wishlist.html" class="icon active swipe-to-top">
                                            <i class="fas fa-heart"></i>
                                          </a>
                                          <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                            <i class="fas fa-eye"></i>
                                          </div>
                                          <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                          </div>
                                        
        
                                        <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                      </div>
                                    
                                  </div>
                                  <div class="pro-description">
                                      <span class="pro-info">
                                          Rings                   
                                        </span>
                                        <h2 class="pro-title"><a href="product-page1.html">Women Crystal Zircon Engagement And Wedding Ring</a></h2>
                                        
                                        <div class="pro-price">
                                          <del>$120</del><ins>$110</ins>
                                      </div>
                                      <div class="pro-mobile-buttons d-lg-none d-xl-none">
                                          <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                          
              
                                    </div>
                                  </div>
                                      
                                </article>
                            </div>
                          </div>
                          
                          <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="product">
                                <article>
                                    <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                                        <a href="wishlist.html" class="icon active swipe-to-top">
                                          <i class="fas fa-heart"></i>
                                        </a>
                                        <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                          <i class="fas fa-eye"></i>
                                        </div>
                                        <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                        </div>
                                                                  
                                  <div class="pro-thumb "> 
                                    <div class="pro-tag bg-success">NEW</div>
                                    <a href="product-page1.html">
                                        <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_05.jpg" alt="Product Image"></span>
                                        <span class="pro-image-hover swipe-to-top  d-none d-lg-block d-xl-block">
                                          <img class="img-fluid" src="images/product_images/product_image_05_02.jpg" alt="Product Image"></span>
                                    </a>
                                    <div class="pro-buttons d-none d-lg-block d-xl-block">
                                          <div class="pro-icons">
                                          <a href="wishlist.html" class="icon active swipe-to-top">
                                            <i class="fas fa-heart"></i>
                                          </a>
                                          <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                            <i class="fas fa-eye"></i>
                                          </div>
                                          <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                          </div>
                                        
        
                                        <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                      </div>
                                    
                                  </div>
                                  <div class="pro-description">
                                      <span class="pro-info">
                                          Bangle                  
                                        </span>
                                        <h2 class="pro-title"><a href="product-page1.html">Viennois Rose Gold Circle Bangle for Wedding</a></h2>
                                        
                                        <div class="pro-price">
                                          <del>$220</del><ins>$185</ins>
                                      </div>
                                      <div class="pro-mobile-buttons d-lg-none d-xl-none">
                                          <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                          
              
                                    </div>
                                  </div>
                                      
                                </article>
                            </div>
                          </div>
                          <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="product">
                                <article>
                                    <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                                        <a href="wishlist.html" class="icon active swipe-to-top">
                                          <i class="fas fa-heart"></i>
                                        </a>
                                        <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                          <i class="fas fa-eye"></i>
                                        </div>
                                        <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                        </div>
                                                                  
                                  <div class="pro-thumb "> 
                                    <div class="pro-tag bg-success">NEW</div>
                                    <a href="product-page1.html">
                                        <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_06.jpg" alt="Product Image"></span>
                                        <span class="pro-image-hover swipe-to-top  d-none d-lg-block d-xl-block">
                                          <img class="img-fluid" src="images/product_images/product_image_06_02.jpg" alt="Product Image"></span>
                                    </a>
                                    <div class="pro-buttons d-none d-lg-block d-xl-block">
                                          <div class="pro-icons">
                                          <a href="wishlist.html" class="icon active swipe-to-top">
                                            <i class="fas fa-heart"></i>
                                          </a>
                                          <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                            <i class="fas fa-eye"></i>
                                          </div>
                                          <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                          </div>
                                        
        
                                        <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                      </div>
                                    
                                  </div>
                                  <div class="pro-description">
                                      <span class="pro-info">
                                          Bracelet                 
                                        </span>
                                        <h2 class="pro-title"><a href="product-page1.html">Bracelet Women Metal Chain for Engagement</a></h2>
                                        
                                        <div class="pro-price">
                                          <del>$140</del><ins>$81</ins>
                                      </div>
                                      <div class="pro-mobile-buttons d-lg-none d-xl-none">
                                          <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                          
              
                                    </div>
                                  </div>
                                      
                                </article>
                            </div>
                          </div>
                          <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                              <div class="product">
                                  <article>
                                      <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                                          <a href="wishlist.html" class="icon active swipe-to-top">
                                            <i class="fas fa-heart"></i>
                                          </a>
                                          <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                            <i class="fas fa-eye"></i>
                                          </div>
                                          <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                          </div>
                                                                    
                                    <div class="pro-thumb "> 
                                      <div class="pro-tag bg-success">NEW</div>
                                      <a href="product-page1.html">
                                          <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_07.jpg" alt="Product Image"></span>
                                          <span class="pro-image-hover swipe-to-top  d-none d-lg-block d-xl-block">
                                            <img class="img-fluid" src="images/product_images/product_image_07_02.jpg" alt="Product Image"></span>
                                      </a>
                                      <div class="pro-buttons d-none d-lg-block d-xl-block">
                                            <div class="pro-icons">
                                            <a href="wishlist.html" class="icon active swipe-to-top">
                                              <i class="fas fa-heart"></i>
                                            </a>
                                            <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                              <i class="fas fa-eye"></i>
                                            </div>
                                            <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                            </div>
                                          
          
                                          <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                        </div>
                                      
                                    </div>
                                    <div class="pro-description">
                                        <span class="pro-info">
                                            Crown                 
                                          </span>
                                          <h2 class="pro-title"><a href="product-page1.html">Gold Rhinestone Pageant Crown for Women</a></h2>
                                          
                                          <div class="pro-price">
                                            <del>$120</del><ins>$85</ins>
                                        </div>
                                        <div class="pro-mobile-buttons d-lg-none d-xl-none">
                                            <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                            
                
                                      </div>
                                    </div>
                                        
                                  </article>
                              </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="product">
                                    <article>
                                        <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                                            <a href="wishlist.html" class="icon active swipe-to-top">
                                              <i class="fas fa-heart"></i>
                                            </a>
                                            <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                              <i class="fas fa-eye"></i>
                                            </div>
                                            <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                            </div>
                                                                      
                                      <div class="pro-thumb "> 
                                        <div class="pro-tag bg-success">NEW</div>
                                        <a href="product-page1.html">
                                            <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_08.jpg" alt="Product Image"></span>
                                            <span class="pro-image-hover swipe-to-top  d-none d-lg-block d-xl-block">
                                              <img class="img-fluid" src="images/product_images/product_image_08_02.jpg" alt="Product Image"></span>
                                        </a>
                                        <div class="pro-buttons d-none d-lg-block d-xl-block">
                                              <div class="pro-icons">
                                              <a href="wishlist.html" class="icon active swipe-to-top">
                                                <i class="fas fa-heart"></i>
                                              </a>
                                              <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                                <i class="fas fa-eye"></i>
                                              </div>
                                              <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                              </div>
                                            
            
                                            <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                          </div>
                                        
                                      </div>
                                      <div class="pro-description">
                                          <span class="pro-info">
                                              Necklace                 
                                            </span>
                                            <h2 class="pro-title"><a href="product-page1.html">Romantic Love Hollow Heart Necklace for Women</a></h2>
                                            
                                            <div class="pro-price">
                                              <ins>$85</ins>
                                          </div>
                                          <div class="pro-mobile-buttons d-lg-none d-xl-none">
                                              <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                              
                  
                                        </div>
                                      </div>
                                          
                                    </article>
                                </div>
                              </div>
                        </div>
                    <!-- 1st tab --> 
                  </div>

                  <div role="tabpanel" class="tab-pane fade" id="special">
                      <div class="tab-carousel-js row">                         
                          <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                              <div class="product">
                                  <article>
                                      <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                                          <a href="wishlist.html" class="icon active swipe-to-top">
                                            <i class="fas fa-heart"></i>
                                          </a>
                                          <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                            <i class="fas fa-eye"></i>
                                          </div>
                                          <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                          </div>
                                                                      
                                    <div class="pro-thumb "> 
                                      <div class="pro-tag bg-primary">Featured</div>
                                      <a href="product-page1.html">
                                          <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_01.jpg" alt="Product Image"></span>
                                          <span class="pro-image-hover swipe-to-top  d-none d-lg-block d-xl-block"><img class="img-fluid" src="images/product_images/product_image_01_02.jpg" alt="Product Image"></span>
                                      </a>
                                      <div class="pro-buttons d-none d-lg-block d-xl-block">
                                            <div class="pro-icons">
                                            <a href="wishlist.html" class="icon active swipe-to-top">
                                              <i class="fas fa-heart"></i>
                                            </a>
                                            <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                              <i class="fas fa-eye"></i>
                                            </div>
                                            <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                            </div>
                                          
          
                                          <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                        </div>
                                      
                                    </div>
                                    <div class="pro-description">
                                        <span class="pro-info">
                                            Ring Collection                   
                                          </span>
                                          <h2 class="pro-title"><a href="product-page1.html">Austrian Crystals Engagement Jewelry Ring</a></h2>
                                          
                                          <div class="pro-price">
                                            <ins>$285</ins>
                                        </div>
                                        <div class="pro-mobile-buttons d-lg-none d-xl-none">
                                            <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                            
                
                                      </div>
                                    </div>
                                        
                                  </article>
                                </div>
                          </div>
                          <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                              <div class="product">
                                  <article>
                                      <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                                          <a href="wishlist.html" class="icon active swipe-to-top">
                                            <i class="fas fa-heart"></i>
                                          </a>
                                          <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                            <i class="fas fa-eye"></i>
                                          </div>
                                          <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                          </div>
                                                                    
                                    <div class="pro-thumb "> 
                                      
                                      <a href="product-page1.html">
                                          <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_02.jpg" alt="Product Image"></span>
                                          <span class="pro-image-hover swipe-to-top  d-none d-lg-block d-xl-block">
                                            <img class="img-fluid" src="images/product_images/product_image_02_02.jpg" alt="Product Image"></span>
                                      </a>
                                      <div class="pro-buttons d-none d-lg-block d-xl-block">
                                            <div class="pro-icons">
                                            <a href="wishlist.html" class="icon active swipe-to-top">
                                              <i class="fas fa-heart"></i>
                                            </a>
                                            <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                              <i class="fas fa-eye"></i>
                                            </div>
                                            <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                            </div>
                                          
          
                                          <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                        </div>
                                      
                                    </div>
                                    <div class="pro-description">
                                        <span class="pro-info">
                                            Earrings                   
                                          </span>
                                          <h2 class="pro-title"><a href="product-page1.html">Crystal Water Drop Wedding Function  Earrings</a></h2>
                                          
                                          <div class="pro-price">
                                            <del>$450</del><ins>$285</ins>
                                        </div>
                                        <div class="pro-mobile-buttons d-lg-none d-xl-none">
                                            <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                            
                
                                      </div>
                                    </div>
                                        
                                  </article>
                              </div>
                            </div>
                          <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="product">
                              <article>     
                                  <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                                      <a href="wishlist.html" class="icon active swipe-to-top">
                                        <i class="fas fa-heart"></i>
                                      </a>
                                      <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                        <i class="fas fa-eye"></i>
                                      </div>
                                      <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                      </div>                              
                                <div class="pro-thumb "> 
                                    <div class="pro-tag">Sale</div>
                                  <a href="product-page1.html">
                                      <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_03.jpg" alt="Product Image"></span>
                                      <span class="pro-image-hover swipe-to-top d-none d-lg-block d-xl-block"><img class="img-fluid" src="images/product_images/product_image_03_02.jpg" alt="Product Image"></span>
                                  </a>
                                  <div class="pro-buttons d-none d-lg-block d-xl-block">
                                        <div class="pro-icons">
                                        <a href="wishlist.html" class="icon active swipe-to-top">
                                          <i class="fas fa-heart"></i>
                                        </a>
                                        <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                          <i class="fas fa-eye"></i>
                                        </div>
                                        <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                        </div>
                                      
    
                                      <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                    </div>
                                
                                </div>
                                <div class="pro-description">
                                    <span class="pro-info">
                                      Ring Collection                          
                                      </span>
                                      <h2 class="pro-title"><a href="product-page1.html">Crytal Wedding Function Rings</a></h2>
                                      
                                      <div class="pro-price">
                                        <del>$120</del><ins>$85</ins>
                                    </div>
                                    <div class="pro-mobile-buttons d-lg-none d-xl-none">
                                        <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                        
            
                                  </div>
                                </div>
                                    
                              </article>
                            </div>
                          </div>
                          <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="product">
                                <article>
                                    <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                                        <a href="wishlist.html" class="icon active swipe-to-top">
                                          <i class="fas fa-heart"></i>
                                        </a>
                                        <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                          <i class="fas fa-eye"></i>
                                        </div>
                                        <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                        </div>
                                                                  
                                  <div class="pro-thumb "> 
                                    <div class="pro-tag bg-success">NEW</div>
                                    <a href="product-page1.html">
                                        <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_04.jpg" alt="Product Image"></span>
                                        <span class="pro-image-hover swipe-to-top  d-none d-lg-block d-xl-block"><img class="img-fluid" src="images/product_images/product_image_04_02.jpg" alt="Product Image"></span>
                                    </a>
                                    <div class="pro-buttons d-none d-lg-block d-xl-block">
                                          <div class="pro-icons">
                                          <a href="wishlist.html" class="icon active swipe-to-top">
                                            <i class="fas fa-heart"></i>
                                          </a>
                                          <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                            <i class="fas fa-eye"></i>
                                          </div>
                                          <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                          </div>
                                        
        
                                        <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                      </div>
                                    
                                  </div>
                                  <div class="pro-description">
                                      <span class="pro-info">
                                          Rings                   
                                        </span>
                                        <h2 class="pro-title"><a href="product-page1.html">Women Crystal Zircon Engagement And Wedding Ring</a></h2>
                                        
                                        <div class="pro-price">
                                          <del>$120</del><ins>$110</ins>
                                      </div>
                                      <div class="pro-mobile-buttons d-lg-none d-xl-none">
                                          <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                          
              
                                    </div>
                                  </div>
                                      
                                </article>
                            </div>
                          </div>
                          
                          <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="product">
                                <article>
                                    <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                                        <a href="wishlist.html" class="icon active swipe-to-top">
                                          <i class="fas fa-heart"></i>
                                        </a>
                                        <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                          <i class="fas fa-eye"></i>
                                        </div>
                                        <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                        </div>
                                                                  
                                  <div class="pro-thumb "> 
                                    <div class="pro-tag bg-success">NEW</div>
                                    <a href="product-page1.html">
                                        <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_05.jpg" alt="Product Image"></span>
                                        <span class="pro-image-hover swipe-to-top  d-none d-lg-block d-xl-block">
                                          <img class="img-fluid" src="images/product_images/product_image_05_02.jpg" alt="Product Image"></span>
                                    </a>
                                    <div class="pro-buttons d-none d-lg-block d-xl-block">
                                          <div class="pro-icons">
                                          <a href="wishlist.html" class="icon active swipe-to-top">
                                            <i class="fas fa-heart"></i>
                                          </a>
                                          <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                            <i class="fas fa-eye"></i>
                                          </div>
                                          <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                          </div>
                                        
        
                                        <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                      </div>
                                    
                                  </div>
                                  <div class="pro-description">
                                      <span class="pro-info">
                                          Bangle                  
                                        </span>
                                        <h2 class="pro-title"><a href="product-page1.html">Viennois Rose Gold Circle Bangle for Wedding</a></h2>
                                        
                                        <div class="pro-price">
                                          <del>$220</del><ins>$185</ins>
                                      </div>
                                      <div class="pro-mobile-buttons d-lg-none d-xl-none">
                                          <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                          
              
                                    </div>
                                  </div>
                                      
                                </article>
                            </div>
                          </div>
                          <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="product">
                                <article>
                                    <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                                        <a href="wishlist.html" class="icon active swipe-to-top">
                                          <i class="fas fa-heart"></i>
                                        </a>
                                        <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                          <i class="fas fa-eye"></i>
                                        </div>
                                        <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                        </div>
                                                                  
                                  <div class="pro-thumb "> 
                                    <div class="pro-tag bg-success">NEW</div>
                                    <a href="product-page1.html">
                                        <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_06.jpg" alt="Product Image"></span>
                                        <span class="pro-image-hover swipe-to-top  d-none d-lg-block d-xl-block">
                                          <img class="img-fluid" src="images/product_images/product_image_06_02.jpg" alt="Product Image"></span>
                                    </a>
                                    <div class="pro-buttons d-none d-lg-block d-xl-block">
                                          <div class="pro-icons">
                                          <a href="wishlist.html" class="icon active swipe-to-top">
                                            <i class="fas fa-heart"></i>
                                          </a>
                                          <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                            <i class="fas fa-eye"></i>
                                          </div>
                                          <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                          </div>
                                        
        
                                        <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                      </div>
                                    
                                  </div>
                                  <div class="pro-description">
                                      <span class="pro-info">
                                          Bracelet                 
                                        </span>
                                        <h2 class="pro-title"><a href="product-page1.html">Bracelet Women Metal Chain for Engagement</a></h2>
                                        
                                        <div class="pro-price">
                                          <del>$140</del><ins>$81</ins>
                                      </div>
                                      <div class="pro-mobile-buttons d-lg-none d-xl-none">
                                          <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                          
              
                                    </div>
                                  </div>
                                      
                                </article>
                            </div>
                          </div>
                          <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                              <div class="product">
                                  <article>
                                      <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                                          <a href="wishlist.html" class="icon active swipe-to-top">
                                            <i class="fas fa-heart"></i>
                                          </a>
                                          <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                            <i class="fas fa-eye"></i>
                                          </div>
                                          <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                          </div>
                                                                    
                                    <div class="pro-thumb "> 
                                      <div class="pro-tag bg-success">NEW</div>
                                      <a href="product-page1.html">
                                          <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_07.jpg" alt="Product Image"></span>
                                          <span class="pro-image-hover swipe-to-top  d-none d-lg-block d-xl-block">
                                            <img class="img-fluid" src="images/product_images/product_image_07_02.jpg" alt="Product Image"></span>
                                      </a>
                                      <div class="pro-buttons d-none d-lg-block d-xl-block">
                                            <div class="pro-icons">
                                            <a href="wishlist.html" class="icon active swipe-to-top">
                                              <i class="fas fa-heart"></i>
                                            </a>
                                            <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                              <i class="fas fa-eye"></i>
                                            </div>
                                            <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                            </div>
                                          
          
                                          <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                        </div>
                                      
                                    </div>
                                    <div class="pro-description">
                                        <span class="pro-info">
                                            Crown                 
                                          </span>
                                          <h2 class="pro-title"><a href="product-page1.html">Gold Rhinestone Pageant Crown for Women</a></h2>
                                          
                                          <div class="pro-price">
                                            <del>$120</del><ins>$85</ins>
                                        </div>
                                        <div class="pro-mobile-buttons d-lg-none d-xl-none">
                                            <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                            
                
                                      </div>
                                    </div>
                                        
                                  </article>
                              </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="product">
                                    <article>
                                        <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                                            <a href="wishlist.html" class="icon active swipe-to-top">
                                              <i class="fas fa-heart"></i>
                                            </a>
                                            <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                              <i class="fas fa-eye"></i>
                                            </div>
                                            <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                            </div>
                                                                      
                                      <div class="pro-thumb "> 
                                        <div class="pro-tag bg-success">NEW</div>
                                        <a href="product-page1.html">
                                            <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_08.jpg" alt="Product Image"></span>
                                            <span class="pro-image-hover swipe-to-top  d-none d-lg-block d-xl-block">
                                              <img class="img-fluid" src="images/product_images/product_image_08_02.jpg" alt="Product Image"></span>
                                        </a>
                                        <div class="pro-buttons d-none d-lg-block d-xl-block">
                                              <div class="pro-icons">
                                              <a href="wishlist.html" class="icon active swipe-to-top">
                                                <i class="fas fa-heart"></i>
                                              </a>
                                              <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                                <i class="fas fa-eye"></i>
                                              </div>
                                              <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                              </div>
                                            
            
                                            <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                          </div>
                                        
                                      </div>
                                      <div class="pro-description">
                                          <span class="pro-info">
                                              Necklace                 
                                            </span>
                                            <h2 class="pro-title"><a href="product-page1.html">Romantic Love Hollow Heart Necklace for Women</a></h2>
                                            
                                            <div class="pro-price">
                                              <ins>$85</ins>
                                          </div>
                                          <div class="pro-mobile-buttons d-lg-none d-xl-none">
                                              <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                              
                  
                                        </div>
                                      </div>
                                          
                                    </article>
                                </div>
                              </div>
                        </div>
                    <!-- 2nd tab -->
                  </div>

                  <div role="tabpanel" class="tab-pane fade" id="liked">
                      <div class="tab-carousel-js row">                         
                          <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                              <div class="product">
                                  <article>
                                      <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                                          <a href="wishlist.html" class="icon active swipe-to-top">
                                            <i class="fas fa-heart"></i>
                                          </a>
                                          <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                            <i class="fas fa-eye"></i>
                                          </div>
                                          <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                          </div>
                                                                      
                                    <div class="pro-thumb "> 
                                      <div class="pro-tag bg-primary">Featured</div>
                                      <a href="product-page1.html">
                                          <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_01.jpg" alt="Product Image"></span>
                                          <span class="pro-image-hover swipe-to-top  d-none d-lg-block d-xl-block"><img class="img-fluid" src="images/product_images/product_image_01_02.jpg" alt="Product Image"></span>
                                      </a>
                                      <div class="pro-buttons d-none d-lg-block d-xl-block">
                                            <div class="pro-icons">
                                            <a href="wishlist.html" class="icon active swipe-to-top">
                                              <i class="fas fa-heart"></i>
                                            </a>
                                            <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                              <i class="fas fa-eye"></i>
                                            </div>
                                            <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                            </div>
                                          
          
                                          <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                        </div>
                                      
                                    </div>
                                    <div class="pro-description">
                                        <span class="pro-info">
                                            Ring Collection                   
                                          </span>
                                          <h2 class="pro-title"><a href="product-page1.html">Austrian Crystals Engagement Jewelry Ring</a></h2>
                                          
                                          <div class="pro-price">
                                            <ins>$285</ins>
                                        </div>
                                        <div class="pro-mobile-buttons d-lg-none d-xl-none">
                                            <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                            
                
                                      </div>
                                    </div>
                                        
                                  </article>
                                </div>
                          </div>
                          <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                              <div class="product">
                                  <article>
                                      <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                                          <a href="wishlist.html" class="icon active swipe-to-top">
                                            <i class="fas fa-heart"></i>
                                          </a>
                                          <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                            <i class="fas fa-eye"></i>
                                          </div>
                                          <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                          </div>
                                                                    
                                    <div class="pro-thumb "> 
                                      
                                      <a href="product-page1.html">
                                          <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_02.jpg" alt="Product Image"></span>
                                          <span class="pro-image-hover swipe-to-top  d-none d-lg-block d-xl-block">
                                            <img class="img-fluid" src="images/product_images/product_image_02_02.jpg" alt="Product Image"></span>
                                      </a>
                                      <div class="pro-buttons d-none d-lg-block d-xl-block">
                                            <div class="pro-icons">
                                            <a href="wishlist.html" class="icon active swipe-to-top">
                                              <i class="fas fa-heart"></i>
                                            </a>
                                            <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                              <i class="fas fa-eye"></i>
                                            </div>
                                            <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                            </div>
                                          
          
                                          <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                        </div>
                                      
                                    </div>
                                    <div class="pro-description">
                                        <span class="pro-info">
                                            Earrings                   
                                          </span>
                                          <h2 class="pro-title"><a href="product-page1.html">Crystal Water Drop Wedding Function  Earrings</a></h2>
                                          
                                          <div class="pro-price">
                                            <del>$450</del><ins>$285</ins>
                                        </div>
                                        <div class="pro-mobile-buttons d-lg-none d-xl-none">
                                            <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                            
                
                                      </div>
                                    </div>
                                        
                                  </article>
                              </div>
                            </div>
                          <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="product">
                              <article>     
                                  <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                                      <a href="wishlist.html" class="icon active swipe-to-top">
                                        <i class="fas fa-heart"></i>
                                      </a>
                                      <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                        <i class="fas fa-eye"></i>
                                      </div>
                                      <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                      </div>                              
                                <div class="pro-thumb "> 
                                    <div class="pro-tag">Sale</div>
                                  <a href="product-page1.html">
                                      <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_03.jpg" alt="Product Image"></span>
                                      <span class="pro-image-hover swipe-to-top d-none d-lg-block d-xl-block"><img class="img-fluid" src="images/product_images/product_image_03_02.jpg" alt="Product Image"></span>
                                  </a>
                                  <div class="pro-buttons d-none d-lg-block d-xl-block">
                                        <div class="pro-icons">
                                        <a href="wishlist.html" class="icon active swipe-to-top">
                                          <i class="fas fa-heart"></i>
                                        </a>
                                        <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                          <i class="fas fa-eye"></i>
                                        </div>
                                        <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                        </div>
                                      
    
                                      <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                    </div>
                                
                                </div>
                                <div class="pro-description">
                                    <span class="pro-info">
                                      Ring Collection                          
                                      </span>
                                      <h2 class="pro-title"><a href="product-page1.html">Crytal Wedding Function Rings</a></h2>
                                      
                                      <div class="pro-price">
                                        <del>$120</del><ins>$85</ins>
                                    </div>
                                    <div class="pro-mobile-buttons d-lg-none d-xl-none">
                                        <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                        
            
                                  </div>
                                </div>
                                    
                              </article>
                            </div>
                          </div>
                          <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="product">
                                <article>
                                    <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                                        <a href="wishlist.html" class="icon active swipe-to-top">
                                          <i class="fas fa-heart"></i>
                                        </a>
                                        <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                          <i class="fas fa-eye"></i>
                                        </div>
                                        <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                        </div>
                                                                  
                                  <div class="pro-thumb "> 
                                    <div class="pro-tag bg-success">NEW</div>
                                    <a href="product-page1.html">
                                        <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_04.jpg" alt="Product Image"></span>
                                        <span class="pro-image-hover swipe-to-top  d-none d-lg-block d-xl-block"><img class="img-fluid" src="images/product_images/product_image_04_02.jpg" alt="Product Image"></span>
                                    </a>
                                    <div class="pro-buttons d-none d-lg-block d-xl-block">
                                          <div class="pro-icons">
                                          <a href="wishlist.html" class="icon active swipe-to-top">
                                            <i class="fas fa-heart"></i>
                                          </a>
                                          <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                            <i class="fas fa-eye"></i>
                                          </div>
                                          <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                          </div>
                                        
        
                                        <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                      </div>
                                    
                                  </div>
                                  <div class="pro-description">
                                      <span class="pro-info">
                                          Rings                   
                                        </span>
                                        <h2 class="pro-title"><a href="product-page1.html">Women Crystal Zircon Engagement And Wedding Ring</a></h2>
                                        
                                        <div class="pro-price">
                                          <del>$120</del><ins>$110</ins>
                                      </div>
                                      <div class="pro-mobile-buttons d-lg-none d-xl-none">
                                          <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                          
              
                                    </div>
                                  </div>
                                      
                                </article>
                            </div>
                          </div>
                          
                          <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="product">
                                <article>
                                    <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                                        <a href="wishlist.html" class="icon active swipe-to-top">
                                          <i class="fas fa-heart"></i>
                                        </a>
                                        <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                          <i class="fas fa-eye"></i>
                                        </div>
                                        <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                        </div>
                                                                  
                                  <div class="pro-thumb "> 
                                    <div class="pro-tag bg-success">NEW</div>
                                    <a href="product-page1.html">
                                        <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_05.jpg" alt="Product Image"></span>
                                        <span class="pro-image-hover swipe-to-top  d-none d-lg-block d-xl-block">
                                          <img class="img-fluid" src="images/product_images/product_image_05_02.jpg" alt="Product Image"></span>
                                    </a>
                                    <div class="pro-buttons d-none d-lg-block d-xl-block">
                                          <div class="pro-icons">
                                          <a href="wishlist.html" class="icon active swipe-to-top">
                                            <i class="fas fa-heart"></i>
                                          </a>
                                          <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                            <i class="fas fa-eye"></i>
                                          </div>
                                          <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                          </div>
                                        
        
                                        <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                      </div>
                                    
                                  </div>
                                  <div class="pro-description">
                                      <span class="pro-info">
                                          Bangle                  
                                        </span>
                                        <h2 class="pro-title"><a href="product-page1.html">Viennois Rose Gold Circle Bangle for Wedding</a></h2>
                                        
                                        <div class="pro-price">
                                          <del>$220</del><ins>$185</ins>
                                      </div>
                                      <div class="pro-mobile-buttons d-lg-none d-xl-none">
                                          <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                          
              
                                    </div>
                                  </div>
                                      
                                </article>
                            </div>
                          </div>
                          <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="product">
                                <article>
                                    <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                                        <a href="wishlist.html" class="icon active swipe-to-top">
                                          <i class="fas fa-heart"></i>
                                        </a>
                                        <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                          <i class="fas fa-eye"></i>
                                        </div>
                                        <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                        </div>
                                                                  
                                  <div class="pro-thumb "> 
                                    <div class="pro-tag bg-success">NEW</div>
                                    <a href="product-page1.html">
                                        <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_06.jpg" alt="Product Image"></span>
                                        <span class="pro-image-hover swipe-to-top  d-none d-lg-block d-xl-block">
                                          <img class="img-fluid" src="images/product_images/product_image_06_02.jpg" alt="Product Image"></span>
                                    </a>
                                    <div class="pro-buttons d-none d-lg-block d-xl-block">
                                          <div class="pro-icons">
                                          <a href="wishlist.html" class="icon active swipe-to-top">
                                            <i class="fas fa-heart"></i>
                                          </a>
                                          <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                            <i class="fas fa-eye"></i>
                                          </div>
                                          <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                          </div>
                                        
        
                                        <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                      </div>
                                    
                                  </div>
                                  <div class="pro-description">
                                      <span class="pro-info">
                                          Bracelet                 
                                        </span>
                                        <h2 class="pro-title"><a href="product-page1.html">Bracelet Women Metal Chain for Engagement</a></h2>
                                        
                                        <div class="pro-price">
                                          <del>$140</del><ins>$81</ins>
                                      </div>
                                      <div class="pro-mobile-buttons d-lg-none d-xl-none">
                                          <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                          
              
                                    </div>
                                  </div>
                                      
                                </article>
                            </div>
                          </div>
                          <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                              <div class="product">
                                  <article>
                                      <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                                          <a href="wishlist.html" class="icon active swipe-to-top">
                                            <i class="fas fa-heart"></i>
                                          </a>
                                          <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                            <i class="fas fa-eye"></i>
                                          </div>
                                          <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                          </div>
                                                                    
                                    <div class="pro-thumb "> 
                                      <div class="pro-tag bg-success">NEW</div>
                                      <a href="product-page1.html">
                                          <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_07.jpg" alt="Product Image"></span>
                                          <span class="pro-image-hover swipe-to-top  d-none d-lg-block d-xl-block">
                                            <img class="img-fluid" src="images/product_images/product_image_07_02.jpg" alt="Product Image"></span>
                                      </a>
                                      <div class="pro-buttons d-none d-lg-block d-xl-block">
                                            <div class="pro-icons">
                                            <a href="wishlist.html" class="icon active swipe-to-top">
                                              <i class="fas fa-heart"></i>
                                            </a>
                                            <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                              <i class="fas fa-eye"></i>
                                            </div>
                                            <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                            </div>
                                          
          
                                          <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                        </div>
                                      
                                    </div>
                                    <div class="pro-description">
                                        <span class="pro-info">
                                            Crown                 
                                          </span>
                                          <h2 class="pro-title"><a href="product-page1.html">Gold Rhinestone Pageant Crown for Women</a></h2>
                                          
                                          <div class="pro-price">
                                            <del>$120</del><ins>$85</ins>
                                        </div>
                                        <div class="pro-mobile-buttons d-lg-none d-xl-none">
                                            <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                            
                
                                      </div>
                                    </div>
                                        
                                  </article>
                              </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="product">
                                    <article>
                                        <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                                            <a href="wishlist.html" class="icon active swipe-to-top">
                                              <i class="fas fa-heart"></i>
                                            </a>
                                            <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                              <i class="fas fa-eye"></i>
                                            </div>
                                            <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                            </div>
                                                                      
                                      <div class="pro-thumb "> 
                                        <div class="pro-tag bg-success">NEW</div>
                                        <a href="product-page1.html">
                                            <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_08.jpg" alt="Product Image"></span>
                                            <span class="pro-image-hover swipe-to-top  d-none d-lg-block d-xl-block">
                                              <img class="img-fluid" src="images/product_images/product_image_08_02.jpg" alt="Product Image"></span>
                                        </a>
                                        <div class="pro-buttons d-none d-lg-block d-xl-block">
                                              <div class="pro-icons">
                                              <a href="wishlist.html" class="icon active swipe-to-top">
                                                <i class="fas fa-heart"></i>
                                              </a>
                                              <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                                                <i class="fas fa-eye"></i>
                                              </div>
                                              <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                                              </div>
                                            
            
                                            <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                          </div>
                                        
                                      </div>
                                      <div class="pro-description">
                                          <span class="pro-info">
                                              Necklace                 
                                            </span>
                                            <h2 class="pro-title"><a href="product-page1.html">Romantic Love Hollow Heart Necklace for Women</a></h2>
                                            
                                            <div class="pro-price">
                                              <ins>$85</ins>
                                          </div>
                                          <div class="pro-mobile-buttons d-lg-none d-xl-none">
                                              <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                                              
                  
                                        </div>
                                      </div>
                                          
                                    </article>
                                </div>
                              </div>
                        </div>
                    <!-- 3rd tab -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

        <!-- Special Offer -->
    <section class="pro-content pro-sp-content">
        <div class="container"> 
          <div class="products-area ">
            
            
            <div class="row align-items-center">                         
              <div class="col-12 col-lg-7 order-2 order-lg-1">
                <div class="pro-description">
                    <span class="pro-info">
                      Super deal of the Month                      
                    </span>
                    <h2 class="pro-title"><a href="product-page1.html">Women Zircon Crystal Engagement Ring Now</a></h2>
                    
                      <div class="pro-price">
                          <del>$180</del>
                          <ins>$250</ins>
                      </div>
                      <div class="countdown pro-timer pro-single-timer">
                          <span class="days" ></span>
                          <span class="hours" ></span>
                          <span class="mintues" ></span>
                          <span class="seconds" ></span>
                      </div>
                    <div class="pro-btns">
                        
                        <button type="button" class="btn btn-secondary swipe-to-top" onclick="notificationCart();">Grab Now</button>
                        
                        
                    </div>
              </div>
              </div>
              <div class="col-12 col-lg-5 order-1 order-lg-2">
                <div class="pro-thumb "> 
                  
                  <a href="product-page1.html">
                      <span class="pro-image"><img class="img-fluid" src="images/product_images/deals_of_month.png" alt="Product Image"></span>
                  </a>
                  
                </div>
              </div>
              
             
            </div>
          </div>
        </div>
      </section>

      
      <!-- Popular Products -->
     <section class="pro-content pro-pl-content">
        <div class="container"> 
          <div class="products-area">
            <div class="row justify-content-center">
              <div class="col-12 col-lg-12">
                <div class="pro-heading-title">
                  <h2> New Arrivals 
                  </h2>
                  <p>Vitae posuere urna blandit sed. Praesent ut dignissim risus. </p></div>
                </div>
            </div>
            <div class="popular-carousel-js row">                         
              <div class="col-6 col-md-6 col-lg-6">
              <div class="popular-product">
                <article>   
                                              
                  <div class="pro-thumb"> 
                      <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                          <a href="wishlist.html" class="icon active swipe-to-top">
                            <i class="fas fa-heart"></i>
                          </a>
                          <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                            <i class="fas fa-eye"></i>
                          </div>
                          <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                          </div>                     
                    <a href="product-page1.html">
                        <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_05.jpg" alt="Product Image"></span>
                        <span class="pro-image-hover swipe-to-top"><img class="img-fluid" src="images/product_images/product_image_05_02.jpg" alt="Product Image"></span>
                    </a>
                    <div class="pro-buttons d-none d-lg-block d-xl-block">
                          <div class="pro-icons">
                          <a href="wishlist.html" class="icon active swipe-to-top">
                            <i class="fas fa-heart"></i>
                          </a>
                          <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                            <i class="fas fa-eye"></i>
                          </div>
                          <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                          </div>
                      </div>
                    <div class="pro-tag pro-success">New</div>
                  </div>
                  <div class="pro-description">
                    <div>
                      <span class="pro-info">
                          Bangles                     
                        </span>
                        <h2 class="pro-title"><a href="product-page1.html">Viennois Rose Gold Circle Bangle</a></h2>
                        
                        <div class="pro-price">
                          <del>$220</del><ins>$185</ins>
                      </div>
                      <div class="pro-options">
                        <div class="color-selection">
                            <h4><b>Color&nbsp;:</b>&nbsp;Silver</h4>
                          <ul>
                            <li class="active"><a class="green " href="javascript:void(0);"></a></li>
                            <li ><a class="red " href="javascript:void(0);"></a></li>
                            
                          </ul>
                          </div>
                          
                    </div>
                      <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                  </div>
                  </div>
                      
                </article>
              </div>
              </div>
              <div class="col-6 col-md-4 col-lg-6">
                <div class="popular-product">
                  <article>            
                                     
                    <div class="pro-thumb "> 
                        <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                            <a href="wishlist.html" class="icon active swipe-to-top">
                              <i class="fas fa-heart"></i>
                            </a>
                            <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                              <i class="fas fa-eye"></i>
                            </div>
                            <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                            </div>       
                      <a href="product-page1.html">
                          <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_06.jpg" alt="Product Image"></span>
                          <span class="pro-image-hover swipe-to-top"><img class="img-fluid" src="images/product_images/product_image_06_02.jpg" alt="Product Image"></span>
                      </a>
                      <div class="pro-buttons d-none d-lg-block d-xl-block">
                            <div class="pro-icons">
                            <a href="wishlist.html" class="icon active swipe-to-top">
                              <i class="fas fa-heart"></i>
                            </a>
                            <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                              <i class="fas fa-eye"></i>
                            </div>
                            <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                            </div>
                        </div>
                      <div class="pro-tag pro-success">New</div>
                    </div>
                    <div class="pro-description">
                        <div>
                          <span class="pro-info">
                              Bracelet                     
                            </span>
                            <h2 class="pro-title"><a href="product-page1.html">Bracelet for Women Metal Chain</a></h2>
                            
                            <div class="pro-price">
                              <del>$140</del><ins>$81</ins>
                          </div>
                          <div class="pro-options">
                            
                              <div class="size-selection">
                                <h4><b>Size&nbsp;:</b>&nbsp;28</h4>
                                  <ul>
                                    <li class="active"><a href="javascript:void(0);">28</a></li>
                                    <li ><a href="javascript:void(0);">32</a></li>
                                    <li ><a href="javascript:void(0);">36</a></li>
                                  </ul>
                                  </div>
                        </div>
                          <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                      </div>
                      </div>
                  </article>
                </div>
              </div>
              <div class="col-6 col-md-4 col-lg-6">
                <div class="popular-product">
                  <article> 
                                                  
                    <div class="pro-thumb "> 
                        <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                            <a href="wishlist.html" class="icon active swipe-to-top">
                              <i class="fas fa-heart"></i>
                            </a>
                            <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                              <i class="fas fa-eye"></i>
                            </div>
                            <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                            </div>   
                      <a href="product-page1.html">
                          <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_07.jpg" alt="Product Image"></span>
                          <span class="pro-image-hover swipe-to-top"><img class="img-fluid" src="images/product_images/product_image_07_02.jpg" alt="Product Image"></span>
                      </a>
                      <div class="pro-buttons d-none d-lg-block d-xl-block">
                            <div class="pro-icons">
                            <a href="wishlist.html" class="icon active swipe-to-top">
                              <i class="fas fa-heart"></i>
                            </a>
                            <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                              <i class="fas fa-eye"></i>
                            </div>
                            <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                            </div>
                          
                        </div>
                      <div class="pro-tag pro-success">NEW</div>
                    </div>
                    <div class="pro-description">
                        <div>
                          <span class="pro-info">
                              Crown                      
                            </span>
                            <h2 class="pro-title"><a href="product-page1.html">Gold Rhinestone Pageant Crown</a></h2>
                            
                            <div class="pro-price">
                              <del>$120</del><ins>$85</ins>
                          </div>
                          <div class="pro-options">
                              <div class="color-selection">
                                  <h4><b>Color&nbsp;:</b>&nbsp;Silver</h4>
                                <ul>
                                  <li class="active"><a class="green " href="javascript:void(0);"></a></li>
                                  <li ><a class="red " href="javascript:void(0);"></a></li>
                                  
                                </ul>
                                </div>
                                
                          </div>
                          
                          <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                      </div>
                    </div>           
                  </article>
                </div>
              </div>
              <div class="col-6 col-md-4 col-lg-6">
                <div class="popular-product">
                  <article> 
                                               
                    <div class="pro-thumb "> 
                        <div class="pro-icons mobile-pro-icons d-lg-none d-xl-none">
                            <a href="wishlist.html" class="icon active swipe-to-top">
                              <i class="fas fa-heart"></i>
                            </a>
                            <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                              <i class="fas fa-eye"></i>
                            </div>
                            <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                            </div>      
                      <a href="product-page1.html">
                          <span class="pro-image"><img class="img-fluid" src="images/product_images/product_image_08.jpg" alt="Product Image"></span>
                          <span class="pro-image-hover swipe-to-top"><img class="img-fluid" src="images/product_images/product_image_08_02.jpg" alt="Product Image"></span>
                      </a>
                      <div class="pro-buttons d-none d-lg-block d-xl-block">
                            <div class="pro-icons">
                            <a href="wishlist.html" class="icon active swipe-to-top">
                              <i class="fas fa-heart"></i>
                            </a>
                            <div class="icon swipe-to-top" data-toggle="modal" data-target="#quickViewModal">
                              <i class="fas fa-eye"></i>
                            </div>
                            <a href="compare.html" class="icon swipe-to-top"><i class="fas fa-align-right" data-fa-transform="rotate-90"></i></a>
                            </div>
                          
                        </div>
                      <div class="pro-tag pro-success">NEW</div>
                    </div>
                    <div class="pro-description">
                        <div>
                          <span class="pro-info">
                              Necklace                      
                            </span>
                            <h2 class="pro-title"><a href="product-page1.html">Romantic Love Hollow Heart Necklace for Women</a></h2>
                            
                            <div class="pro-price">
                              <ins>$85</ins>
                          </div>
                          <div class="pro-options">
                            
                              <div class="size-selection">
                                <h4><b>Size&nbsp;:</b>&nbsp;28</h4>
                                  <ul>
                                    <li ><a href="javascript:void(0);">28</a></li>
                                    <li ><a href="javascript:void(0);">32</a></li>
                                    <li class="active"><a href="javascript:void(0);">36</a></li>
                                  </ul>
                                  </div>
                        </div>
                          <button type="button" class="btn btn-secondary btn-block swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                      </div>
                    </div>
                  </article>
                </div>
              </div>
            </div>
          </div>

        </div>
      </section>  
      

      <section class="pro-content pro-full-screen-banner">
          <img class="img-fluid" src="images/banners/full_screen.jpg" alt="full screen banner">
          <div class="pro-full-screen-overlay">
                <div class="pro-full-screen">
                    <div class="pro-heading-title">
                        <h2> The Perfect Match 
                        </h2>
                        <p>Vitae posuere urna blandit sed. Praesent ut dignissim risus.
                          
                        </p>
                      </div>
                      <button class="btn btn-secondary swipe-to-top">
                        Shop Now
                      </button>
                </div>  
              </div>
      </section>

      <!-- Blogs -->
      <section class="pro-content blog-content">
        
        <div class="container">
          <!-- heading -->
          
          <div class="row justify-content-center">
            <div class="col-12 col-lg-12">
              <div class="pro-heading-title">
                <h2> Latest Blog 
                </h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></div>
              </div>
          </div>
          <div class="blog-carousel-js row">                         
              <div class="col-6 col-md-3 pro-blog">
                  <div class="pro-thumb">
                  
                      <div class="blog-date">
                          30
                          <small>Dec</small>
                      </div>
                      
                      <img class="img-fluid swipe-to-top" src="images/blogs/blog_post_1.jpg" alt="Image">
                      
                    </div>
                    <div class="pro-detail">
                        <h2><a href="blog-page1.html" >Woman wearing Silver-colore ring necklaces</a></h2>
                      
                      <p>Lorem ipsum dolor sit amet, eiusmod tempor
                          incdidunt ut labore et aliqua [....]
                        </p>
                        <a href="blog-page1.html" class="pro-readmore" tabindex="0">Read more</a>
                    </div>
              </div>
              <div class="col-6 col-md-3 pro-blog">
                  <div class="pro-thumb">
                  
                      <div class="blog-date">
                          18
                          <small>aug</small>
                      </div>
                      
                      <img class="img-fluid" src="images/blogs/blog_post_2.jpg" alt="Image">
                      
                    </div>
                    <div class="pro-detail">
                        <h2><a href="blog-page1.html" >White gold engagement rings for couples</a></h2>
                      
                      <p>Lorem ipsum dolor sit amet, tempor
                          incdidunt ut labore et dolore [....]
                        </p>
                        <a href="blog-page1.html" class="pro-readmore" tabindex="0">Read more</a>
                    </div>
              </div>
              <div class="col-6 col-md-3 pro-blog">
                  <div class="pro-thumb">
                  
                      <div class="blog-date">
                          01
                          <small>Sep</small>
                      </div>
                      
                      <img class="img-fluid" src="images/blogs/blog_post_3.jpg" alt="Image">
                      
                    </div>
                    <div class="pro-detail">
                        <h2><a href="blog-page1.html" >Shallow focus photo of gold-colored ring</a></h2>
                      
                      <p>Lorem ipsum dolor sit amet, eiusmod tempor
                          incdidunt ut dolore magna aliqua [....]
                        </p>
                        <a href="blog-page1.html" class="pro-readmore" tabindex="0">Read more</a>
                    </div>
              </div>
              <div class="col-6 col-md-3 pro-blog">
                  <div class="pro-thumb">
                  
                      <div class="blog-date">
                          22
                          <small>Oct</small>
                      </div>
                      
                      <img class="img-fluid" src="images/blogs/blog_post_4.jpg" alt="Image"> 
                      
                    </div>
                    <div class="pro-detail">
                        <h2><a href="blog-page1.html" >Pearl necklace jewelry treasure box</a></h2>
                      
                      <p>Lorem ipsum dolor sit amet, eiusmod tempor
                          labore et dolore magna aliqua [....]
                        </p>
                        <a href="blog-page1.html" class="pro-readmore" tabindex="0">Read more</a>
                    </div>
              </div>
              <div class="col-6 col-md-3 pro-blog">
                  <div class="pro-thumb">
                  
                      <div class="blog-date">
                          22
                          <small>Jan</small>
                      </div>
                      
                      <img class="img-fluid" src="images/blogs/blog_post_5.jpg" alt="Image">
                      
                    </div>
                    <div class="pro-detail">
                        <h2><a href="blog-page1.html" >Woman wearing silver colore jewelery</a></h2>
                      
                      <p>Lorem ipsum, sed do eiusmod tempor
                          incdidunt ut labore et dolore [....]
                        </p>
                        <a href="blog-page1.html" class="pro-readmore" tabindex="0">Read more</a>
                    </div>
              </div>
              <div class="col-6 col-md-3 pro-blog">
                  <div class="pro-thumb">
                  
                      <div class="blog-date">
                          12
                          <small>Mar</small>
                      </div>
                      
                      <img class="img-fluid" src="images/blogs/blog_post_6.jpg" alt="Image">
                      
                    </div>
                    <div class="pro-detail">
                      <h2><a href="blog-page1.html" >Gold bangle is top of the engagement band</a></h2>
                      
                      <p>Lorem ipsum dolor sit amet, sed do eiusmod tempor
                          incdidunt dolore magna aliqua [....]
                        </p>
                        <a href="blog-page1.html" class="pro-readmore" tabindex="0">Read more</a>
                    </div>
              </div>
            
          </div>
        </div>
      </section>

      <!-- Product Modal -->
      <div class="modal fade" id="quickViewModal" tabindex="-1" role="dialog" aria-hidden="true">
      
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    
                    <div class="container">
                      <div class="row align-items-center">
                        <div class="col-12 col-md-6">
                            <div class="row ">
                                <div id="quickViewCarousel" class="carousel slide" data-ride="carousel">
                                    <!-- The slideshow -->
                                    <div class="carousel-inner">
                                      <div class="carousel-item active">
                                          
                                        <img class="img-fluid" src="images/gallery/preview/Product_image_01.jpg" alt="image">
                                      </div>
                                      <div class="carousel-item">
                                          
                                        <img class="img-fluid" src="images/gallery/preview/Product_image_02.jpg" alt="image">
                                      </div>
                                      <div class="carousel-item">
                                          
                                        <img class="img-fluid" src="images/gallery/preview/Product_image_03.jpg" alt="image">
                                      </div>
                                      <div class="carousel-item">
                                          
                                        <img class="img-fluid" src="images/gallery/preview/Product_image_04.jpg" alt="image">
                                      </div>
                                    </div>
                                    <!-- Left and right controls -->
                                    <a class="carousel-control-prev" href="#quickViewCarousel" data-slide="prev">
                                      <span class="fas fa-angle-left "></span>
                                    </a>
                                    <a class="carousel-control-next" href="#quickViewCarousel" data-slide="next">
                                      <span class="fas fa-angle-right "></span>
                                    </a>
                                  
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="pro-description">
                                <h2 class="pro-title">Stylish Necklace Women Heart</h2>
                            
                                <div class="pro-price">
                                    <del>$120</del><ins>$110</ins>
                                </div>
  
                                <div class="pro-infos">
                                    <div class="pro-single-info"><b>Product ID :</b>1004</div>
                                    <div class="pro-single-info"><b>Categroy :</b><a href="#">Rings</a></div>
                                    <div class="pro-single-info">
                                      <b>Tags :</b>
                                      <ul>
                                          <li><a href="#">bracelets</a></li>
                                          <li><a href="#">diamond</a></li>
                                          <li><a href="#">ring</a></li>
                                      </ul>
                                    </div>
                                    <div class="pro-single-info"><b>Available :</b><span class="text-secondary">InStock</span></div>
                                </div>
                                
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor. 
                                </p>
                                <div class="pro-counter">
                                    <div class="input-group item-quantity">
                                          
                                        <input type="text" id="quantity1" name="quantity" class="form-control " value="10">
                                        
                                        <span class="input-group-btn">
                                            <button type="button" value="quantity1" class="quantity-plus btn" data-type="plus" data-field="">
                                                <i class="fas fa-plus"></i>
                                            </button>
                                        
                                            <button type="button" value="quantity1" class="quantity-minus btn" data-type="minus" data-field="">
                                                <i class="fas fa-minus"></i>
                                            </button>
                                        </span>
                                      </div>
                                      <button type="button" class="btn btn-secondary btn-lg swipe-to-top" onclick="notificationCart();">Add to Cart</button>
                              
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
            </div>
          </div>
        </div>







<?php /*<div class="container-fluid">
    <div class="row">
        <div class="col-12 slider">
<!--
            <span class="actBtns pull-right"> 
                <a href ng-click="openModal('-1', 'Views/Polls/Modals/addModal.php', 'PollsController')">
                    <i class="fa fa-plus">Add Poll</i>
                </a> 
                <a href ng-click="openModal('-1', 'Views/Users/Modals/loginModal.php', 'UsersController')">
                    <i class="fa fa-plus">Login</i>
                </a>
                
            </span>
-->
        </div>
    </div>
</div>
<section class="container">
    
    <button ng-click="getList(1, 'polls', 'latestPolls', false, 'polls.php', 12)" id="updt_polls" class="hideIt" ></button>
    
    <div class="row gridContainer" masonry>
        <div ng-repeat="pollItm in list.latestPolls" class="item col-sm-6 col-md-4 col-lg-3">
            <figure>
                <a href ="/{{app_folder}}{{currLang}}/poll/{{pollItm.slug}}">
                    <div class="pollImg img-thumbnail" ng-init="r = set_rand(5)">
                        <img src="./Library/img/polls_photos/thumb/{{pollItm.seo_image || 'think'+r+'.svg'}}">
                    </div>
                    <h3>{{pollItm.poll_title}}</h3>
                </a>
<!--
                <ul class="pollOptions">
                    <li ng-repeat="optItm in pollItm['options']">
                        <div class="optionImg">
                            <img src="./Library/img/options_photos/thumb/{{optItm.option_photo || 'option.svg'}}" alt="{{optItm.option_text}}" >
                        </div>
                        <div> {{optItm.option_text}} </div>
                    </li>
                </ul>
-->
            </figure>
        </div>
    </div>
</section>*/?>