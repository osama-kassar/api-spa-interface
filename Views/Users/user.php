
<section class="container settings_page " ng-init="
            t_datepicker=false;">
    <div class="row">
        <div class="col-sm-9">
            <h2 class="row">{{'email_notification'|translate}} </h2>
            <section class="row">
                <div class="col-sm-4">{{'digest_email'|translate}}</div>
                <div class="col-sm-4">
                    <label class="switch">
                      <input type="checkbox" id="digest_email">
                      <span class="slider round"></span>
                    </label> <label for="digest_email">{{'activated'|translate}}</label>
                </div>
                <div class="col-sm-4">
                    <select class="form-control">
                        <option value="WEEKLY">{{'weekly'|translate}}</option>
                        <option value="MONTHLY">{{'monthly'|translate}}</option>
                        <option value="QUARTELY">{{'quartrly'|translate}}</option>
                    </select>
                </div>
                
                <div class="col-sm-4">{{'new_reviews'|translate}}</div>
                <div class="col-sm-4">
                    <label class="switch">
                      <input type="checkbox" id="new_reviews">
                      <span class="slider round"></span>
                    </label> <label for="new_reviews">{{'activated'|translate}}</label>
                </div>
                <div class="col-sm-4"></div>
                
                <div class="col-12">{{'setteing_note'|translate}}</div>
                
                <div class="col-sm-4">{{'pending_approval'|translate}}</div>
                <div class="col-sm-4">
                    <label class="switch">
                      <input type="checkbox" id="digest_email">
                      <span class="slider round"></span>
                    </label> <label for="digest_email">{{'activated'|translate}}</label>
                </div>
                <div class="col-sm-4">
                    <select class="form-control">
                        <option value="ALWAYS">{{'immediate'|translate}}</option>
                        <option value="DAILY">{{'daily'|translate}}</option>
                    </select>
                </div>
            </section>
            
            
            <h2 class="row">{{'sessions'|translate}} </h2>
            <section class="row">
                <div class="col-12">
                    {{'other_sessions'|translate}} <b>{{sessions}}</b>
                </div>
                <div class="col-12">
                    <button class="btn btn-info"> {{'logout_all_sessions'|translate}}</button>
                </div>
            </section>
        </div>
    </div>
</section>