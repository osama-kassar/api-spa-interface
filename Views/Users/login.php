<div class="container-fluid login" ng-init="chkLogin()">
    <form class="form-signin" ng-submit="doLogin()">
        <div class="text-center">
            <img class="logo" src="Library/img/admea-logo.png" alt="" >
        </div>

        <div class="form-group">
            <label for="email">{{'email'|translate}}</label>
            <input type="email" class="form-control" placeholder="{{'email'|translate}}" ng-model="userData.email">
        </div>
        <div class="form-group">
            <label for="password">{{'password'|translate}}</label>
            <input type="password" class="form-control" placeholder="{{'password'|translate}}" ng-model="userData.password">
        </div>
        <div class="form-group ">
            <label class="mycheckbox">
                <input type="checkbox" ng-model="userData.remember_me">
                <span></span> {{'remember_me'|translate}}
            </label>
        </div>
        <div class="">
            <button class="btn btn-primary " type="submit">{{'signin'|translate}}</button>
        </div>
        <div class="form-group">
            <a href="#">{{'forget_password'|translate}}</a>
        </div>
    </form>
</div>