
<section class="container settings_page ">
    <div class="row justify-content-sm-center">
        <div class="col-sm-9">
            <h2 class="row">{{'email_notification'|translate}} </h2>
            <section class="row"> 
                <div class="col-sm-4">{{'digest_email'|translate}}</div>
                <div class="col-sm-4 mb-3"  style="line-height: 40px;">
                    <label class="switch">
                      <input type="checkbox" id="digest_email" 
                             ng-model="settings.emailSettings[0].val"
                             ng-change="settings.emailSettings[0].frequency = !settings.emailSettings[0].val ? 'NEVER' : 'WEEKLY'">
                      <span class="slider round"></span>
                    </label> <label for="digest_email">{{'activated'|translate}}</label>
                </div>
                <div class="col-sm-4" >
                    <select class="form-control" 
                            ng-if="settings.emailSettings[0].val"
                            ng-model="settings.emailSettings[0].frequency">
                        <option value="WEEKLY">{{'weekly'|translate}}</option>
                        <option value="MONTHLY">{{'monthly'|translate}}</option>
                        <option value="QUARTERLY">{{'quarterly'|translate}}</option>
                    </select>
                </div>
                
                <div class="col-sm-4">{{'new_reviews'|translate}}</div>
                <div class="col-sm-4 mb-3" style="line-height: 40px;">
                    <label class="switch">
                      <input type="checkbox" id="new_reviews"
                              ng-model="settings.emailSettings[4].val"
                             ng-change="settings.emailSettings[4].frequency = !settings.emailSettings[4].val ? 'NEVER' : 'ALWAYS'">
                      <span class="slider round"></span>
                    </label> <label for="new_reviews">{{'activated'|translate}}</label>
                </div>
                <div class="col-sm-4"></div>
                
                <div class="col-12 mb-5"><p>{{'setteing_note'|translate}}</p></div>
                
                <div class="col-sm-4">{{'pending_approval'|translate}}</div>
                <div class="col-sm-4 mb-3" style="line-height: 40px;">
                    <label class="switch ">
                      <input type="checkbox" id="pending_approval"
                            ng-model="settings.emailSettings[2].val"
                             ng-change="settings.emailSettings[2].frequency = !settings.emailSettings[2].val ? 'NEVER' : 'ALWAYS'">
                      <span class="slider round"></span>
                    </label> <label for="pending_approval">{{'activated'|translate}}</label>
                </div>
                <div class="col-sm-4">
                    <select class="form-control"
                             ng-if="settings.emailSettings[2].val"
                              ng-model="settings.emailSettings[2].frequency">
                        <option value="ALWAYS">{{'immediate'|translate}}</option>
                        <option value="DAILY">{{'daily'|translate}}</option>
                    </select>
                </div>
            </section>
            
            
            <h2 class="row">{{'sessions'|translate}} </h2>
            <section class="row">
                <div class="col-12">
                    {{'other_sessions'|translate}} (<b>{{settings.allSessions}}</b>)
                </div>
                <div class="col-12">
                    <button class="btn btn-info"
                            ng-click="deleteSessions()"> {{'logout_all_sessions'|translate}}</button>
                </div>
            </section>

            <div class="row">
                <button class="btn btn-primary" ng-click="saveSettings()">{{'save'|translate}}</button>
            </div>
        </div>
    </div>
</section>