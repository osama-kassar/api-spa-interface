<div class="container-fluid login" ng-controller="UsersController as UsersController">
    <div class="text-center">
        <img class="logo" src="Library/img/admea-logo.png" alt="" >
    </div>
    <div ng-init="doLogout()" class="text-center" >
        {{'logout_msg' | translate}}
    </div>
</div>