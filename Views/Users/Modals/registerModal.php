<div class="modal-header">
    <h3 class="modal-title" id="modal-title">{{'create_account'|translate}}</h3>
</div>
<div class="modal-body" id="modal-body">


    <div ng-controller="UsersController as UsersController">
        <div ng-init="getRec( param )" class="login_card ">
            <form class="row">
<!--                <p class="col-12">{{'register_msg'|translate}}</p>-->
                <label class="col-12 myInput"><div class="nfoHolder"></div> {{'fullname'|translate}}
                    <input class="form-control" type="text" auto-fill chk="isEmpty" ng-model="rec.users.fullname" />
                </label>
                <label class="col-12 myInput"><div class="nfoHolder"></div> {{'username'|translate}}
                    <input class="form-control" auto-fill chk="isEmail" type="text" ng-model="rec.users.user_name" />
                </label>
                <label class="col-12 myInput"><div class="nfoHolder"></div> {{'password'|translate}}
                    <input class="form-control" auto-fill chk="isPassword" type="password" ng-model="rec.users.user_pass" />
                    <i class="fa fa-eye-slash sideIcon" show-password></i>
                </label>
                <div class="col-12">
                    <button class="btn btn-info" ng-click="doRegister(  );">{{'create_account'|translate}}</button>
                </div>
                
            <div class="socialmedia_login col-12">
                <div class="middle-line"> {{'or_fast_access'|translate}} </div>
                <button class="btn btn-facebook">
                    {{'login_with'|translate}}
                     Facebook <i class="fa fa-facebook"></i>
                </button>
                <button class="btn btn-twitter">
                    {{'login_with'|translate}}
                     Twitter <i class="fa fa-twitter"></i>
                </button>
                <button class="btn btn-google-plus">
                    {{'login_with'|translate}}
                     Google Plus <i class="fa fa-google-plus"></i>
                </button>
                <button class="btn btn-linkedin">
                    {{'login_with'|translate}}
                     Linkedin <i class="fa fa-linkedin"></i>
                </button>
            </div>
            </form>
            
        </div>
    </div>
    
    
</div>
<div class="modal-footer">
    <!--    <button class="btn btn-info" type="button" ng-click="ok()">{{'ok'|translate}}</button>-->
<!--    <button class="btn btn-info" type="button" ng-click="cancel()">{{'cancel'|translate}}</button>-->
    
<!--
    <button class="btn btn-info" ng-click="doRegister(  );">{{'create_account'|translate}}</button>
    <button class="btn btn-info" ng-click="getModal();">{{'register'|translate}}</button>
-->
    <a href="#">{{'login'|translate}}</a>
</div>