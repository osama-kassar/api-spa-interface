<form class=" form_style1" ng-submit="do(modalTab)" name="loginform">
    <div class="modal-header">    
        <h3 class="modal-title" id="modal-title">{{'poll2know'|translate}}</h3>
        <button type="button" class="close" ng-click="cancel()">&times;</button>
    </div>
    
    <div class="modal-body" id="modal-body" ng-if="user.userInfo == null">
        <div class="login_card">
            <div class="switchBtn">
                <a href ng-click="set_tab('login')" id="login" class="active">
                    {{'login'|translate}}
                </a>
                <a href ng-click="set_tab('register')" id="register" >
                    {{'register'|translate}}
                </a>
            </div>
            
            
            
            
<!-- LOGIN -->
            <div class="col-12 myInput animationIf" ng-if="modalTab=='login'">
                <label><div class="nfoHolder"></div> {{'username'|translate}}
                    <input class="form-control" chk="isEmail" type="text" ng-model="loginInfo.user_name" auto-fill />
                </label>
                <label><div class="nfoHolder"></div> {{'password'|translate}}
                    <input class="form-control" chk="isPassword" type="password" ng-model="loginInfo.user_pass"  />
                </label>
                <label class="mycheckbox">
                    <input type="checkbox" ng-model="loginInfo.remember_me" 
                           ng-false-value="'0'" 
                           ng-true-value="'1'" >
                    <ii></ii>{{'remember_me'|translate}}
                </label>
                <button class="btn btn-info" ng-disabled="loginform.$invalid">{{'login'|translate}}</button>
            </div>
            
<!-- FORGET PASSWORD -->
            <div class="col-12 myInput animationIf" ng-if="modalTab=='forget_password'">
                <label><div class="nfoHolder"></div> {{'email'|translate}}
                    <input class="form-control" chk="isEmail" type="text" ng-model="forget_password.user_name" auto-fill />
                </label>
                <button class="btn btn-info" ng-disabled="loginform.$invalid">{{'send'|translate}}</button>
            </div>
            
            
            
<!-- RIGESTERATION -->
            <div class="col-12 myInput animationIf" ng-if="modalTab=='register'">            
                <label><div class="nfoHolder"></div> {{'fullname'|translate}}
                    <input class="form-control" type="text" auto-fill chk="isEmpty" ng-model="rec.users.user_fullname" />
                </label>
                <label><div class="nfoHolder"></div> {{'username'|translate}}
                    <input class="form-control" auto-fill chk="isEmail" is-unique="{{rec.users.user_name}}" type="text" ng-model="rec.users.user_name" />
                </label>
                <label><div class="nfoHolder"></div> {{'password'|translate}}
                    <input class="form-control" auto-fill chk="isPassword" type="password" ng-model="rec.users.user_pass" />
                    <i class="fa fa-eye-slash sideIcon" show-password></i>
                </label>
                <div>
                    <button class="btn btn-info" ng-click="doRegister(  );">{{'create_account'|translate}}</button>
                </div>
            </div>
            
            
            
<!-- LOGIN WITH SOCIAL MEDIA -->
<!--
            <div class="socialmedia_login col-12">
                <div class="middle-line"> {{'or_fast_access'|translate}} </div>
                <button type="button" class="btn btn-facebook" ng-click="authenticate('facebook')">
                    {{'login_with'|translate}}
                     Facebook <i class="fa fa-facebook"></i>
                </button>
                <button type="button" class="btn btn-twitter" ng-click="authenticate('twitter')">
                    {{'login_with'|translate}}
                     Twitter <i class="fa fa-twitter"></i>
                </button>
                <button class="btn btn-google-plus" ng-click="authenticate('google')">
                    {{'login_with'|translate}}
                     Google Plus <i class="fa fa-google-plus"></i>
                </button>
                <button class="btn btn-linkedin" ng-click="authenticate('linkedin')">
                    {{'login_with'|translate}}
                     Linkedin <i class="fa fa-linkedin"></i>
                </button>
            </div>
-->
        </div>
    </div>
    
    
<!-- SHOW MESSAGE IF ALREADY LOGGIDIN -->
    <div ng-if="user.userInfo !== null" class="modal-body">
        <h1>{{'you-loggedin-already'|translate}}</h1>
    </div>
    
    
    <div class="modal-footer">
        <div  ng-if="user.userInfo == null">
            <a href ng-click="set_tab('forget_password')">{{'forget_password'|translate}}</a>
            
        </div>
        
        <div  ng-if="user.userInfo !== null">
            <button class="btn btn-info" type="button" ng-click="ok()">{{'ok'|translate}}</button>
        </div>
    </div>
</form>