<div class="squarelist_style">
    <div>
        <h4>{{'news_list'|translate}}</h4>         
            <!--            NEW PHOTOS -->
        <div  class="row">
            
            <div class=" col-12">
<!--
                <a class="btn btn-success"  href="{{currLang}}/contents"
                        tooltip="" title="{{'add_edit_news'|translate}}">
                    <i class="fa fa-plus"></i>
                </a>
-->
                <a class="btn btn-success"  href ng-click="syncNews()"
                        tooltip="" title="{{'sync_news'|translate}}" id="sec_4_btn">
                    <span><i class="fa fa-exchange"></i></span>
                </a>
            </div>
            <div class="col-sm-3 tmpltDiv" ng-repeat="newsItem in landingpage.news">
                <h3>{{newsItem.news_title}}</h3>
                <div>
                    <img src="{{'./Library/img/lp_photos/thumb/'+newsItem.news_photos[0]||'./Library/img/150.png'}}" alt="">
                </div>
                <p>{{newsItem.news_desc}}</p>
                <span>
                    <a class="btn btn-info" tooltip title="{{'edit_news'|translate}}"
                       href="{{currLang}}/contents">
                        <i class="fa fa-pencil"></i>
                    </a>
                </span>
            </div>
        </div>
    </div>
</div>