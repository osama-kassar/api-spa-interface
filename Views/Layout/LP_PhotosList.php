<div class="squarelist_style">
    <div>
        <h4>{{'photos_list'|translate}}</h4>         
            <!--            NEW PHOTOS -->
        <div  class="row">
            
            <div class=" col-12">
<!--
                <a class="btn btn-success"  href="{{currLang}}/profiles/media"
                        tooltip="" title="{{'add_photos'|translate}}">
                    <i class="fa fa-plus"></i>
                </a>
-->
                <a class="btn btn-success"  href ng-click="syncPhotos(landingpage.photos)"
                        tooltip="" title="{{'sync_photos'|translate}}" id="sec_5_btn">
                    <span><i class="fa fa-exchange"></i></span>
                </a>
            </div>
            <div class="col-sm-3 tmpltDiv" style="height: auto;" 
                 ng-repeat="photoItem in landingpage.photos.content_object.photos">
                <div>
                    <img src="{{'./Library/img/lp_photos/thumb/'+photoItem||'./Library/img/150.png'}}" alt="">
                </div>
                <span>
                    <a class="btn btn-info" tooltip title="{{'edit_photo'|translate}}"
                       href="{{currLang}}/profiles/media">
                        <i class="fa fa-pencil"></i>
                    </a>
                </span>
            </div>
        </div>
    </div>
</div>