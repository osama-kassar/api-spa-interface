<div id="accordion" ng-init="tab1_opnd = false; tab2_opnd = false;">

<!--
    <div class="card">
        <div class="card-header" id="heading-deactivate_location" data-toggle="collapse" data-target="#deactivate_location" aria-expanded="false" aria-controls="deactivate_location"  
             ng-click="tab1_opnd = !tab1_opnd;">
            <a href>
                {{'deactivate_location'|translate}} <i class="fa fa-caret-{{tab1_opnd ? 'up' : 'down'}}"></i>
            </a>
        </div>
        <div id="deactivate_location" class="collapse collapsed" aria-labelledby="deactivate_location">
            <div class="card-body">
                <div class="row">
                    <p>{{'deactivate_location_desc'|translate}}</p>
                    <div class="col-12">
                        <button class="btn btn-info">{{'deactivate_location_btn'|translate}}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
-->
    <div class="card">
        <div class="card-header" id="heading-delete_location" data-toggle="collapse" data-target="#delete_location" aria-expanded="false" aria-controls="delete_location" 
              ng-click="tab2_opnd = !tab2_opnd;">
            <a href>
                {{'delete_location'|translate}} <i class="fa fa-caret-{{tab2_opnd ? 'up' : 'down'}}"></i>
            </a>
        </div>
        <div id="delete_location" class="collapse collapsed" aria-labelledby="delete_location">
            <div class="card-body">
                <div class="row">
                    <p>{{'delete_location_desc'|translate}}</p>
                    <div class="col-12">
                        <button class="btn btn-info">{{'delete_location_btn'|translate}}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>