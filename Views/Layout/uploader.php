<div class="upload_style">
    <div>
        <h4>{{photoType|translate}}</h4>         
            <!--            NEW PHOTOS -->
        <div class="photos">
            <img src="./Library/img/150.jpg" ng-if="filesInfo['photos'+photoType].length == 0" show-img>
            <span ng-repeat="itm in filesInfo['photos'+photoType] track by $index" class="photo">
                <a class="floatBtn" href ng-click="deleteItm( filesInfo['photos'+photoType], $index)">
                    <i class="fa fa-times"></i>
                </a>
                <div>
                    <img src="{{itm.tmp_name||set_img(itm)}}" show-img>
                </div>
            </span>
        </div>
        <label>
            <!--            SINGLE PHOTO -->
            <input ng-if="photoType!='PHOTO' && photoType!='LP_PHOTOS'" 
                   type="file" class="form-control"
                   accept="image/*"
                   file-model="files.photos[photoType]" name="photos{{photoType}}"
                   ng-model="photoObj[photoType]" >
            <div id="error"></div>
            
            <!--            MULTIPLE PHOTOS -->
            <input ng-if="photoType=='PHOTO' || photoType=='LP_PHOTOS'" 
                   type="file" class="form-control"
                   accept="image/*" multiple
                   file-model="files.photos[photoType]" name="photos{{photoType}}"
                   ng-model="photoObj[photoType]" >
            
            <div id="error_message_{{photoType}}" class="errormessage"></div>
        </label>
    </div>
</div>