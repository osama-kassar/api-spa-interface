<div >
    <div class="row">
        <div class="col-2">
            <img src="./Library/img/cleaning.png" alt="">
        </div>
        <div class="col">
            <h2>
                {{'cleaning_progress'|translate}}
            </h2>
            {{'cleaning_progress_msg'|translate}}
        </div>
    </div>
</div>