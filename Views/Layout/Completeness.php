<div  ng-init=" getCompleteness();">
    <h2>{{'Completeness'|translate}} 
        <i class="fa fa-info-circle" tooltip title="{{'Completeness_desc'|translate}}"></i>
    </h2>
    <a class="progress colors_bg"
            tooltip="html" title="" data-original-title="{{basicStatuses}}" >
        <div class="progress-bar  progress-bar-striped"
             role="progressbar" 
             aria-valuenow="{{CompletenessAverage.averageProfileCompleteness}}" 
             aria-valuemin="20" 
             aria-valuemax="100"  
             style="width: {{CompletenessAverage.averageProfileCompleteness}}%">
            <i><span count-to="{{CompletenessAverage.averageProfileCompleteness}}" duration="2" count-from="0"></span>% <i class="fa fa-info-circle"></i></i>

        </div>
    </a>
    <div class="row">
        <div class="col-12"><b>{{'countLocationsMissingData'|translate}}</b>:</div>
        <div class="col-12">
            <div ng-repeat="(k,v) in Completeness.completenessPerField.BASIC" ng-if="!v">
                <i class="fa fa-info-circle" tooltip 
                   title="{{k.toLowerCase()+'.text' |translate}}"></i> 
                <a href="{{currLang}}/{{linksMissedActions[k]}}"><b>{{k|translate}}</b></a>
                <p>{{k.toLowerCase()+'.text' |translate}}</p>
            </div>
        </div>
    </div>
</div>