
<div class="row" style="width: 100%;">
    <div class="col-sm-12">
        <b><i class="fa fa-cogs"></i> {{'landingpage_configs'|translate}}</b> 
    </div>
    
    <hr class="col-12 hr_center">
    
    <div class="col-6 ">
        <i class="fa fa-info-circle" tooltip title="{{'lp_show_hide_desc'|translate}}"></i> {{'lp_show_hide'|translate}}
    </div>
    <div class="col-6 no_label_padding">
        <label class="switch">
            <input type="checkbox" id="lp_show_hide"
                          ng-model="landingpage.configs.status">
            <span class="slider round"></span>
        </label> 
        <label for="lp_show_hide">{{'activated'|translate}}</label>
    </div>
    
    <hr class="col-12 hr_center">
    
    <div class="col-6 ">
        <i class="fa fa-info-circle" tooltip title="{{'page_preview_desc'|translate}}"></i> {{'page_preview_title'|translate}}</div>
    <div class="col-6 " >
        <button class="btn btn-info btn-sm">
            <i class="fa fa-eye"></i> {{'page_preview'|translate}}
        </button>
    </div>
    
    <hr class="col-12 hr_center">
    
    <div class="col-12"><i class="fa fa-info-circle" tooltip title="{{'metatags_desc'|translate}}"></i> {{'metatags_title'|translate}}</div>
    <div class="col-sm-12" >
        <select class="form-control mb-2" ng-model="landingpage.configs.metatags.key">
            <option ng-repeat="itm in landingpage.configs.metatags"
                    value="{{itm.attr}}">{{itm.attr}}</option>
        </select>
        <input type="text" ng-model="landingpage.configs.metatags.val" class="form-control mb-2">
        <button class="btn btn-info"><i class="fa fa-save"></i> {{'save'|translate}}</button>
        <div ng-repeat="itm in landingpage.configs.metatags">
            <b>{{itm.attr}}</b>: <a href ng-click=""><i class="fa fa-pencil" tooltip title="{{'edit'|translate}}"></i></a>
            <p>{{itm.value}}</p>
        </div>
    </div>
</div>