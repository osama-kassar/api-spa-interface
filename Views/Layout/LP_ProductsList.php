<div class="squarelist_style">
    <div>
        <h4>{{'products_list'|translate}}</h4>         
            <!--            NEW PHOTOS -->
        <div  class="row">
            <div class=" col-12">
                <button class="btn btn-success" 
                        tooltip="" title="{{'add_product'|translate}}" 
                        data-toggle="collapse" data-target="#add_product" id="add_product_btn">
                    <i class="fa fa-plus"></i>
                </button>
            </div>
            <div class="col-sm-3 tmpltDiv" ng-repeat="product in landingpage.products">
                <h3>{{product.product_title}}</h3>
                <div>
                    <img src="./Library/img/lp_photos/thumb/{{product.product_photo}}" alt="">
                </div>
                <p>{{product.product_desc}}</p>
                <span>
                    <button class="btn btn-info" tooltip title="{{'edit_product'|translate}}"
                            ng-click="getProduct(product.id);
                                      openTab('add_product');
                                      scrollTo()">
                        <i class="fa fa-pencil"></i>
                    </button>
                    <button class="btn btn-danger" tooltip title="{{'delete_product'|translate}}"
                            ng-click="deleteProduct(product);">
                        <i class="fa fa-trash"></i>
                    </button>
                </span>
            </div>
        </div>
    </div>
</div>