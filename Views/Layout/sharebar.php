<div class="shareBar">
    <b>{{'shareit'|translate}}</b>
    <br>
    <div class="">
        <a href 
            ng-click="openWin('facebook'); updateShares();">
            <i class="fa fa-facebook"></i>
        </a>
        <a href 
            ng-click="openWin('twitter'); updateShares();">
            <i class="fa fa-twitter"></i>
        </a>
        <a href 
            ng-click="openWin('google-plus'); updateShares();">
            <i class="fa fa-google-plus"></i>
        </a>
        <a href 
            ng-click="openWin('linkedin'); updateShares();">
            <i class="fa fa-linkedin"></i>
        </a>
        <a href 
            ng-click="openWin('pinterest'); updateShares();">
            <i class="fa fa-pinterest"></i>
        </a>
        <a href 
            ng-click="openWin('email'); updateShares();">
            <i class="fa fa-envelope"></i>
        </a>
    </div>
</div>
