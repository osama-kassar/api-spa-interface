<div class="container-fluid footer">
    <div class="row">
        <div class="col-12">
            Admea © 2017-2018
            
            <small class="by">by <a href="https://devzonia.com" target="_new">Dev Zonia</a> </small>
        </div>
    </div>
</div>