<div class="listing_style">
    <h2>{{'ListingHealth'|translate}} 
        <i class="fa fa-info-circle" tooltip title="{{'ListingHealth_desc'|translate}}"></i>
    </h2>
    <div class="row">
        <div class="col-9">
            <img src="./Library/img/directories/safeguard-active.svg" alt="" style="height: 20px">
            <span  class="nowrap_line">{{'countListingsInSync'|translate}}</span>
        </div>
        <div class="col-3 text-right">
            <span count-to="{{ListingHealth.countListingsInSync}}" duration="2" count-from="0">{{ListingHealth.countListingsInSync}}</span>
            <i class="fa fa-info-circle" tooltip title="{{'countListingsBeingUpdated_info'|translate}}"></i>
        </div>
    </div>
    <div class="row">
        <div class="col-9">
            <img src="./Library/img/directories/safeguard-updating.svg" alt="" style="height: 20px">
            <span  class="nowrap_line">{{'countListingsBeingUpdated'|translate}}</span>
        </div>
        <div class="col-3 text-right">
            <span count-to="{{ListingHealth.countListingsBeingUpdated}}" duration="1" count-from="0">
            {{ListingHealth.countListingsBeingUpdated}}</span>
            <i class="fa fa-info-circle" tooltip title="{{'countListingsInSync_info'|translate}}"></i>
        </div>
    </div>
    <div class="row">
        <div class="col-9">
            <img src="./Library/img/directories/edit-needed.svg" alt="" style="height: 20px">
            <span class="nowrap_line">{{'countLocationsRequireSync'|translate}}</span>
        </div>
        <div class="col-3 text-right">
            <span count-to="{{ListingHealth.pending}}" duration="2" count-from="0">
            {{ListingHealth.pending}}</span>
            <i class="fa fa-info-circle" tooltip title="{{'countLocationsRequireSync_info'|translate}}"></i>
        </div>
    </div>

    <div class="row" ng-if="ListingHealth.directoriesMissingConnect.length>0">
        <div class="col-12"><b>{{'directoriesMissingConnect'|translate}}</b>:</div>
        <div class="col-12">
            <span ng-repeat="itm in ListingHealth.directoriesMissingConnect">
                <img src="./Library/img/directories/{{itm}}.png" alt="" style="height: 25px;">
            </span><br>
            <a ng-if="currPage!='directories'" href="{{currLang}}/directories">{{'go_to_dir'|translate}}</a>
        </div>
    </div>
</div>