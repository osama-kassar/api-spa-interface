<div ng-if="(currPage!=='login')" class="header">
    <div class="">
        <div class="topmenu row">
            <div class="col">
                <img src="Library/img/admea-logo.png"> <i tooltip title="{{'your_connection_status'|translate}}" class="fa fa-circle {{online ? 'greenText' : 'redText'}}"></i>
            </div>
            <div class=" toptext dropdown">
                <a class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    <small class="hideMob">{{'welcome'|translate}}, <b>Mr. {{user.userInfo.lastname}}</b></small> 
                    <small class="hideWeb"><i class="fa fa-cog"></i></small>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu1">
                    <div class="text-center"><i class="fa fa-caret-up"></i></div>
                    <li >
                        <a href="{{currLang}}/settings" class="dropdown-item"> 
                            <i class="fa fa-cog"></i>
                            {{'settings'|translate}}
                        </a>
                    </li>
                    <li  class="divider"></li>
                    <li >
                        <a href="{{currLang}}/logout"  class="dropdown-item"> 
                            <i class="fa fa-power-off"></i>
                            {{'logout'|translate}}
                        </a>
                    </li>
                </ul>
            </div>
            <div class=" toptext dropdown langs">
                <a class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    {{currLang}} 
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu1">
                    <div class="text-center"><i class="fa fa-caret-up"></i></div>
                    <li ng-repeat="itm in langs" ng-if="itm !== currLang">
                        <a href="{{itm}}/statistics" class="dropdown-item"> 
                            {{itm}}
                        </a>
                    </li>
                </ul>
            </div>
        </div>


        <nav class="navbar navbar-expand-md navbar-light bg-white ">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        
            <div class="company_name">
                <b>{{profile.businessName}}</b> 
                <small>{{profile.streetAndNumber}}, {{profile.zip}} {{profile.city}}<br>{{profile.province}} {{user.userInfo.salesPartner.country}}</small>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    
                    <li class="nav-item  {{isActive('statistics')}}">
                        <a class="nav-link" href="{{currLang}}/statistics">
                            <i class="fa fa-tachometer"></i>
                            {{'Dashboard'|translate}}
                        </a>
                    </li>
                    
                    <li class="hideMob nav-item {{isActive('profiles')}}">
                        <a class="nav-link" href="{{currLang}}/profiles/basicdata">
                            <i class="fa fa-map-marker"></i>
                            {{'Profile'|translate}}
                        </a>
                    </li>
                    
                    <li class=" hideWeb nav-item dropdown {{isActive('profiles')}}">
                        <a class="nav-link dropdown-toggle" href="{{currLang}}/profiles/basicdata" id="navbarDropdown" role="link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-map-marker"></i>
                            {{'Profile'|translate}}
                        </a>
                        <div class="dropdown-menu hideWeb" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item {{isActive('basicdata', true)}}" href="{{currLang}}/profiles/basicdata">{{'Basic Data'|translate}}</a>
                            <a class="dropdown-item {{isActive('richdata', true)}}" href="{{currLang}}/profiles/richdata">{{'Rich Data'|translate}}</a>
                            <a class="dropdown-item {{isActive('media', true)}}" href="{{currLang}}/profiles/media">{{'Photos and Videos'|translate}}</a>
                            <a class="dropdown-item {{isActive('suggestions', true)}}" href="{{currLang}}/profiles/suggestions">{{'Suggestions'|translate}}</a>
                            <a class="dropdown-item {{isActive('landingpage', true)}}" href="{{currLang}}/profiles/landingpage">{{'landingpage'|translate}}</a>
                        </div>
                    </li>
                    <li class="nav-item {{isActive('directories')}}">
                        <a class="nav-link" href="{{currLang}}/directories">
                            <i class="fa fa-usb"></i>
                            {{'Directories'|translate}}
                        </a>
                    </li>
                    <li class="nav-item {{isActive('feedbacks')}}">
                        <a class="nav-link" href="{{currLang}}/feedbacks">
                            <i class="fa fa-envelope"></i>
                            {{'Customer Feedback'|translate}}
                        </a>
                    </li>
                    <li class="nav-item {{isActive('contents')}}">
                        <a class="nav-link" href="{{currLang}}/contents">
                            <i class="fa fa-paper-plane-o"></i>
                            {{'Publish'|translate}}
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
        
        <div class="submenu hideMob" ng-if="currPage=='profiles'">
            <a class="dropdown-item {{isActive('basicdata', true)}}" href="{{currLang}}/profiles/basicdata">{{'Basic Data'|translate}}</a>
            <a class="dropdown-item {{isActive('richdata', true)}}" href="{{currLang}}/profiles/richdata">{{'Rich Data'|translate}}</a>
            <a class="dropdown-item {{isActive('media', true)}}" href="{{currLang}}/profiles/media">{{'Photos and Videos'|translate}}</a>
            <a class="dropdown-item {{isActive('suggestions', true)}}" href="{{currLang}}/profiles/suggestions">{{'Suggestions'|translate}}</a>
            <a class="dropdown-item {{isActive('landingpage', true)}}" href="{{currLang}}/profiles/landingpage"><span class="newBadge">{{'new'|translate}}</span>{{'landingpage'|translate}}</a>
            <div>
                <label ng-if="!profile.autoSync">
                    <button class="btn btn-info btn-xs" ng-click="doSync()">
                        <span class="fa fa-refresh"></span> {{'syncNow'|translate}}
                    </button>
                </label>
                <span>{{'autoSync'|translate}}</span> <span class="fa fa-info-circle" tooltip title="{{'autoSync_desc'|translate}}"></span>
                <label class="switch">
                    <input type="checkbox" id="autoSync"
                           ng-model="profile.autoSync"
                           ng-change="saveProfile()">
                    <span class="slider round"></span>
                </label> 
                <label for="autoSync" class="slideLabel">{{profile.autoSync ? 'on' : 'off'|translate}}</label>
            </div>
        </div>
    </div>