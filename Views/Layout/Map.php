<div>
    <div class="row">
        <div class="col-12 map">
            <img src="https://maps.googleapis.com/maps/api/staticmap?center={{profile.lat}},{{profile.lng}}&zoom=16&maptype=roadmap&markers=color:red|{{profile.lat}},{{profile.lng}}&size=320x160&scale=1&language=en&key=AIzaSyCqChdwcS_OfmDJWh7LaexI-hJR2WV6aCc" alt="">
        </div>
        <div class="col-2 ">
            <img src="./Library/img/address.png" alt="">
        </div>
        <div class="col">
            <b>{{profile.name}}</b>
            <p>{{profile.streetAndNumber}}, {{profile.zip}} {{profile.city}} {{profile.country}}</p>
        </div>
    </div>
</div>