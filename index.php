<?php include_once('./Server/meta.php'); $mode='production'; ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>
		<title><?=$dt['site_main_title']?> | <?=$dt['_title']?></title>
		<base href="<?=$app_folder?>/" />
		<!-- Meta Tags -->
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />  
        <meta http-equiv="Pragma" content="no-cache" />  
        <meta http-equiv="Expires" content="0" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="robots" content="index, follow" />
        
		<!-- Meta SEO -->
		<meta name="generator" content="<?=$mainDt['site_main_title']?> <?=$metaDt['_title']?>" />
		<meta name="keywords" content="<?=$metaDt['_keywords']?>" />
		<meta name="description" content="<?=$metaDt['_description']?>" />
        <meta name="author" content="Osama Kassar" />
        <meta name="date" content="Jul. 10, 2019" />
        
        <link rel="icon" type="png" href="icon.png" />
 		<link rel="canonical" href="<?=urldecode( $mainDt['current_url'] )?>" />
        
		<!-- CSS  -->
		<link rel="stylesheet" type="text/css" href="Library/css/bootstrap.min.css" >
		<link rel="stylesheet" type="text/css" href="Library/css/font-awesome.min.css" >
		<link rel="stylesheet" type="text/css" href="Library/css/ng-tags-input.bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="Library/css/ng-tags-input.min.css">
		<link rel="stylesheet" type="text/css" href="Library/css/loading-bar.min.css" >
		<link rel="stylesheet" type="text/css" href="Library/css/style.css">
		<!-- JS Libraries 1.7.0 -->
		<script defer src="Library/js/jquery-1.11.1.min.js"></script>
		<script defer src="Library/js/popper.min.js"></script>
		<script defer src="Library/js/bootstrap.min.js"></script>
		<!-- AngularJS Libraries 1.7.0 -->
		<script defer src="Library/js/angular.js"></script>
		<script defer src="Library/js/angular-route.min.js"></script> 
		<script defer src="Library/js/angular-animate.min.js"></script> 
		<script defer src="Library/js/angular-count-to.js"></script>
		<script defer src="Library/js/angular-translate.min.js"></script>
		<script defer src="Library/js/angular-translate-loader-url.min.js"></script>
		<script defer src="Library/js/ui-bootstrap-tpls-3.0.6.min.js"></script>
		<script defer src="Library/js/ng-tags-input.min.js"></script>
		<script defer src="Library/js/loading-bar.min.js"></script>
		<script defer src="Library/js/Chart.min.js"></script>
		<script defer src="Library/js/angular-chart.min.js"></script>
		<!-- AngularJS Controllers. -->
		<script defer src="App/app.js"></script>
		<script defer src="App/routes.js"></script>
		<script defer src="App/directives.js"></script>
		<script defer src="App/Controllers/UsersController.js"></script>
		<script defer src="App/Controllers/ProfilesController.js"></script>
		<script defer src="App/Controllers/StatisticsController.js"></script>
		<script defer src="App/Controllers/DirectoriesController.js"></script>
		<script defer src="App/Controllers/FeedbacksController.js"></script>
		<script defer src="App/Controllers/ContentsController.js"></script>
	</head>
	
	
	
	<body ng-app="app" ng-controller="MainAppCtrl as MainAppCtrl" ng-claok>
        
        <div class="preloader-all"><i class="fa fa-cog fa-spin fa-5x fa-fw"></i></div>
		<div>
            <header ng-include="'Views/Layout/header.php'">
                <!-- THIS IS SIDE BAR -->
            </header>

            <div >
                <div ng-view style="min-height: 550px;">
                     <!--THIS IS WHERE THE VIEW COMES -->
                </div>
            </div>
            <footer ng-include="'Views/Layout/footer.php'">
                <!-- THIS IS FOOTER -->
            </footer>
        </div>
        
        <modal-container></modal-container>
        
        <div id="msgHolder" onClick="this.setAttribute('style', 'opacity:0; visibility:hidden;')">
            <div class="{{msgColor}}">
                <span><i class="fa fa-times"></i></span>
                <p>
                    <div>{{modalMsg}}</div>
                    <i style="color: red;">{{modalError}}</i>
                </p>
            </div>
        </div>
        
        <div id="imgHolder" onClick="this.setAttribute('style', 'opacity:0; visibility:hidden;')"></div>
        
        <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              {{modalMsg}}
            </div>
          </div>
        </div>
    
    <script>
        setTimeout(function(){
            $(".preloader-all").css({"opacity":"0", "zIndex":"-1"})
        }, 1000)
    </script>
	</body>
	
</html>







