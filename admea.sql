-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 02, 2020 at 02:54 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admea`
--

DELIMITER $$
--
-- Procedures
--
CREATE PROCEDURE `contents_del` (IN `_id` VARCHAR(255))  BEGIN 
	DELETE FROM contents WHERE id IN (_id); 
END$$

CREATE PROCEDURE `contents_list` (IN `_orderby` VARCHAR(255), IN `_from` INT, IN `_to` INT)  BEGIN 
	SELECT *, ( 
		SELECT COUNT(*) FROM contents
		) AS 'total' 
	FROM contents 
ORDER BY 
	CASE WHEN _orderby = '' THEN id END ASC, 
	CASE WHEN _orderby = 'id:asc' THEN id END ASC, 
	CASE WHEN _orderby = 'id:desc' THEN id END DESC, 
	CASE WHEN _orderby = 'section_id:asc' THEN section_id END ASC, 
	CASE WHEN _orderby = 'section_id:desc' THEN section_id END DESC, 
	CASE WHEN _orderby = 'rec_state:asc' THEN rec_state END ASC, 
	CASE WHEN _orderby = 'rec_state:desc' THEN rec_state END DESC LIMIT _from, _to; 
END$$

CREATE PROCEDURE `contents_one` (IN `_id` INT(11), IN `_section_id` INT(11))  BEGIN 
SELECT * FROM contents 
	WHERE (_id = -1 OR contents.id = _id)
	AND (_section_id = -1 OR contents.section_id = _section_id); 
END$$

CREATE PROCEDURE `contents_save` (IN `_id` INT(11), IN `_landingpage_id` INT(11), IN `_section_id` TINYINT(4), IN `_content_object` MEDIUMTEXT, IN `_rec_state` TINYINT(4))  BEGIN 
IF _id > -1 THEN  UPDATE contents 
	SET 
		id = CASE WHEN _id != -1 THEN _id ELSE id END, 
		landingpage_id = CASE WHEN _landingpage_id != -1 THEN _landingpage_id ELSE landingpage_id END, 
		section_id = CASE WHEN _section_id != -1 THEN _section_id ELSE section_id END, 
		content_object = CASE WHEN _content_object != -1 THEN _content_object ELSE content_object END, 
		rec_state = CASE WHEN _rec_state != -1 THEN _rec_state ELSE rec_state END 
	WHERE id = _id; 
	SELECT LAST_INSERT_ID() AS 'last_id'; 
ELSE INSERT INTO contents ( 
		landingpage_id, 
		section_id, 
		content_object, 
		rec_state ) 
	VALUES (  
		_landingpage_id, 
		_section_id, 
		_content_object, 
		_rec_state ); 
	SELECT LAST_INSERT_ID() AS 'last_id'; 
END IF; 
END$$

CREATE PROCEDURE `landingpages_del` (IN `_id` VARCHAR(255))  BEGIN 
	DELETE FROM landingpages WHERE id IN (_id); 
END$$

CREATE PROCEDURE `landingpages_list` (IN `_orderby` VARCHAR(255), IN `_from` INT, IN `_to` INT)  BEGIN 
	SELECT *, ( 
		SELECT COUNT(*) FROM landingpages
		) AS 'total' 
	FROM landingpages 
ORDER BY 
	CASE WHEN _orderby = '' THEN id END ASC, 
	CASE WHEN _orderby = 'id:asc' THEN id END ASC, 
	CASE WHEN _orderby = 'id:desc' THEN id END DESC, 
	CASE WHEN _orderby = 'page_title:asc' THEN page_title END ASC, 
	CASE WHEN _orderby = 'page_title:desc' THEN page_title END DESC, 
	CASE WHEN _orderby = 'seo_data:asc' THEN seo_data END ASC, 
	CASE WHEN _orderby = 'seo_data:desc' THEN seo_data END DESC, 
	CASE WHEN _orderby = 'stat_views:asc' THEN stat_views END ASC, 
	CASE WHEN _orderby = 'stat_views:desc' THEN stat_views END DESC, 
	CASE WHEN _orderby = 'stat_shares:asc' THEN stat_shares END ASC, 
	CASE WHEN _orderby = 'stat_shares:desc' THEN stat_shares END DESC, 
	CASE WHEN _orderby = 'rec_state:asc' THEN rec_state END ASC, 
	CASE WHEN _orderby = 'rec_state:desc' THEN rec_state END DESC LIMIT _from, _to; 
END$$

CREATE PROCEDURE `landingpages_one` (IN `_id` INT(11), IN `_user_id` INT)  BEGIN 
SELECT * FROM landingpages 
	WHERE (_id = -1 OR landingpages.id = _id) 
	AND (_user_id = -1 OR landingpages.user_id = _user_id); 
END$$

CREATE PROCEDURE `landingpages_save` (IN `_id` INT(11), IN `_user_id` INT(11), IN `_user_domain` VARCHAR(255), IN `_page_title` VARCHAR(255), IN `_seo_data` TEXT, IN `_stat_views` INT(11), IN `_stat_shares` INT(11), IN `_rec_state` TINYINT(4))  BEGIN 
IF _id > -1 THEN  UPDATE landingpages 
	SET 
		id = CASE WHEN _id != -1 THEN _id ELSE id END, 
		user_id = CASE WHEN _user_id != -1 THEN _user_id ELSE user_id END, 
		user_domain = CASE WHEN _user_domain != -1 THEN _user_domain ELSE user_domain END, 
		page_title = CASE WHEN _page_title != -1 THEN _page_title ELSE page_title END, 
		seo_data = CASE WHEN _seo_data != -1 THEN _seo_data ELSE seo_data END, 
		stat_views = CASE WHEN _stat_views != -1 THEN _stat_views ELSE stat_views END, 
		stat_shares = CASE WHEN _stat_shares != -1 THEN _stat_shares ELSE stat_shares END, 
		rec_state = CASE WHEN _rec_state != -1 THEN _rec_state ELSE rec_state END 
	WHERE id = _id; 
	SELECT LAST_INSERT_ID() AS 'last_id'; 
ELSE INSERT INTO landingpages ( 
    	user_id,
    	user_domain,
		page_title, 
		seo_data, 
		stat_views, 
		stat_shares, 
		rec_state ) 
	VALUES (  
        _user_id,
        _user_domain,
		_page_title, 
		_seo_data, 
		_stat_views, 
		_stat_shares, 
		_rec_state ); 
	SELECT * FROM landingpages WHERE id = LAST_INSERT_ID(); 
END IF; 
END$$

CREATE PROCEDURE `news_del` (IN `_id` VARCHAR(255))  BEGIN 
	DELETE FROM news WHERE id IN (_id); 
END$$

CREATE PROCEDURE `news_list` (IN `_orderby` VARCHAR(255), IN `_from` INT, IN `_to` INT)  BEGIN 
	SELECT *, ( 
		SELECT COUNT(*) FROM news
		) AS 'total' 
	FROM news 
ORDER BY 
	CASE WHEN _orderby = '' THEN id END ASC, 
	CASE WHEN _orderby = 'id:asc' THEN id END ASC, 
	CASE WHEN _orderby = 'id:desc' THEN id END DESC, 
	CASE WHEN _orderby = 'landingpage_id:asc' THEN landingpage_id END ASC, 
	CASE WHEN _orderby = 'landingpage_id:desc' THEN landingpage_id END DESC, 
	CASE WHEN _orderby = 'news_title:asc' THEN news_title END ASC, 
	CASE WHEN _orderby = 'news_title:desc' THEN news_title END DESC, 
	CASE WHEN _orderby = 'news_desc:asc' THEN news_desc END ASC, 
	CASE WHEN _orderby = 'news_desc:desc' THEN news_desc END DESC, 
	CASE WHEN _orderby = 'news_photo:asc' THEN news_photo END ASC, 
	CASE WHEN _orderby = 'news_photo:desc' THEN news_photo END DESC, 
	CASE WHEN _orderby = 'rec_state:asc' THEN rec_state END ASC, 
	CASE WHEN _orderby = 'rec_state:desc' THEN rec_state END DESC LIMIT _from, _to; 
END$$

CREATE PROCEDURE `news_one` (IN `_id` INT(11))  BEGIN 
SELECT * FROM news 
	WHERE (_id = -1 OR news.id = _id); 
END$$

CREATE PROCEDURE `news_save` (IN `_id` INT(11), IN `_landingpage_id` INT(11), IN `_news_title` VARCHAR(255), IN `_news_desc` TEXT, IN `_news_photo` VARCHAR(255), IN `_rec_state` TINYINT(4))  BEGIN 
IF _id > -1 THEN  UPDATE news 
	SET 
		id = CASE WHEN _id != -1 THEN _id ELSE id END, 
		landingpage_id = CASE WHEN _landingpage_id != -1 THEN _landingpage_id ELSE landingpage_id END, 
		news_title = CASE WHEN _news_title != -1 THEN _news_title ELSE news_title END, 
		news_desc = CASE WHEN _news_desc != -1 THEN _news_desc ELSE news_desc END, 
		news_photo = CASE WHEN _news_photo != -1 THEN _news_photo ELSE news_photo END, 
		rec_state = CASE WHEN _rec_state != -1 THEN _rec_state ELSE rec_state END 
	WHERE id = _id; 
	SELECT LAST_INSERT_ID() AS 'last_id'; 
ELSE INSERT INTO news ( 
		landingpage_id, 
		news_title, 
		news_desc, 
		news_photo, 
		rec_state ) 
	VALUES (  
		_landingpage_id, 
		_news_title, 
		_news_desc, 
		_news_photo, 
		_rec_state ); 
	SELECT LAST_INSERT_ID() AS 'last_id'; 
END IF; 
END$$

CREATE PROCEDURE `products_del` (IN `_id` VARCHAR(255))  BEGIN 
	DELETE FROM products WHERE id IN (_id); 
END$$

CREATE PROCEDURE `products_list` (IN `_orderby` VARCHAR(255), IN `_from` INT, IN `_to` INT)  BEGIN 
	SELECT *, ( 
		SELECT COUNT(*) FROM products
		) AS 'total' 
	FROM products 
ORDER BY 
	CASE WHEN _orderby = '' THEN id END ASC, 
	CASE WHEN _orderby = 'id:asc' THEN id END ASC, 
	CASE WHEN _orderby = 'id:desc' THEN id END DESC, 
	CASE WHEN _orderby = 'landingpage_id:asc' THEN landingpage_id END ASC, 
	CASE WHEN _orderby = 'landingpage_id:desc' THEN landingpage_id END DESC, 
	CASE WHEN _orderby = 'product_title:asc' THEN product_title END ASC, 
	CASE WHEN _orderby = 'product_title:desc' THEN product_title END DESC, 
	CASE WHEN _orderby = 'product_desc:asc' THEN product_desc END ASC, 
	CASE WHEN _orderby = 'product_desc:desc' THEN product_desc END DESC, 
	CASE WHEN _orderby = 'product_photo:asc' THEN product_photo END ASC, 
	CASE WHEN _orderby = 'product_photo:desc' THEN product_photo END DESC, 
	CASE WHEN _orderby = 'rec_state:asc' THEN rec_state END ASC, 
	CASE WHEN _orderby = 'rec_state:desc' THEN rec_state END DESC LIMIT _from, _to; 
END$$

CREATE PROCEDURE `products_one` (IN `_id` INT(11))  BEGIN 
SELECT * FROM products 
	WHERE (_id = -1 OR products.id = _id); 
END$$

CREATE PROCEDURE `products_save` (IN `_id` INT(11), IN `_landingpage_id` INT(11), IN `_product_title` VARCHAR(255), IN `_product_desc` TEXT, IN `_product_photo` VARCHAR(255), IN `_rec_state` TINYINT(4))  BEGIN 
IF _id > -1 THEN  UPDATE products 
	SET 
		id = CASE WHEN _id != -1 THEN _id ELSE id END, 
		landingpage_id = CASE WHEN _landingpage_id != -1 THEN _landingpage_id ELSE landingpage_id END, 
		product_title = CASE WHEN _product_title != -1 THEN _product_title ELSE product_title END, 
		product_desc = CASE WHEN _product_desc != -1 THEN _product_desc ELSE product_desc END, 
		product_photo = CASE WHEN _product_photo != -1 THEN _product_photo ELSE product_photo END, 
		rec_state = CASE WHEN _rec_state != -1 THEN _rec_state ELSE rec_state END 
	WHERE id = _id; 
	SELECT LAST_INSERT_ID() AS 'last_id'; 
ELSE INSERT INTO products ( 
		landingpage_id, 
		product_title, 
		product_desc, 
		product_photo, 
		rec_state ) 
	VALUES (  
		_landingpage_id, 
		_product_title, 
		_product_desc, 
		_product_photo, 
		_rec_state ); 
	SELECT LAST_INSERT_ID() AS 'last_id'; 
END IF; 
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

CREATE TABLE `contents` (
  `id` int(11) NOT NULL,
  `landingpage_id` int(11) NOT NULL,
  `section_id` tinyint(4) NOT NULL,
  `content_object` mediumtext NOT NULL COMMENT 'Json all section items and contents',
  `rec_state` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contents`
--

INSERT INTO `contents` (`id`, `landingpage_id`, `section_id`, `content_object`, `rec_state`) VALUES
(1, 22, 1, '{\"title\":\"Lorem Ipsum MOTO lorems\",\"subtitle\":\"--Lands\' Ends--\",\"LP_BACKGROUND\":\"1588423271328_LP_BACKGROUND.png\",\"LP_LOGO\":\"1588423271328_LP_LOGO.png\"}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `landingpages`
--

CREATE TABLE `landingpages` (
  `id` int(11) NOT NULL,
  `user_id` bigint(11) NOT NULL,
  `user_domain` varchar(255) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `seo_data` text NOT NULL COMMENT 'Json all seo stuff',
  `stat_views` int(11) NOT NULL DEFAULT '0',
  `stat_shares` int(11) NOT NULL DEFAULT '0',
  `rec_state` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `landingpages`
--

INSERT INTO `landingpages` (`id`, `user_id`, `user_domain`, `page_title`, `seo_data`, `stat_views`, `stat_shares`, `rec_state`) VALUES
(22, 106502, 'localhost', 'ABC Marketing GmbH', '[{\"attr\":\"keywords\",\"value\":\"Marketing,SEO,Web-Hosting,Web-Design,SEO-Optimierung\",\"type\":\"name\"},{\"attr\":\"description\",\"value\":\"ABC Agentur ist Ihr Full-Service-Provider in den Bereichen Corporate Identity, SEO-Optimierung und Web-Hosting. Wir stehen Ihnen mit unserer Erfahrung sowie Expertise zur Seite. Wir kennen die Herausforderungen im digitalen Zeitalter und bereiten Sie entsprechend darauf vor.\",\"type\":\"name\"},{\"attr\":\"auther\",\"value\":\"ABC Marketing GmbH\",\"type\":\"name\"}]', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `landingpage_id` int(11) NOT NULL,
  `news_title` varchar(255) NOT NULL,
  `news_desc` text NOT NULL,
  `news_photo` varchar(255) NOT NULL,
  `rec_state` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `landingpage_id`, `news_title`, `news_desc`, `news_photo`, `rec_state`) VALUES
(-1, 2321, '685897', '-1', '-1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `landingpage_id` int(11) NOT NULL,
  `product_title` varchar(255) NOT NULL,
  `product_desc` text NOT NULL,
  `product_photo` varchar(255) NOT NULL,
  `rec_state` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contents`
--
ALTER TABLE `contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `landingpages`
--
ALTER TABLE `landingpages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contents`
--
ALTER TABLE `contents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `landingpages`
--
ALTER TABLE `landingpages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
